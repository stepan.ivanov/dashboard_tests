properties([
        disableConcurrentBuilds()
])

pipeline {
    agent any
    parameters {
        choice(
                description: "Target suite",
                choices: ['ui', 'dashboard', 'sanitychecks'],
                name: "TEST_SUITE")
        choice(
                description: "Target environment",
                choices: ['stage','devel-13'],
                name: "ENVIRONMENT")
        choice(
                description: "Target browser",
                choices: ['chrome', 'firefox'],
                name: "BROWSER")
    }

    stages {
        stage ("Create Docker image") {
            steps {
                script {
                    sh """docker build -f Dashboard.Dockerfile -t dashboard . --no-cache"""
                }
            }
        }

        stage ("Clean report folder") {
            steps {
                script {
                    sh """rm -rf allure-results"""
                    sh """rm -rf allure_reports"""
                    sh """mkdir -p allure_reports"""
                }
            }
        }

        stage ('Run tests') {
            steps {
                script {
                    sh """docker run --rm -v \$(pwd)/allure_reports/:/app/target/allure-results/:rw dashboard:latest mvn test -Denv=${params.ENVIRONMENT} -DsuiteXmlFiles=${params.TEST_SUITE} -Dbrowser=${params.BROWSER}"""
                }
            }
        }
    }

    post {
        always {
            script {
                sh """mkdir -p allure-results"""
                sh """cp allure_reports/* allure-results/"""
            }
            allure results: [[path: 'allure-results']]
        }
    }
}
