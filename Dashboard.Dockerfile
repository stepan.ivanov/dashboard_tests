FROM maven:3.6.0-jdk-11-slim
RUN     mkdir /app
WORKDIR /app
COPY    pom.xml .
ADD     suites ./suites
ADD     src ./src
RUN mvn -f pom.xml clean package -Dmaven.test.skip=true
