FROM selenoid/vnc:chrome_91.0

RUN     mkdir /app
WORKDIR /app
ADD     src/test/resources ./src/test/resources
RUN chown -R selenium:nogroup ./src/test/resources

USER selenium