package com.mati.web.tests;

import com.mati.api.ApiService;
import com.mati.api.models.AuthBodyModel;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.TokenModel;
import com.mati.api.models.merchant.MerchantModel;
import com.mati.utils.EnvironmentConfig;
import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

public class BaseAPITest {

    @BeforeSuite(alwaysRun = true)
    public void init() {
        RestAssured.useRelaxedHTTPSValidation();
        ConnectionConfig.CloseIdleConnectionConfig closeIdleConnectionConfig = new ConnectionConfig.CloseIdleConnectionConfig(600L, TimeUnit.SECONDS);
        RestAssured.config().connectionConfig(ConnectionConfig.connectionConfig().closeIdleConnectionsAfterEachResponseAfter(closeIdleConnectionConfig));

        String token = ApiService.getToken().as(TokenModel.class).getToken();
        TokenContainer.getInstance().setToken(token);

        Response response = ApiService.auth(new AuthBodyModel(EnvironmentConfig.login,EnvironmentConfig.password,""));
        MerchantModel merchantModel = response.as(MerchantModel.class);
        TokenContainer.getInstance().setBearer(merchantModel.getToken());
        TokenContainer.getInstance().setMerchant(merchantModel.getMerchant().getId());
    }

    @AfterClass
    public void deleteTestFlows() {
        ApiService.deleteFlows();
    }

}

