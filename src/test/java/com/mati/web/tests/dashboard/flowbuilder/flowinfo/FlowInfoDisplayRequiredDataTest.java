package com.mati.web.tests.dashboard.flowbuilder.flowinfo;

import com.mati.web.DriverUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class FlowInfoDisplayRequiredDataTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID);
    }

    @Test(description = "C2722 - Flow Info - Display required data")
    public void isFlowURLCorrectTest() {
        String expected = flowHelper.getFlowPageUrl(IConstants.AQA_FLOW_ID);
        assertThat(
                "Flow does not appear at correct URL",
                DriverUtils.getCurrentUrl(),
                Matchers.equalTo(expected)
        );
    }

    @Test(description = " C2722 - Flow Info - Display required data")
    public void isFlowNameDisplayedTest() {
        assertThat(
                "Flow name is not displayed on flow page",
                flowHelper.isFlowNameDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = " C2722 - Flow Info - Display required data")
    public void isFlowCreationDateDisplayedTest() {
        assertThat(
                "Flow creation date is not displayed on flow page",
                flowHelper.isFlowCreationDateDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = " C2722 - Flow Info - Display required data")
    public void isFlowNameNotEditableTest() {
        assertThat(
                "Flow name is editable on flow page",
                flowHelper.isFlowNameNotEditable(),
                Matchers.equalTo(true)
        );
    }

}
