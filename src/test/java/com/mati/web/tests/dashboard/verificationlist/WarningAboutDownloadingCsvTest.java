package com.mati.web.tests.dashboard.verificationlist;

import com.codeborne.selenide.WebDriverRunner;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class WarningAboutDownloadingCsvTest extends BaseWebTest {

    @Test(description = "C9211 - Before refresh page user should be warned about download CSV in progress")
    public void warnAboutDownloadingCsvTest() {
        loginHelper.loginMatiSuccessful();
        verificationListHelper
                .navigateToPage()
                .downloadVerificationCSV();

        WebDriverRunner.getWebDriver().navigate().refresh();

        assertThat("Alert is present",
                verificationListHelper.isAlertPresent(), equalTo(true));
    }
}
