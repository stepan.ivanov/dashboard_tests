package com.mati.web.tests.dashboard.flowbuilder.graphui;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class DeleteFlowTest extends BaseWebTest {

    @BeforeClass
    public void navigateToFlow() {
        ApiService.createFlow(RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0));
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isDeleteButtonReturnToVerificationFlowPageTest() {
        flowHelper.clickFlowSettingsButton()
                .clickDeleteFlowButton()
                .clickConfirmDeleteFlowButton();
        assertThat(
                "After deleting of flow user does not move to Verification Flows list",
                verificationFlowsHelper.isVerificationFlowsPage(),
                Matchers.equalTo(true)
        );
    }
}
