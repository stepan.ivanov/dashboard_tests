package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class CountryRestrictionTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerificationFlow() {
        ApiService.createFlow(RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0))
                .clickProductInFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .selectSettingCheckbox(DocumentationSettings.COUNTRY_RESTRICTION, true)
        .fillCountryRestriction("Andorra")
        .fillCountryRestriction("Mexico")
        .fillCountryRestriction("Afghanistan")
        .clickCrossCountryRestrictionButton();
    }

    @DataProvider
    public Object[] countries() {
        return new Object[] {"Andorra", "Mexico", "Afghanistan"};
    }

    @Test(description = "C2750 - Country restriction - clears all with the large X in the drop-down," +
            "C15532 - Country restriction - specific countries can be removed from the flow", dataProvider = "countries")
    public void allCountryRestrictionAreDeletedTest(String country) {
        assertThat(
                "Country was not deleted",
                flowHelper.isCountryRestrictionDisplayed(country),
                Matchers.equalTo(false)
        );
    }

}