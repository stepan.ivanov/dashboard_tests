package com.mati.web.tests.dashboard.dev;

import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.mati.utils.RandomUtils.randomString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class DevTest extends BaseWebTest {

    private final String password = randomString();
    private final int diff = 2;

    @BeforeClass
    public void loginToMati() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(description = "C2785 - Asterisks in the Secret field of the webhook setup dialog can be deleted")
    public void asterisksCanBeDeletedTest() {
        devHelper.navigateToPage();
        devHelper.openWebHooksMenu()
                .fillSecretInput(password)
                .removeLastCharactersInSecretInput(diff);

        assertThat(
                "Asterisks can be deleted",
                webHookMenuHelper.getSecret().length(),
                equalTo(password.length() - diff));
    }

    @Test(description = "C2785 - Asterisks in the Secret field of the webhook setup dialog can be deleted")
    public void secretCanBeDeletedTest() {
        devHelper.navigateToPage();
        devHelper.openWebHooksMenu()
                .clickToggleSecretVisibility()
                .fillSecretInput(password)
                .removeLastCharactersInSecretInput(diff);

        assertThat(
                webHookMenuHelper.getSecret(),
                equalTo(password.substring(0, password.length() - diff)));

    }
}
