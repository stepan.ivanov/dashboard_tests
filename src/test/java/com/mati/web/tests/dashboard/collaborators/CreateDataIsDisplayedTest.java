package com.mati.web.tests.dashboard.collaborators;

import com.mati.enums.Role;
import com.mati.utils.DateUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.time.LocalDateTime;

import static com.mati.utils.RandomUtils.randomString;
import static com.mati.utils.RandomUtils.randomTestPersonalEmailWithPrefix;

public class CreateDataIsDisplayedTest extends BaseWebTest {

    private final String email = randomTestPersonalEmailWithPrefix();
    private final String firstName = randomString();
    private final String lastName = randomString();
    private final Role role = Role.AGENT;
    private LocalDateTime currentDateTimeOfRegistration;

    @BeforeClass
    public void inviteNewUser() {
        gmailHelper.openInputGmailMessages(IConstants.testEmailPersonal, IConstants.passwordForTestEmailPersonal);
        loginHelper.loginMatiSuccessful();
        analyticsHelper.navigateToPage();
        rollUpMenuHelper.clickOnInviteTeamButton();
        invitationPageHelper
                .inviteNewTeamMember(firstName, lastName, email, role);
        currentDateTimeOfRegistration = DateUtils.currentDateOfRegistration();
    }

    @Test(description = "C2707 - Correct creation data of the agent is displayed")
    public void isCorrectCreationDateDisplayedTest() {
        settingsPageHelper.navigateToPage()
                .openProfileByEmail(email);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(agentHistoryPageHelper.getEmail(), email.toLowerCase());
        softAssert.assertEquals(agentHistoryPageHelper.getName(), firstName + " " + lastName);
        softAssert.assertEquals(agentHistoryPageHelper.getDateOfRegistration(), currentDateTimeOfRegistration);
        softAssert.assertEquals(agentHistoryPageHelper.getRole(), role);
        softAssert.assertAll();
    }

    @AfterMethod
    public void deleteNewUser(){
        settingsPageHelper
                .navigateToPage()
                .deleteUserByEmail(email);
    }
}
