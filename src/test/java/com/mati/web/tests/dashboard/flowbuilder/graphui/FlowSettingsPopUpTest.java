package com.mati.web.tests.dashboard.flowbuilder.graphui;

import com.mati.utils.RandomUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class FlowSettingsPopUpTest extends BaseWebTest {

    private final String flowName = IConstants.prefix + RandomUtils.randomString();

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        verificationFlowsHelper.navigateToPage().createFlow(flowName);
    }

    @BeforeMethod
    public void openFlowSettings() {
        flowHelper.clickFlowSettingsButton();
    }

    @Test(description = "C2723 - Flow Info - “Gear” icon opens Flow Settings popup")
    public void isFlowSettingsPopupDisplayed() {
        assertThat(
                "Flow settings popup is not opened",
                flowHelper.isFlowSettingsPopupDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isFlowNameDisplayedTest() {
        assertThat(
                "Flow name is not displayed in flow settings popup",
                flowHelper.isFlowNamePopUpDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isFlowIdDisplayedTest() {
        assertThat(
                "Flow id is not displayed in flow settings popup",
                flowHelper.isFlowIdPopUpDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isDateCreationFlowDisplayedTest() {
        assertThat(
                "Date creation of flow is not displayed in flow settings popup",
                flowHelper.isDateCreationFlowPopUpDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isListsFlowChecksDisplayedTest() {
        assertThat(
                "Lists the flow’s checks is not displayed in flow settings popup",
                flowHelper.isListsFlowChecksDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isCDPRCheckBoxSwitchableTest() {
        assertThat(
                "CDPR setting is not switchable",
                flowHelper.isCDPRCheckBoxSwitchable(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isPolicyDaysEnabledTest() {
        boolean result = flowHelper.selectGDPRCheckBox(true).isPoliceDaysEnabled();
        assertThat(
                "Policy days is not enabled with selected GDPR setting",
                result,
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isTimestampCheckBoxSwitchableTest() {
        assertThat(
                "Timestamp setting is not switchable",
                flowHelper.isTimestampCheckBoxSwitchable(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isUnsavedChangesAlertDisplayedTest() {
        flowHelper.selectGDPRCheckBox(true).fillPolicyDays(RandomUtils.randomNumber());
        assertThat(
                "Alert message is not displayed for unsaved changes",
                flowHelper.isUnsavedChangesAlertPopupDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isSaveButtonSavesAllChangesTest() {
        flowHelper.selectGDPRCheckBox(true)
                .fillPolicyDays(RandomUtils.randomNumber())
                .selectTimestampCheckBox(true)
                .clickSaveChangesButton()
                .clickFlowSettingsButton();
        assertThat(
                "Flow settings changes were not save after click to 'Save changes and publish' button",
                flowHelper.isFlowSettingsChangesSaved(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isConfirmationPopupDisplayedTest() {
        flowHelper.clickDeleteFlowButton();
        boolean result = flowHelper.isConfirmationPopupDisplayed();
        flowHelper.closeModalWindow().clickFlowSettingsButton();
        assertThat(
                "Confirmation popup is not displayed after click to Delete button",
                result,
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isCloseButtonNotSavesChangesTest() {
        flowHelper.selectGDPRCheckBox(true)
                .fillPolicyDays(RandomUtils.randomNumber())
                .selectTimestampCheckBox(true)
                .closeModalWindow()
                .clickFlowSettingsButton();
        assertThat(
                "Flow settings changes were not save after click to 'Save changes and publish' button",
                flowHelper.isFlowSettingsChangesSaved(),
                Matchers.equalTo(false)
        );
    }

    //TODO BUG - after click to CANCEL button
    //actual result: flow settings popup is closed, expected result: confirm popup is closed but flow settings is not closed
    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isCancelDeletingButtonReturnToFlowSettingsTest() {
        flowHelper.clickDeleteFlowButton().clickCancelDeletingButton();
        boolean actual = flowHelper.isFlowSettingsPopupDisplayed();
        flowHelper.clickFlowSettingsButton();
        assertThat(
                "Flow settings popup is not displayed after click cancel deleting button",
                actual,
                Matchers.equalTo(false)
        );
    }


    @Test(description = "C2737 - Flow settings popup displays and stores required settings")
    public void isCloseButtonClosesFlowSettingsTest() {
        flowHelper.closeModalWindow();
        boolean result = flowHelper.isFlowSettingsPopupDisplayed();
        flowHelper.clickFlowSettingsButton();
        assertThat(
                "Flow settings popup is displayed after click close button",
                result,
                Matchers.equalTo(false)
        );
    }

    @AfterMethod
    public void closeFlowSettings() {
        flowHelper.closeModalWindow();
    }

    @AfterClass
    public void deleteTestFlow() {
        verificationFlowsHelper.navigateToPage().removeFlowByName(flowName);
    }
}
