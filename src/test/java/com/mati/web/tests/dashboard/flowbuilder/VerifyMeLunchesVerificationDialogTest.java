package com.mati.web.tests.dashboard.flowbuilder;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.enums.verificationflows.Products;
import com.mati.utils.RandomUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class VerifyMeLunchesVerificationDialogTest extends BaseWebTest {

    @BeforeClass
    public void loginAndClickToVerifyMeButton() {
        ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
    }

    @Test(description = "C2716 - Verify Me launches verification dialog")
    public void isVerificationWizardDisplayedTest() {
        verificationFlowsHelper.navigateToPage()
                .selectVerificationFlowByName(IConstants.AQA_FLOW_NAME)
                .clickVerifyMeButton()
                .switchToVerificationWizard()
                .waitFlowWizardToBeLoaded();
        assertThat(
                "Verification wizard was not opened after click 'Verify Me' button",
                flowHelper.isVerificationWizardDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2721 - Flow name can contain the _underscore_ character")
    public void isFlowNameCanContainUnderscoreCharacterTest() {
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0))
                .moveProductToFlowBuilder(Products.IP_CHECK)
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .switchToVerificationWizard()
                .waitFlowWizardToBeLoaded();
        assertThat(
                "Flow name can not contain underscore character",
                flowHelper.isVerificationWizardDisplayed(),
                Matchers.equalTo(true));
    }

    @AfterMethod
    public void refreshPage() {
        verificationFlowsHelper.refreshPage();
    }
}
