package com.mati.web.tests.dashboard.analytics;

import com.mati.enums.CountriesFilter;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AnalyticsFilterByLocation extends BaseWebTest {

    @Test(description = "С2793 - Map on the Analytics page responds selecting locations on the widget menu")
    public void analyticsFilterByLocationTest() {
        loginHelper
                .loginMatiSuccessful()
                .waitForPageLoad();
        analyticsHelper
                .openFilters()
                .setFilterByCountry(CountriesFilter.MEXICO)
                .applyFilter();

        int expectedVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int expectedVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int expectedRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        verificationListHelper
                .navigateToPage()
                .selectVerificationByName(VerificationInfoByCountry.MEXICO.getPersonName())
//
                .switchToNewView()
//
                .selectVerificationStatus(VerificationStatus.REJECTED)
                .waitSuccessfulAlertToBeVisibility();

        analyticsHelper
                .navigateToPage()
                .openFilters()
                .setFilterByCountry(CountriesFilter.MEXICO)
                .applyFilter();

        int actualVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int actualVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int actualRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(actualVerificationsAmount, expectedVerificationsAmount,
                "Verifications amount the same");
        softAssert.assertEquals(actualRejectedUsersAmount, expectedRejectedUsersAmount + 1,
                "Rejected amount user was increased by 1");
        softAssert.assertEquals(actualVerifiedUsersAmount, expectedVerifiedUsersAmount - 1,
                "Verified amount user was decreased by 1");
        softAssert.assertAll();
    }
}
