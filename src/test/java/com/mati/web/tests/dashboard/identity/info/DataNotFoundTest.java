package com.mati.web.tests.dashboard.identity.info;

import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.DriverUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DataNotFoundTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerification() {
        loginHelper.loginMatiSuccessful();
        analyticsHelper.waitForPageLoad();
        rollUpMenuHelper.selectVerificationFlowsMenu();
        verificationFlowsHelper.selectVerificationFlowByName(IConstants.AQA_FLOW_NAME);
        flowHelper.createVerification(VerificationInfoByCountry.CANADA, IConstants.frontCanada, IConstants.backCanada);
    }

    @Test(description = "C2681 - Data not found")
    public void isIdentityDeletedTest() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView();
        //
        String currentUrl = DriverUtils.getCurrentUrl();
        verificationObjectHelper.deleteVerification();
        open(currentUrl);

        assertThat("Verification not found button is displayed",
                verificationObjectHelper.isVerificationNotFoundBannerDisplayed(), equalTo(true));
    }
}
