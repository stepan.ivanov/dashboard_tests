package com.mati.web.tests.dashboard.login;

import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class LoginTest extends BaseWebTest {

    @BeforeClass
    public void navigateToLoginPage() {
        loginHelper.navigateToPage();
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isLanguageMenuDisplayedTest() {
        assertThat(
                "Language menu is not displayed",
                loginHelper.isLanguageMenuDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isMatiLogoDisplayedTest() {
        assertThat(
                "MATI logo is not displayed",
                loginHelper.isMatiLogoDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isContactUsLinkDisplayedTest() {
        assertThat(
                "'Contact Us' link is not displayed",
                loginHelper.isContactUsLinkDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isEmailErrorMessageDisplayedTest() {
        assertThat(
                "Email error message is displayed",
                loginHelper.isEmailErrorMessageDisplayed(),
                Matchers.equalTo(false));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isPasswordErrorMessageDisplayedTest() {
        assertThat(
                "Password error message is displayed",
                loginHelper.isPasswordErrorMessageDisplayed(),
                Matchers.equalTo(false));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isSignInTitleDisplayedTest() {
        assertThat(
                "'Sign In' title is not displayed",
                loginHelper.isSignInTitleDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isSignInButtonDisplayedTest() {
        assertThat(
                "'Sign In' button is not displayed",
                loginHelper.isSignInButtonDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isEmailFieldDisplayedTest() {
        assertThat(
                "Email field is not displayed",
                loginHelper.isEmailFieldDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isPasswordFieldDisplayedTest() {
        assertThat(
                "Password field is not displayed",
                loginHelper.isPasswordFieldDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isForgotPasswordLinkDisplayedTest() {
        assertThat(
                "Forgot password link is not displayed",
                loginHelper.isForgotPasswordLinkDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isFlexStartDisplayedTest() {
        assertThat(
                "'Flex start' part of page is not displayed",
                loginHelper.isFlexStartDisplayed(),
                Matchers.equalTo(true));
    }
}
