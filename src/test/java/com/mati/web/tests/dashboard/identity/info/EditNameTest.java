package com.mati.web.tests.dashboard.identity.info;

import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class EditNameTest extends IdentityBaseTest {

    private final String firstName = "Jhon";
    private final String surName = "Smith";
    private final String fullName = firstName + " " + surName;

    @Test(description = "C2683 - Edit name")
    public void editNameTest() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .scrollToEditDataButton()
                .clickEditDataButton()
                .fillFirstName(firstName)
                .fillLastName(surName)
                .fillFullName(fullName)
                .saveEdits()
                .waitSuccessfulAlertToBeVisibility();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(verificationObjectHelper.getFirstName(), firstName,
                "FirstName was edited");
        softAssert.assertEquals(verificationObjectHelper.getSurName(), surName,
                "Surname was edited");
        softAssert.assertEquals(verificationObjectHelper.getFullName(), fullName,
                "FullName was edited");
        softAssert.assertAll();
    }
}
