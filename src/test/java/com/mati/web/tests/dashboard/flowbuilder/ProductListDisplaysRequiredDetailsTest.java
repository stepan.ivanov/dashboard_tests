package com.mati.web.tests.dashboard.flowbuilder;

import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class ProductListDisplaysRequiredDetailsTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
        .deleteProductsFromFlowBuilder();
    }

    @Test(description = "C2717 - Product list displays required details")
    public void isNotProductsResizableTest() {
        assertThat(
                "The product list can be resizable",
                flowHelper.isNotProductsResizable(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2717 - Product list displays required details")
    public void isProductListScrollableTest() {
        assertThat(
                "The product list is not scrollable",
                flowHelper.isProductListScrollable(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2717 - Product list displays required details")
    public void isProductTitleDisplayedTest() {
        assertThat(
                "Product title is not displayed",
                flowHelper.isProductTitleDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2717 - Product list displays required details")
    public void isProductIconDisplayedTest() {
        assertThat(
                "Product icon is not displayed",
                flowHelper.isProductIconDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2717 - Product list displays required details")
    public void isUserInputsDisplayedTest() {
        assertThat(
                "User inputs are not displayed",
                flowHelper.isUserInputsDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2717 - Product list displays required details")
    public void isSDKOnlyBadgeDisplayedTest() {
        assertThat(
                "SDK Only badge is not displayed",
                flowHelper.isSDKOnlyBadgeDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2717 - Product list displays required details")
    public void isDependencyWizardDisplayedTest() {
        flowHelper.moveProductToFlowBuilder(Products.GOVERNMENT_CHECK);
        assertThat(
                "Dependency wizard is not displayed",
                flowHelper.isModalWindowDisplayed(),
                Matchers.equalTo(true)
        );
    }
}
