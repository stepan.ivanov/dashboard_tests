package com.mati.web.tests.dashboard.flowbuilder.products;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class ProductsRequireDocumentVerificationTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID);
    }

    @BeforeMethod
    public void deleteProductsFromFlowBuilder() {
        flowHelper.deleteProductsFromFlowBuilder();
    }

    @DataProvider
    public Object[] products() {
        return new Object[] {
                Products.ANTI_MONEY_LAUNDERING,
                Products.GOVERNMENT_CHECK
        };
    }

    @Test(description = "C2771 - AML requires Document Verification," +
                        "C2767 - Gov check requires Document Verification", dataProvider = "products")
    public void isProductRequiresDocumentVerificationTest(Products product) {
        boolean result = flowHelper.moveProductToFlowBuilder(product)
                .isModalWindowDisplayed();
        flowHelper.closeDependencyWizard();
        assertThat(
                "Product does not require Document Verification",
                result,
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2775 - Biometrics check requires Document Verification")
    public void isBiometricsRequiresDocumentVerificationTest() {
        flowHelper.moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .moveProductToFlowBuilder(Products.BIOMETRIC_VERIFICATION)
                .clickProductInFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .addDocumentToDVSettings("National ID", true)
                .clickProductSettingRadioButton(DocumentationSettings.CUSTOM_LIMIT)
                .clickSaveAndPublishButton()
                .clickBasketButtonIntoProduct(Products.BIOMETRIC_VERIFICATION);
        boolean actual = flowHelper.isModalWindowDisplayed();
        flowHelper.closeDependencyWizard();
        assertThat(
                "'Removing Biometrics would turn off Facematch Setting in Document Verification product' warning is not displayed.",
                actual,
                Matchers.equalTo(true)
        );
    }
}
