package com.mati.web.tests.dashboard.verificationlist;

import com.mati.enums.Period;
import com.mati.enums.rollup.Language;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CalendarLocalizedTest extends BaseWebTest {

    @BeforeClass
    public void login() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(dataProvider = "languages",
            description = "C14109 - Calendar in the application is localized to Spanish" +
                    "C1410 - Calendar in the application is localized to Portuguese")
    public void calendarLocalizedTest(Language language, String today, String yesterday,
                                      String last7day, String last30days, String lastWeek, String lastMonth, String thisWeek,
                                      String verificationByFlow, String verificationByStatus) {
        analyticsHelper.navigateToPage();
        rollUpMenuHelper
                .clickOnLanguageButton()
                .selectLanguage(language);
        verificationListHelper
                .navigateToPage()
                .openFilter();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(verificationListHelper.isFilterByPeriodDisplayed(today), true,
                String.format("Today label is equal : %s", today));
        softAssert.assertEquals(verificationListHelper.isFilterByPeriodDisplayed(yesterday), true,
                String.format("Yesterday label is equal : %s", yesterday));
        softAssert.assertEquals(verificationListHelper.isFilterByPeriodDisplayed(last7day), true,
                String.format("Last 7 days label is equal : %s", last7day));
        softAssert.assertEquals(verificationListHelper.isFilterByPeriodDisplayed(last30days), true,
                String.format("Last 30 days label is equal : %s", last30days));
        softAssert.assertEquals(verificationListHelper.isFilterByPeriodDisplayed(lastWeek), true,
                String.format("Last week label is equal : %s", lastWeek));
        softAssert.assertEquals(verificationListHelper.isFilterByPeriodDisplayed(lastMonth), true,
                String.format("Last month label is equal : %s", lastMonth));
        softAssert.assertEquals(verificationListHelper.isFilterByPeriodDisplayed(thisWeek), true,
                String.format("This week label is equal : %s", thisWeek));
        softAssert.assertEquals(verificationListHelper.getFilterByFlowLabel(), verificationByFlow,
                String.format("Filter verification by flow label is equal : %s", verificationByFlow));
        softAssert.assertEquals(verificationListHelper.getFilterByStatusLabel(), verificationByStatus,
                String.format("Filter verification by status label is equal : %s", verificationByStatus));
        softAssert.assertAll();
    }


    @DataProvider
    public Object[][] languages() {
        return new Object[][]{
                {
                        Language.SPANISH, Period.TODAY.getSpanishPeriod(), Period.YESTERDAY.getSpanishPeriod(),
                        Period.LAST_SEVEN_DAYS.getSpanishPeriod(), Period.LAST_THIRTY_DAYS.getSpanishPeriod(),
                        Period.LAST_WEEK.getSpanishPeriod(), Period.LAST_MONTH.getSpanishPeriod(),
                        Period.THIS_WEEK.getSpanishPeriod(), "Flujo de verificación", "Estado"
                },
                {
                        Language.PORTUGUESE, Period.TODAY.getPortuguesePeriod(), Period.YESTERDAY.getPortuguesePeriod(),
                        Period.LAST_SEVEN_DAYS.getPortuguesePeriod(), Period.LAST_THIRTY_DAYS.getPortuguesePeriod(),
                        Period.LAST_WEEK.getPortuguesePeriod(), Period.LAST_MONTH.getPortuguesePeriod(),
                        Period.THIS_WEEK.getPortuguesePeriod(), "Fluxo de verificação", "Status"
                },
        };
    }
}
