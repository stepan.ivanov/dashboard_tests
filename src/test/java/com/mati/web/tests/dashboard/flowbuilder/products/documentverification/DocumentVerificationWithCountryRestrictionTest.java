package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class DocumentVerificationWithCountryRestrictionTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerificationFlow() {
        ApiService.createFlow(RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0))
                .deleteProductFromFlowBuilder(Products.BIOMETRIC_VERIFICATION)
                .clickProductInFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .selectSettingCheckbox(DocumentationSettings.COUNTRY_RESTRICTION, true)
                .fillCountryRestriction("Canada")
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .createVerificationWithCountryRestriction(VerificationInfoByCountry.CANADA, VerificationInfoByCountry.MEXICO.getFrontSide(), VerificationInfoByCountry.MEXICO.getBackSide());
        verificationListHelper.navigateToPage().selectNewVerification();
    }

    @Test(description = "C2749 - Country restriction - specific countries can be selected for the flow")
    public void verificationHasIssuesTest() {
        assertThat(
                "Verification has invalid status after creation with incorrect documents!",
                verificationObjectHelper.isVerificationStatusCorrect(VerificationStatus.REVIEW_NEEDED),
                Matchers.equalTo(true)
        );
    }
}
