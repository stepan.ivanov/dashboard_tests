package com.mati.web.tests.dashboard.login;

import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class RetrieveLostPasswordTest extends BaseWebTest {

    boolean isRecoveryPasswordPageOpened;

    @BeforeClass
    public void passwordChangeRequest() {
        loginHelper.navigateToPage()
                .clickForgotPasswordLink();
        isRecoveryPasswordPageOpened = recoveryPasswordHelper.isRecoveryPasswordPage();
        recoveryPasswordHelper.fillEmailField(IConstants.testEmail)
                .clickSendResetEmailButton();
    }

    @Test(description = "C2629 - Retrieve lost password")
    public void isRecoveryPasswordPageOpenedTest() {
        assertThat("'Recovery Password' page was not open", isRecoveryPasswordPageOpened, Matchers.equalTo(true));
    }

    @Test(description = "C2629 - Retrieve lost password", enabled = false)
    public void isMessageComeToEmailTest() {
        WaitUtils.sleepSeconds(15);
        gmailHelper.openInputGmailMessages(IConstants.testEmail,IConstants.testGmailPassword);
        boolean isResetPasswordButtonDisplayed = gmailHelper.selectMessage().isResetPasswordButtonDisplayed();
        gmailHelper.deleteMessage();
        assertThat("Message for reset password was not come to email", isResetPasswordButtonDisplayed, Matchers.equalTo(true));
    }
}
