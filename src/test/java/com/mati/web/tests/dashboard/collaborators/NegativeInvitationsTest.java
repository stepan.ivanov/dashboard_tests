package com.mati.web.tests.dashboard.collaborators;

import com.mati.enums.Errors;
import com.mati.utils.EnvironmentConfig;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.*;

import static com.mati.enums.Errors.*;
import static com.mati.utils.RandomUtils.randomEmailWithPrefix;
import static com.mati.utils.RandomUtils.randomString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class NegativeInvitationsTest extends BaseWebTest {

    private final String firstName = randomString();
    private final String lastName = randomString();
    private final String email = randomEmailWithPrefix();

    @BeforeClass
    public void loginToMati() {
        loginHelper.loginMatiSuccessful();
    }

    @BeforeMethod
    public void openInvitationPanel() {
        analyticsHelper.navigateToPage();
        rollUpMenuHelper.clickOnInviteTeamButton();
    }

    @Test(dataProvider = "create-data",
            description = "C2663 - Empty fields in invite teammate form" +
                    "C2665 - No special characters allowed (```) in Invite Teammate form" +
                    "C2664 - Invalid email address in Invite teammate form")
    public void negativeInvitationsTest(String firstName, String lastName, String email, String message) {
        invitationPageHelper
                .fillFirstNameInput(firstName)
                .fillLastNameInput(lastName)
                .fillEmailField(email)
                .clickOnSendButton();

        assertThat(
                String.format("Error message '%s' is displayed", message),
                invitationPageHelper.isErrorMessageDisplayed(message),
                equalTo(true));
    }

    @Test(description = "C2666 - Email specified in Invite Teammate form is already in use")
    public void emailAlreadyInUsedTest() {
        invitationPageHelper
                .fillFirstNameInput(randomString())
                .fillLastNameInput(randomString())
                .fillEmailField(EnvironmentConfig.login)
                .clickOnSendButton();

        assertThat(
                String.format("Error message '%s' is displayed", Errors.EMAIL_ALREADY_IN_USE.getMessage()),
                analyticsHelper.getAlertMessage(),
                equalTo(Errors.EMAIL_ALREADY_IN_USE.getMessage()));
    }

    @DataProvider(name = "create-data")
    public Object[][] createDataWithEmptyValue() {
        return new Object[][]{
                {IConstants.emptyString, lastName, email, FIELD_IS_REQUIRED.getMessage()},
                {firstName, IConstants.emptyString, email, FIELD_IS_REQUIRED.getMessage()},
                {firstName, lastName, IConstants.emptyString, FIELD_IS_REQUIRED.getMessage()},
                {firstName + "~", lastName, email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName + "`", lastName, email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName + "^", lastName, email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName + "#", lastName, email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName + "%", lastName, email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName, lastName + "~", email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName, lastName + "`", email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName, lastName + "^", email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName, lastName + "#", email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName, lastName + "%", email, NO_SPECIAL_CHARACTERS.getMessage()},
                {firstName, lastName, IConstants.emailWithoutDog, INVALID_EMAIL_ADDRESS.getMessage()},
                {firstName, lastName, IConstants.emailWithDotAfterDog, INVALID_EMAIL_ADDRESS.getMessage()}
        };
    }
}
