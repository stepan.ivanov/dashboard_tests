package com.mati.web.tests.dashboard.identity.info;

import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CopyIdentityIdTest extends IdentityBaseTest {

    @Test(description = "C2685 - Copy Identity ID to clipboard")
    public void copyIdentityIdTest() throws IOException, UnsupportedFlavorException {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .clickCopyIdentityIdButton()
                .waitSuccessfulAlertToBeVisibility();

        String savedValue = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);

        assertThat("'Copy Identity Id' button is working",
                savedValue, equalTo(verificationObjectHelper.getIdentityId()));
    }
}
