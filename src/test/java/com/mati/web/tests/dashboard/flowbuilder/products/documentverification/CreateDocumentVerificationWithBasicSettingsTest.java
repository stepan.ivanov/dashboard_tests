package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class CreateDocumentVerificationWithBasicSettingsTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerificationFlow() {
        ApiService.createFlow(RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0))
                .deleteProductFromFlowBuilder(Products.BIOMETRIC_VERIFICATION);
    }

    @Test(description = "C2740 - Document Verification runs with basic settings")
    public void verificationWasCreatedSuccessfulTest() {
        flowHelper.clickVerifyMeButton()
                .createVerification(VerificationInfoByCountry.CANADA,
                        VerificationInfoByCountry.CANADA.getFrontSide(),
                        VerificationInfoByCountry.CANADA.getBackSide());
        verificationListHelper.navigateToPage();
        assertThat(
                "Document Verification was not created successful with basic settings",
                verificationListHelper.isVerificationExist(VerificationInfoByCountry.CANADA.getPersonName()),
                Matchers.equalTo(true)
        );
    }
}
