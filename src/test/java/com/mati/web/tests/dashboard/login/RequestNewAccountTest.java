package com.mati.web.tests.dashboard.login;

import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RequestNewAccountTest extends BaseWebTest {

    @Test(description = "C2631 - Request new account")
    public void requestNewAccountTest() {
        loginHelper
                .navigateToPage()
                .clickContactUsLink()
                .fillContactUsForm("test+test@mati.com", RandomUtils.randomString(), RandomUtils.randomString(),
                        RandomUtils.randomNumber(), RandomUtils.randomString())
                .clickSubmitButton();

        assertThat("Schedule frame is displayed",
                contactUsHelper.isScheduleFrameDisplayed(), equalTo(true));
    }
}
