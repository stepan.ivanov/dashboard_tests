package com.mati.web.tests.dashboard.signup;

import com.mati.utils.EnvironmentConfig;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.mati.enums.Errors.*;
import static com.mati.utils.RandomUtils.randomPersonalEmail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SignUpNegativeTest extends BaseWebTest {

    private final String assertInfo = "Error message %s is displayed";

    @BeforeMethod
    public void navigateToSignUpPage() {
        signUpHelper.navigateToPage();
    }

    @Test(dataProvider = "error-message",
            description = "C2639 - Empty fields" +
                    "C2640 - Wrong email - personal email is used" +
                    "C2641 - Wrong email - invalid email is used - missing @" +
                    "C2642 - Wrong email - invalid email is used - dot after @")
    public void negativeSignUpErrorMessage(String email, String password, String errorMessage) {
        signUpHelper
                .fillEmailField(email)
                .fillPasswordField(password)
                .clickSignInButton();

        assertThat(
                String.format(assertInfo, errorMessage),
                signUpHelper.isErrorMessageDisplayed(errorMessage),
                equalTo(true));
    }

    @Test(dataProvider = "error-message-span",
            description = "C2643 - Wrong password - invalid password is used" +
                    "C2644 - User already exists")
    public void negativeSignUpErrorMessageSpan(String email, String password, String errorMessage) {
        signUpHelper
                .fillEmailField(email)
                .fillPasswordField(password)
                .clickSignInButton();

        assertThat(
                String.format(assertInfo, errorMessage),
                signUpHelper.isErrorMessageSpanDisplayed(errorMessage),
                equalTo(true));
    }

    @DataProvider(name = "error-message")
    public Object[][] createDataForErrorMessage() {
        return new Object[][]{
                {IConstants.emptyString, IConstants.emptyString, FIELD_IS_REQUIRED.getMessage()},
                {randomPersonalEmail(), EnvironmentConfig.password, CAN_NOT_BE_PERSONAL_EMAIL.getMessage()},
                {IConstants.emailWithoutDog, EnvironmentConfig.password, INVALID_EMAIL_ADDRESS.getMessage()},
//                TODO inform QA team
//                {IConstants.emailWithDotAfterDog, EnvironmentConfig.password, INVALID_EMAIL_ADDRESS.getMessage()}
        };
    }

    @DataProvider(name = "error-message-span")
    public Object[][] createDataForErrorMessageSpan() {
        return new Object[][]{
                {EnvironmentConfig.login, "123", PASSWORD_NEED_TO_BE_AT_LEAST_8_CHAR.getMessage()},
                {EnvironmentConfig.login, EnvironmentConfig.password, EMAIL_ALREADY_IN_USE.getMessage()}
        };
    }
}
