package com.mati.web.tests.dashboard.identity.info;

import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class IdentityIncludeAllRequiredDataTest extends IdentityBaseTest {

    @Test(description = "C2680 - Identity details include all required data")
    public void identityIncludeAllRequiredData() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(verificationObjectHelper.isNameDisplayed(), true,
                "Name is displayed");
        softAssert.assertEquals(verificationObjectHelper.isDateOfBirthAgeDisplayed(), true,
                "Date of birth and age is displayed");
        softAssert.assertEquals(verificationObjectHelper.isIdentityIdDisplayed(), true,
                "Identity ID is displayed");
        softAssert.assertEquals(verificationObjectHelper.isIpGeolocationDisplayed(), true,
                "Ip Geolocation is displayed");
        softAssert.assertAll();
    }
}
