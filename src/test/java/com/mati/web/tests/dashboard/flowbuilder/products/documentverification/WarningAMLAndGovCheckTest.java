package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationflows.Products;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class WarningAMLAndGovCheckTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerificationFlow() {
        ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0));
    }

    @Test(description = "C2743 - Proof of residency as the only document")
    public void warningAMLAndGovCheckWonNotWorkTest() {
        flowHelper.moveProductToFlowBuilder(Products.GOVERNMENT_CHECK)
                .clickProductInFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .deleteDocumentStep(1)
                .addDocumentToDVSettings("Proof of residency", true);
        assertThat(
                "'Products Gov Check and AML won't work with this type of document' warning is not displayed",
                flowHelper.isIncorrectTypeDocumentWarningDisplayed(),
                Matchers.equalTo(true)
        );
    }

}
