package com.mati.web.tests.dashboard.collaborators;

import com.mati.enums.Role;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.mati.enums.Role.ADMIN;
import static com.mati.enums.Role.AGENT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class InvitationsTest extends BaseWebTest {

    @BeforeClass
    public void logIn() {
        loginHelper.loginMatiSuccessful();
    }

    @BeforeMethod
    public void openInvitationPanel() {
        analyticsHelper.navigateToPage();
        rollUpMenuHelper.clickOnInviteTeamButton();
    }

    @Test(description = "C2657 - 'Invite teammate' button opens the agent invitation screen.")
    public void isInvitationScreenDisplayed() {
        assertThat(
                "InvitationScreen is displayed",
                invitationPageHelper.isInvitationScreenDisplayed(),
                equalTo(true));
    }

    @Test(description = "C2658 - 'Invite teammate' screen displays required information, fields, and controls.")
    public void isRequiredInformationFieldsAndControlsDisplayed() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(invitationPageHelper.isFirstNameFieldDisplayed(), true, "FirstName field is displayed");
        softAssert.assertEquals(invitationPageHelper.isLastNameFieldDisplayed(), true, "LastName field is displayed");
        softAssert.assertEquals(invitationPageHelper.isEmailDisplayed(), true, "Email field is displayed");
        softAssert.assertEquals(invitationPageHelper.isSendButtonDisplayed(), true, "Send button is displayed");
        softAssert.assertEquals(invitationPageHelper.isCancelButtonDisplayed(), true, "Cancel button is displayed");
        softAssert.assertEquals(invitationPageHelper.isAdminRadioButtonDisplayed(), true, "Admin radio button is displayed");
        softAssert.assertEquals(invitationPageHelper.isAgentRadioButtonDisplayed(), true, "Agent radio button is displayed");
        softAssert.assertAll();
    }

    @Test(dataProvider = "role-provider",
            description = "C2661 - Agent Role changes through the Role select box.")
    public void roleIsChangedTest(Role role) {
        invitationPageHelper
                .setRole(role);

        assertThat(String.format("Role should be: %s", role.name()),
                invitationPageHelper.isRoleChecked(role),
                equalTo(true));
    }

    @DataProvider(name = "role-provider")
    public Object[][] createDataWithEmptyValue() {
        return new Object[][]{
                {ADMIN},
                {AGENT}
        };
    }
}