package com.mati.web.tests.dashboard.login;

import com.mati.web.DriverUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CloseSessionTest extends BaseWebTest {

    @Test(description = "C2710 - Make sure the session is closed")
    public void closeSessionTest() {
        loginHelper.loginMatiSuccessful();
        String currentUrl = DriverUtils.getCurrentUrl();
        rollUpMenuHelper
                .clickLogoutButton()
                .clickConfirmPopUpButton();
        DriverUtils.openUrl(currentUrl);

        assertThat(
                "Login page is opened",
                loginHelper.isLoginPage(),
                equalTo(true));
    }
}
