package com.mati.web.tests.dashboard.language;

import com.mati.enums.rollup.Language;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;

public class LanguageTest extends BaseWebTest {

    @BeforeClass
    public void loginToLanguagePage() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(description = "C2709 - All UI items are properly translated")
    public void isLanguageSelectorPresentTest() {
        assertThat(
                "Language button is not displayed in 'RollUp Menu'",
                rollUpMenuHelper.isLanguageButtonDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2709 - All UI items are properly translated")
    public void isLanguageListCorrectTest() {
        rollUpMenuHelper.clickOnLanguageButton();
        List<String> actual = rollUpMenuHelper.getLanguagesList();
        List<String> expected = List.of(
                Language.ENGLISH.getLanguage(),
                Language.SPANISH.getLanguage(),
                Language.PORTUGUESE.getLanguage()
        );
        assertThat(
                "'Review mode' button is disabled on 'Analytics' page",
                actual,
                Matchers.equalTo(expected));
    }

    @Test(dataProvider = "languages", description = "C2709 - All UI items are properly translated")
    public void rollUpMenuLanguageVersionTest(Language language, Map expected) {
        analyticsHelper.navigateToPage();
        rollUpMenuHelper.clickOnLanguageButton().selectLanguage(language);
        String rollUpMenuElementText = rollUpMenuHelper.getRollUpMenuElementText();
        String analyticsElementText = rollUpMenuHelper.getAnalyticsElementText();
        String verificationListElementText = rollUpMenuHelper.getVerificationListElementText();
        String verificationFlowsElementText = rollUpMenuHelper.getVerificationFlowsElementText();
        String forDevelopersElementText = rollUpMenuHelper.getForDevelopersElementText();
        String faqElementText = rollUpMenuHelper.getFAQElementText();
        String whatsNewElementText = rollUpMenuHelper.getWhatsNewElementText();
        String inviteTeammateElementText = rollUpMenuHelper.getInviteTeammateElementText();
        String settingsElementText = rollUpMenuHelper.getSettingsElementText();
        String logoutElementText = rollUpMenuHelper.getLogoutElementText();
        Map actual =
                Map.ofEntries(
                        Map.entry("rollUpMenu", rollUpMenuElementText),
                        Map.entry("analytics", analyticsElementText),
                        Map.entry("verificationList", verificationListElementText),
                        Map.entry("verificationFlow", verificationFlowsElementText),
                        Map.entry("forDevelopers", forDevelopersElementText),
                        Map.entry("helpCenter", faqElementText),
                        Map.entry("whatsNew", whatsNewElementText),
                        Map.entry("inviteTeammate", inviteTeammateElementText),
                        Map.entry("settings", settingsElementText),
                        Map.entry("logout", logoutElementText));

        assertThat(
                "Language values is not correct after choose another language",
                actual,
                Matchers.equalTo(expected));
    }

    @DataProvider
    public Object[][] languages() {
        return new Object[][] {
            {
                Language.ENGLISH,
                Map.ofEntries(
                        Map.entry("rollUpMenu", "Roll up menu"),
                        Map.entry("analytics", "Analytics"),
                        Map.entry("verificationList", "Verification List"),
                        Map.entry("verificationFlow", "Verification Flows"),
                        Map.entry("forDevelopers", "For Developers"),
                        Map.entry("helpCenter", "Help Center"),
                        Map.entry("whatsNew", "What's new"),
                        Map.entry("inviteTeammate", "Invite a teammate"),
                        Map.entry("settings", "Settings"),
                        Map.entry("logout", "Logout"))
            },
            {
                Language.SPANISH,
                Map.ofEntries(
                        Map.entry("rollUpMenu", "Menú"),
                        Map.entry("analytics", "Analíticas"),
                        Map.entry("verificationList", "Lista de verificaciones"),
                        Map.entry("verificationFlow", "Flujos de verificación"),
                        Map.entry("forDevelopers", "Para desarrolladores"),
                        Map.entry("helpCenter", "Centro de ayuda"),
                        Map.entry("whatsNew", "Qué hay de nuevo"),
                        Map.entry("inviteTeammate", "Invitar compañero"),
                        Map.entry("settings", "Configuración"),
                        Map.entry("logout", "Cerrar Sesión"))
            },
            {
                Language.PORTUGUESE,
                Map.ofEntries(
                        Map.entry("rollUpMenu", "Recolher menu"),
                        Map.entry("analytics", "Análise"),
                        Map.entry("verificationList", "Lista de Verificação"),
                        Map.entry("verificationFlow", "Fluxos de Verificação"),
                        Map.entry("forDevelopers", "Para Desenvolvedores"),
                        Map.entry("helpCenter", "Centro de ajuda"),
                        Map.entry("whatsNew", "O que há de novo"),
                        Map.entry("inviteTeammate", "Convide um colega"),
                        Map.entry("settings", "Configurações"),
                        Map.entry("logout", "Logout"))
            }
        };
    }

    @Test(description = "C2708 - Change language selector changes the language for the user", enabled = false)
    public void changeLanguageAndLogOutAndLogInTest() {
        analyticsHelper.navigateToPage();
        rollUpMenuHelper
                .clickOnLanguageButton()
                .selectLanguage(Language.SPANISH)
                .clickLogoutButton()
                .clickConfirmPopUpButton();
        loginHelper.loginMatiSuccessful();
        String actual = rollUpMenuHelper.getRollUpMenuElementText();
        assertThat(
                "Language is not saved after user logout",
                actual,
                Matchers.equalTo("Menú"));
    }
}
