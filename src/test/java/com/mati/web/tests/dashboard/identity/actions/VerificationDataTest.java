package com.mati.web.tests.dashboard.identity.actions;

import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class VerificationDataTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerification() {
        loginHelper.loginMatiSuccessful();
        analyticsHelper.waitForPageLoad();
        rollUpMenuHelper.selectVerificationFlowsMenu();
        verificationFlowsHelper.selectVerificationFlowByName(IConstants.AQA_FLOW_NAME);
        flowHelper.createVerification(VerificationInfoByCountry.CANADA, IConstants.frontCanada, IConstants.backCanada);
    }

    @Test(description = "C2695 - Verification Data button")
    public void verificationDataTest() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .clickOnVerificationDataButton()
                .waitPageLoaded();

        assertThat("Verification Modal window is displayed",
                verificationDataObjectModelHelper.isModalWindowDisplay(), equalTo(true));
    }

    @AfterMethod(alwaysRun = true)
    public void deleteVerification() {
        verificationListHelper
                .navigateToPage()
                .deleteVerificationByName(IConstants.verificationNameForCanada);
    }
}
