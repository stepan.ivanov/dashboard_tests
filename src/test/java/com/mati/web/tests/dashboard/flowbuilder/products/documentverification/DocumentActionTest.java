package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class DocumentActionTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID);
    }

    @DataProvider
    public Object[] documents() {
        return new Object[]{"Passport", "National ID", "Driving License", "Proof of residency"};
    }

    @Test(description = "C2741 - Documents can be added to and removed from the flow", dataProvider = "documents")
    public void stepCanBeDeletedTest(String document) {
        flowHelper.deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION);
        flowHelper.addDocumentToDVSettings(document, true).deleteDocumentStep(1);
        assertThat(
                "Step can not be deleted from document settings",
                flowHelper.isDocumentDisplayed(document),
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2741 - Documents can be added to and removed from the flow", dataProvider = "documents")
    public void documentCanBeDeletedFromStepTest(String document) {
        flowHelper.deleteProductFromFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .clickAddStepButton()
                .selectDocumentsCheckbox("Passport", true)
                .selectDocumentsCheckbox("National ID", true)
                .selectDocumentsCheckbox("Driving License", true)
                .selectDocumentsCheckbox("Proof of residency", true)
                .clickAddDocumentButton()
                .clickEditStepButton(1)
                .selectDocumentsCheckbox(document, false)
                .clickSaveChangesButton();
        assertThat(
                "Document can not be deleted from steps in document settings",
                flowHelper.isDocumentDisplayed(document),
                Matchers.equalTo(false)
        );
    }
}
