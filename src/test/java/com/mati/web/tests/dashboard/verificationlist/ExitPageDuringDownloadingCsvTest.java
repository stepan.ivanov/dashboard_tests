package com.mati.web.tests.dashboard.verificationlist;

import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ExitPageDuringDownloadingCsvTest extends BaseWebTest {

    @Test(description = "C9210 - Possible to exit page while downloading CSV")
    public void exitPageDuringDownloadingTest() {
        loginHelper.loginMatiSuccessful();
        verificationListHelper
                .navigateToPage()
                .downloadVerificationCSV();

        rollUpMenuHelper
                .selectAnalyticsMenu()
                .waitForPageLoad();

        assertThat("Download CSV spinner is displayed",
                verificationListHelper.isCsvLoadingSpinnerDisplayed(), equalTo(true));
    }
}
