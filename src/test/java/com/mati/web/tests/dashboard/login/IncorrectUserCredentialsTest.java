package com.mati.web.tests.dashboard.login;

import com.mati.enums.Errors;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class IncorrectUserCredentialsTest extends BaseWebTest {

    @BeforeMethod
    public void navigateToLoginPage() {
        loginHelper.navigateToPage();
    }

    @Test(description = "C2634 - Wrong Email")
    public void isEmailErrorMessageCorrectTest() {
        loginHelper
                .fillEmailField("!"+RandomUtils.randomEmailWithPrefix())
                .fillPasswordField(EnvironmentConfig.password)
                .clickSignInButton();
        assertThat(
                "Error message is not correct for incorrect email and correct password",
                loginHelper.getEmailErrorMessage(),
                Matchers.equalTo(Errors.INVALID_EMAIL_ADDRESS.getMessage()));
    }

    @Test(description = " C2634 - Wrong Password")
    public void isPasswordErrorMessageCorrectTest() {
        loginHelper
                .fillEmailField(EnvironmentConfig.login)
                .fillPasswordField(RandomUtils.randomString())
                .clickSignInButton();
        assertThat(
                "Error message is not correct for incorrect email and correct password",
                loginHelper.getPasswordErrorMessage(),
                Matchers.equalTo(Errors.INCORRECT_CREDENTIALS.getMessage()));
    }
}
