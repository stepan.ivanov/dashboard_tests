package com.mati.web.tests.dashboard.flowbuilder.graphui;

import com.codeborne.selenide.WebDriverRunner;
import com.mati.enums.verificationflows.Products;
import com.mati.utils.RandomUtils;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class SaveAndPublishFlowTest extends BaseWebTest {

    private final String flowName = IConstants.prefix + RandomUtils.randomString();

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        verificationFlowsHelper.navigateToPage().createFlow(flowName);
    }

    @BeforeMethod
    public void selectFlow() {
        rollUpMenuHelper.selectVerificationFlowsMenu()
                .selectVerificationFlowByName(flowName)
                .deleteProductsFromFlowBuilder();
    }

    @Test(description = "C2736 - Save & Publish button saves changes / displays unsaved data warning ")
    public void isUnsavedChangesAlertDisplayedTest() {
        flowHelper.moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION);
        assertThat(
                "Alert message is not displayed for unsaved changes",
                flowHelper.isUnsavedChangesAlertDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2736 - Save & Publish button saves changes / displays unsaved data warning ")
    public void isSaveAndPublishButtonDisabledTest() {
        flowHelper.moveProductToFlowBuilder(Products.BIOMETRIC_VERIFICATION).clickSaveAndPublishButton();
        WaitUtils.sleepSeconds(5);
        assertThat(
                "Save and publish button is enabled",
                flowHelper.isSaveAndPublishButtonEnabled(),
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2736 - Save & Publish button saves changes / displays unsaved data warning")
    public void isSavedButtonDisabledWithoutProductsTest() {
        flowHelper.deleteProductsFromFlowBuilder();
        assertThat(
                "Save and publish button is enabled without products",
                flowHelper.isSaveAndPublishButtonEnabled(),
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2736 - Save & Publish button saves changes / displays unsaved data warning")
    public void isConfirmMessageOfClosingPageDisplayed() {
        flowHelper.moveProductToFlowBuilder(Products.CUSTOM_DOCUMENT);
        WebDriverRunner.getWebDriver().navigate().refresh();
        boolean result = flowHelper.isAlertPopupDisplayed();
        flowHelper.closeAlertPopup();
        assertThat(
                "Confirm message is not displayed for refreshing page",
                result,
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2736 - Save & Publish button saves changes / displays unsaved data warning ")
    public void isSaveAndPublishButtonTurnOffTest() {
        flowHelper.selectApiIntegration().moveProductToFlowBuilder(Products.PHONE_CHECK);
        assertThat(
                "Save and publish button is disabled when we have a problem on flow page",
                flowHelper.isSaveAndPublishButtonEnabled(),
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2736 - Save & Publish button saves changes / displays unsaved data warning ")
    public void isIssuesMessageDisplayedTest() {
        flowHelper.selectApiIntegration().moveProductToFlowBuilder(Products.IP_CHECK);
        assertThat(
                "'You have issues' message is not disabled when we have a problem on flow page",
                flowHelper.isIssuesMessageDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2736 - Save & Publish button saves changes / displays unsaved data warning ")
    public void isSDKOnlyProductsInFlowBuilderTest() {
        flowHelper.selectApiIntegration().moveProductToFlowBuilder(Products.EMAIL_CHECK);
        assertThat(
                "Products with SDK only label can not move to flow builder when API integration is selected",
                flowHelper.isProductMovedToFlowBuilder(Products.EMAIL_CHECK),
                Matchers.equalTo(true)
        );
    }

    @AfterClass
    public void deleteTestFlow() {
        rollUpMenuHelper.selectVerificationFlowsMenu().removeFlowByName(flowName);
    }
}
