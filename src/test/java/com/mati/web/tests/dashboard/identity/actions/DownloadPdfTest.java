package com.mati.web.tests.dashboard.identity.actions;

import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DownloadPdfTest extends IdentityBaseTest {

    @Test(description = "C2694 - Download PDF button")
    public void downloadPdfTest() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView();
                //

        String verificationNumber = verificationObjectHelper.getVerificationNumber();
        verificationObjectHelper.downloadVerificationPDF();

        // Wait until PDF is downloading
        WaitUtils.sleepSeconds(30);
        boolean result = new File(String.format("target/mati-identity-%s.pdf", verificationNumber)).exists();
        assertThat("Pdf was downloaded", result, equalTo(true));
    }
}
