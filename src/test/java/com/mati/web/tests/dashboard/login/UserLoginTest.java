package com.mati.web.tests.dashboard.login;

import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class UserLoginTest extends BaseWebTest {

    @BeforeClass
    public void loginUser() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(description = "C2627 - New user login, C2628 - Existing user login")
    public void isMerchantDashboardOpenedTest() {
        assertThat("Current page is not 'Merchant Dashboard'", analyticsHelper.isAnalyticsPage(), Matchers.equalTo(true));
    }

}
