package com.mati.web.tests.dashboard.verificationlist;

import com.codeborne.selenide.WebDriverRunner;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class VerificationListCanBeLeftTest extends BaseWebTest {

    @Test(description = "C8524 - Verification List page can be left while downloading CSV")
    public void verificationListCanBeLeftTest() {
        loginHelper.loginMatiSuccessful();
        verificationListHelper
                .navigateToPage()
                .downloadVerificationCSV();

        rollUpMenuHelper
                .selectAnalyticsMenu()
                .waitForPageLoad();

        WebDriverRunner.getWebDriver().navigate().refresh();

        assertThat("Alert is present",
                verificationListHelper.isAlertPresent(), equalTo(true));
    }
}
