package com.mati.web.tests.dashboard.flowbuilder.products;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.BiometricsVerificationSettings;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.mati.enums.verificationflows.settings.BiometricsVerificationSettings.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class BiometricsSettingsTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.BIOMETRIC_VERIFICATION);
    }

    @DataProvider
    public Object[] settings() {
        return new Object[] {
                SELFIE_VIDEO,
                SELFIE_VIDEO_VOICE,
                SELFIE_PHOTO
        };
    }

    @Test(description = "C2774 - Required settings are available", dataProvider = "settings")
    public void isRequiredSettingsAvailableTest(BiometricsVerificationSettings setting) {
        assertThat(
                "Required setting is not available for BIOMETRICS VERIFICATION product",
                flowHelper.isProductSettingDisplayed(setting),
                Matchers.equalTo(true)
        );
    }

    @DataProvider
    public Object[] selectiveSettings() {
        return new Object[] {
                SELFIE_VIDEO,
                SELFIE_PHOTO
        };
    }

    @Test(description = "C2774 - Required settings are available", dataProvider = "selectiveSettings")
    public void isSettingSelectiveTest(BiometricsVerificationSettings setting) {
        assertThat(
                "Product setting is not selective in BIOMETRICS VERIFICATION product",
                flowHelper.isProductSettingSelective(setting),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2774 - Required settings are available")
    public void isSettingSwitchableTest() {
        assertThat(
                "'Selfie Video + Voice' product setting is not switchable in BIOMETRICS VERIFICATION product",
                flowHelper.isProductSettingCheckBoxSwitchable(SELFIE_VIDEO_VOICE),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2774 - Required settings are available")
    public void isNotWorkInMobileWarningDisplayedTest() {
        assertThat(
                "'Does not work in mob version' warning is not displayed",
                flowHelper.isWarningWithContentDisplayed("If you use mobile integrations please upgrade to 2.9.0 (iOS) " +
                        "and 2.9.3 (Android) or higher to prevent breaking the flow"),
                Matchers.equalTo(true)
        );
    }
}
