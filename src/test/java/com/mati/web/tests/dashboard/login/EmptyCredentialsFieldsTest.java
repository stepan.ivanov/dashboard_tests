package com.mati.web.tests.dashboard.login;

import com.mati.enums.Errors;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class EmptyCredentialsFieldsTest extends BaseWebTest {

    @BeforeClass
    public void clickSignInButton() {
        loginHelper.navigateToPage()
                .clickSignInButton();
    }

    @Test(description = "C2632 - Empty fields")
    public void isEmailFieldErrorMessageDisplayedTest() {
        assertThat("Error message is not displayed for empty 'email' field", loginHelper.isEmailErrorMessageDisplayed(), Matchers.equalTo(true));
    }

    @Test(description = "C2632 - Empty fields")
    public void isEmailErrorMessageCorrectTest() {
        assertThat("Error message is not correct for empty email field", loginHelper.getEmailErrorMessage(), Matchers.equalTo(Errors.FIELD_IS_REQUIRED.getMessage()));
    }
}
