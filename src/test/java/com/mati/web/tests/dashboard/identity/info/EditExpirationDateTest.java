package com.mati.web.tests.dashboard.identity.info;

import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class EditExpirationDateTest extends BaseWebTest {


    @BeforeClass
    public void loginAndCreateVerification() {
        loginHelper.loginMatiSuccessful();
        analyticsHelper.waitForPageLoad();
        rollUpMenuHelper.selectVerificationFlowsMenu();
        verificationFlowsHelper.selectVerificationFlowByName(IConstants.AQA_FLOW_NAME);
        flowHelper.createVerification(VerificationInfoByCountry.CANADA, IConstants.frontCanada, IConstants.backCanada);
    }

    @BeforeMethod
    public void openVerification() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView();
        //
    }

    @Test(description = "C8439 - The filed 'Expiration Date' could be edited" +
            "C8440 - The field 'Expiration Date' could be empty" +
            "C8441 - The fields 'Day' and 'Month' (Expiration Date) could enter a maximum of 2 characters\n" +
            "C8442 - The field 'Year' (Expiration Date) could enter a maximum of 4 characters",
            dataProvider = "data-provider")
    public void editDateOfExpirationTest(String day, String month, String year, String expectedDate) {
        verificationObjectHelper
                .scrollToEditDataButton()
                .clickEditDataButton()
                .fillDayOfExpirationDate(day)
                .fillMonthOfExpirationDate(month)
                .fillYearOfExpirationDate(year)
                .saveEdits()
                .waitSuccessfulAlertToBeVisibility()
                .scrollToEditDataButton();

        assertThat(String.format("Date should be: %s", expectedDate),
                verificationObjectHelper.getDateOfExpiration().trim(), equalTo(expectedDate));
    }

    @AfterClass
    public void deleteVerification() {
        verificationObjectHelper.deleteVerification();
    }

    @DataProvider(name = "data-provider")
    public Object[][] createData() {
        return new Object[][]{
                {"22", "01", "1992", "22 01 1992"},
                {"", "", "", "- - -"},
                {"222", "011", "19922", "22 01 1992"}
        };
    }
}

