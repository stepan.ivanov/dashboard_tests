package com.mati.web.tests.dashboard.identity.info;

import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class VerificationHasDocumentTest extends IdentityBaseTest {

    @Test(description = "C8443 - 'Document 1' has a picture")
    public void hasVerificationDocumentTest() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
        //
                .clickDocumentVerificationButton();

        assertThat("National Id is displayed",
                verificationObjectHelper.isDocumentImageDisplayed(), equalTo(true));
    }
}
