package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class RemovingDocumentVerificationWarningTest extends BaseWebTest {

    @BeforeClass
    public void navigateToFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .moveProductToFlowBuilder(Products.GOVERNMENT_CHECK)
                .moveProductToFlowBuilder(Products.ANTI_MONEY_LAUNDERING)
                .clickBasketButtonIntoProduct(Products.DOCUMENT_VERIFICATION);
    }

    @Test(description = " C2753 - Removing Document Verification pops a warning")
    public void removingDocumentVerificationWarningPopupIsAppearedTest() {
        assertThat(
                "Removing Document Verification does not pop a warning",
                flowHelper.isModalWindowDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = " C2753 - Removing Document Verification pops a warning")
    public void isAMLProductIntoPopupTest() {
        assertThat(
                "'AML' product is not into 'Warning' popup",
                flowHelper.isProductIntoWarningPopup(Products.ANTI_MONEY_LAUNDERING),
                Matchers.equalTo(true)
        );
    }

    @Test(description = " C2753 - Removing Document Verification pops a warning")
    public void isGovCheckProductIntoPopupTest() {
        assertThat(
                "'Gov check' product is not into 'Warning' popup",
                flowHelper.isProductIntoWarningPopup(Products.GOVERNMENT_CHECK),
                Matchers.equalTo(true)
        );
    }
}
