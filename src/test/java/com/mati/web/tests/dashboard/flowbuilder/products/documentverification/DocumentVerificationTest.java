package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class DocumentVerificationTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .clickAddStepButton()
                .selectDocumentsCheckbox("Passport", true)
                .selectDocumentsCheckbox("National ID", true)
                .selectDocumentsCheckbox("Driving License", true)
                .selectDocumentsCheckbox("Proof of residency", true)
                .clickAddDocumentButton()
                .selectSettingCheckbox(DocumentationSettings.AGE_THRESHOLD, true);
    }

    @DataProvider
    public Object[] documents() {
        return new Object[] {"Passport", "National ID", "Driving License", "Proof of residency"};
    }

    @Test(description = "C2742 - All required documents can be added", dataProvider = "documents")
    public void requiredDocumentsCanBeAddedTest(String document) {
        assertThat(
                "Document can not be added to verification list in 'Document Verification' product settings",
                flowHelper.isDocumentDisplayed(document),
                Matchers.equalTo(true)
        );
    }

    @DataProvider
    public Object[] invalidAges() {
        return new Object[] {"0", "1", "14", "101"};
    }

    @Test(description = "C2746 - Age can be restricted by default or manually", dataProvider = "invalidAges")
    public void verifyInvalidAgesTest(String age) {
        flowHelper.fillAgeThresholdInput(age);
        assertThat(
                "'Age Threshold' value is valid and error is not displayed",
                flowHelper.isErrorAgeThresholdDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @DataProvider
    public Object[] validAges() {
        return new Object[] {"15", "99", "100"};
    }

    @Test(description = "C2746 - Age can be restricted by default or manually", dataProvider = "validAges")
    public void verifyValidAgesTest(String age) {
        flowHelper.fillAgeThresholdInput(age);
        assertThat(
                "'Age Threshold' value is not valid",
                flowHelper.isErrorAgeThresholdDisplayed(),
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2744 - One document cannot be selected more than once")
    public void documentCanBeSelectedOnceTest() {
        assertThat(
                "One document can be selected more than once for verification",
                flowHelper.isAddStepButtonNotDisplayed(),
                Matchers.equalTo(false)
        );
    }
}
