package com.mati.web.tests.dashboard.collaborators;

import com.mati.enums.Role;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.*;

import static com.mati.enums.Role.ADMIN;
import static com.mati.enums.Role.AGENT;
import static com.mati.utils.RandomUtils.randomString;
import static com.mati.utils.RandomUtils.randomTestPersonalEmailWithPrefix;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class InviteTeamMember extends BaseWebTest {

    private final String successMessage = "Your teammate was invited";
    private String email;

    @BeforeClass
    public void logInToGmailAndMati() {
        gmailHelper.openInputGmailMessages(IConstants.testEmailPersonal, IConstants.passwordForTestEmailPersonal);
        loginHelper.loginMatiSuccessful();
    }

    @Test(dataProvider = "role-provider",
            description = "C2659 - On 'Send', invitation email is sent to the new agent.")
    public void inviteMember(Role role) {
        email = randomTestPersonalEmailWithPrefix();

        analyticsHelper.navigateToPage();
        rollUpMenuHelper.clickOnInviteTeamButton();

        invitationPageHelper
                .inviteNewTeamMember(randomString(), randomString(), email, role);

        assertThat(
                String.format("Alert with message: '%s' is displayed ", successMessage),
                analyticsHelper.getAlertMessage(),
                equalTo(successMessage)
        );

        WaitUtils.sleepSeconds(15);
        gmailHelper
                .navigateToPage()
                .selectMessage();
        assertThat(
                String.format("Email for : '%s' is present ", email),
                gmailHelper.isEmailForPresent(email),
                equalTo(true)
        );
    }

    @AfterMethod
    public void deleteUser() {
        settingsPageHelper
                .navigateToPage()
                .deleteUserByEmail(email);
    }

    @DataProvider(name = "role-provider")
    public Object[][] createDataWithEmptyValue() {
        return new Object[][]{
                {ADMIN},
                {AGENT}
        };
    }
}
