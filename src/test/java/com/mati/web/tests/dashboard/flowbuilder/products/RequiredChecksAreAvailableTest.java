package com.mati.web.tests.dashboard.flowbuilder.products;

import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class RequiredChecksAreAvailableTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder();
    }

    @DataProvider
    public Object[][] checks() {
        return new Object[][] {
                {
                        Products.DOCUMENT_VERIFICATION,
                        Arrays.asList(
                                "Document Reading",
                                "Expiration detection",
                                "Template matching",
                                "Age threshold",
                                "Duplicate user detection",
                                "Alteration detection",
                                "Facematch"
                        )
                },
                {
                        Products.BIOMETRIC_VERIFICATION,
                        Arrays.asList(
                                "Movement Liveness check",
                                "Voice Liveness check"
                        )
                },
                {
                        Products.ANTI_MONEY_LAUNDERING,
                        Arrays.asList(
                                "Watchlist check",
                                "Premium watchlist check",
                                "Premium watchlist monitoring"
                        )
                },
                {
                        Products.GOVERNMENT_CHECK,
                        Arrays.asList(
                                "Government database checks"
                        )
                },
                {
                        Products.IP_CHECK,
                        Arrays.asList(
                                "IP check",
                                "Geo-restriction check",
                                "VPN/Proxy check"
                        )
                }
        };
    }

    @Test(description = "C2739 - All checks included, C2773, C2769, C2765, C2777 - Required checks are available", dataProvider = "checks")
    public void isProductChecksAvailableTest(Products product, List<String> checks) {
        assertThat(
                "Product checks are not available in product settings",
                flowHelper.moveProductToFlowBuilder(product).isProductCheckDisplayed(checks),
                Matchers.equalTo(true)
        );
    }
}
