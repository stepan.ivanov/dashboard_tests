package com.mati.web.tests.dashboard.flowbuilder.products;

import com.mati.enums.verificationflows.settings.AntiMoneySettings;
import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.mati.enums.verificationflows.settings.AntiMoneySettings.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class AMLSettingsTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.ANTI_MONEY_LAUNDERING)
                .confirmAddingProductToFlowBuilder()
                .clickProductInFlowBuilder(Products.ANTI_MONEY_LAUNDERING);
    }

    @DataProvider
    public Object[] settings() {
        return new Object[] {SEARCH, MONITORING};
    }

    @Test(description = "C2770 - Required settings are available ", dataProvider = "settings")
    public void isProductSettingAvailableTest(AntiMoneySettings setting) {
        assertThat(
                "Product setting is not available in AML product settings",
                flowHelper.isProductSettingDisplayed(setting),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2770 - Required settings are available ", dataProvider = "settings")
    public void isProductSettingToggleableTest(AntiMoneySettings setting) {
        assertThat(
                "Product setting checkbox is not switchable in AML product settings",
                flowHelper.isProductSettingCheckBoxSwitchable(setting),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2770 - Required settings are available")
    public void isSearchSettingTurnOnIndependentlyTest() {
        boolean result = flowHelper.selectSettingCheckbox(SEARCH, false)
                .selectSettingCheckbox(MONITORING, false)
                .selectSettingCheckbox(SEARCH, true)
                .isCheckBoxSelected(MONITORING);
        assertThat(
                "'Search' setting can not be turned on independently in AML product settings",
                result,
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2770 - Required settings are available")
    public void isMonitoringSettingTurnOnIndependentlyTest() {
        boolean result = flowHelper.selectSettingCheckbox(SEARCH, false)
                .selectSettingCheckbox(MONITORING, false)
                .selectSettingCheckbox(MONITORING, true)
                .isCheckBoxSelected(SEARCH);
        assertThat(
                "'Monitoring' setting can not be turned on independently in AML product settings",
                result,
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2770 - Required settings are available")
    public void isSearchSettingTurnOffIndependentlyTest() {
        boolean result = flowHelper.selectSettingCheckbox(SEARCH, true)
                .selectSettingCheckbox(MONITORING, true)
                .selectSettingCheckbox(SEARCH, false)
                .isCheckBoxSelected(MONITORING);
        assertThat(
                "'Search' setting can not be turned off independently in AML product settings",
                result,
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2770 - Required settings are available")
    public void isMonitoringSettingTurnOffIndependentlyTest() {
        boolean result = flowHelper.selectSettingCheckbox(SEARCH, true)
                .selectSettingCheckbox(MONITORING, true)
                .selectSettingCheckbox(MONITORING, false)
                .isCheckBoxSelected(SEARCH);
        assertThat(
                "'Monitoring' setting can not be turned off independently in AML product settings",
                result,
                Matchers.equalTo(false)
        );
    }
}
