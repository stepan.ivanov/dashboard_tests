package com.mati.web.tests.dashboard.identity.actions;

import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class HistoryButtonTest extends IdentityBaseTest {

    @Test(description = "C2692 - Page history button displays history of actions for the verification")
    public void historyButtonTest() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .selectVerificationStatus(VerificationStatus.REJECTED)
                .waitSuccessfulAlertToBeVisibility()
                .closeAlerts()
                .selectVerificationStatus(VerificationStatus.VERIFIED)
                .waitSuccessfulAlertToBeVisibility()
                .closeAlerts()
                .downloadVerificationPDF();
        WaitUtils.sleepSeconds(30);
        verificationObjectHelper.clickOnHistoryPageButton();

        List<String> listHistoryActions = verificationHistoryPageHelper
                .waitPageLoaded()
                .getListHistoryActions();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(listHistoryActions.get(0), "Downloaded the PDF",
                "The latest action 'Downloaded the PDF'");
        softAssert.assertEquals(listHistoryActions.get(1), "Rejected\nVerified",
                "The previous action 'Rejected -> Verified'");
        softAssert.assertAll();
    }
}
