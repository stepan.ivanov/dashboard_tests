package com.mati.web.tests.dashboard.identity.info;

import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class TimestampIdentityTest extends BaseWebTest {

    @BeforeMethod
    public void loginAndCreateVerification() {
        loginHelper.loginMatiSuccessful();
        analyticsHelper.waitForPageLoad();
        rollUpMenuHelper.selectVerificationFlowsMenu();
        verificationFlowsHelper.selectVerificationFlowByName(IConstants.TIMESTAMP_FLOW);
        flowHelper.createVerification(VerificationInfoByCountry.MEXICO, IConstants.frontMexico, IConstants.backMexico);
    }

    @Test(description = "C9275 - Check Certified Timestamp is worked correctly and Unique ID copped")
    public void timestampIdentityTest() throws IOException, UnsupportedFlavorException {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForMexico)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .clickCertifiedTimestampButton()
                .clickOnCopyUniqueIdButton();

        String savedValue = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(verificationObjectHelper.isUniqueIdDisplayed(), true,
                "UniqueID is displayed");
        softAssert.assertEquals(verificationObjectHelper.isCertifiedTimestampDisplayed(), true,
                "CertifiedTimestamp is displayed");
        softAssert.assertEquals(verificationObjectHelper.getUniqueId(), savedValue,
                "Copy uniqueId button works correctly");
        softAssert.assertAll();
    }

    @AfterMethod(alwaysRun = true)
    public void deleteVerification() {
        verificationListHelper
                .navigateToPage()
                .deleteVerificationByName(IConstants.verificationNameForMexico);
    }
}
