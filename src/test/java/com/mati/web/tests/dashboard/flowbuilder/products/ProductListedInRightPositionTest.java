package com.mati.web.tests.dashboard.flowbuilder.products;

import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class ProductListedInRightPositionTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .moveAllProductsToFlowBuilder();
    }

    @DataProvider
    public Object[][] productsAndPosition() {
        return new Object[][] {
                {Products.IP_CHECK, 1},
                {Products.DOCUMENT_VERIFICATION, 2},
                {Products.BIOMETRIC_VERIFICATION, 3},
                {Products.GOVERNMENT_CHECK, 4},
                {Products.ANTI_MONEY_LAUNDERING, 5},
        };
    }

    @Test(description = "C2772, C2768, C2764, C2776 - Product listed in the right position, C2731 - Products are always sorted by order",
            dataProvider = "productsAndPosition")
    public void isProductInRightPositionTest(Products products, int position) {
        assertThat(
                "Product is not in right position in flow builder",
                flowHelper.isProductInRightPosition(products, position),
                Matchers.equalTo(true)
        );
    }
}
