package com.mati.web.tests.dashboard.flowbuilder.products;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.FlowSettings;
import com.mati.enums.verificationflows.settings.IPSettings;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class IPCheckSettingsTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.IP_CHECK);
    }

    @DataProvider
    public Object[] settings() {
        return new Object[]{IPSettings.IP_GEO_RESTRICTION, IPSettings.VPN_RESTRICTION};
    }

    @Test(description = "C2778 - Required settings are available", dataProvider = "settings")
    public void isProductSettingsDisplayedTest(FlowSettings setting) {
        assertThat(
          "Product setting is not available in IP check product settings",
                flowHelper.isProductSettingDisplayed(setting),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2778 - Required settings are available", dataProvider = "settings")
    public void isSettingCheckboxSwitchableTest(IPSettings setting) {
        assertThat(
                "Product setting checkbox is not switchable in IP check product settings",
                flowHelper.isProductSettingCheckBoxSwitchable(setting),
                Matchers.equalTo(true)
        );
    }
}
