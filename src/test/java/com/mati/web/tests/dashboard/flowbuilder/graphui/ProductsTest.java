package com.mati.web.tests.dashboard.flowbuilder.graphui;

import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class ProductsTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        verificationFlowsHelper.navigateToPage().selectVerificationFlowByName(IConstants.AQA_FLOW_NAME);
        flowHelper.deleteProductsFromFlowBuilder();
    }

    @Test(description = "C2730 - Products are aligned vertically")
    public void isProductsAlignedVerticallyTest() {
        assertThat(
                "Products are not aligned vertically",
                flowHelper.isProductsAlignedVertically(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2732 - Product is expandable")
    public void isProductExpandableTest() {
        assertThat(
                "Product is not expandable",
                flowHelper.isProductExpandable(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2734 - Clicking the 'Delete' icon by the product removes it from the Flow Builder Zone and returns it to the Product List")
    public void isProductDeletedFromFlowBuilderTest() {
        flowHelper.moveProductToFlowBuilder(Products.PHONE_CHECK)
                .deleteProductFromFlowBuilder(Products.PHONE_CHECK);
        boolean result = flowHelper.isProductInProductsList(Products.PHONE_CHECK);
        assertThat(
                "Product was delete from flow builder",
                result,
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2733 - Clicking the gear icon by the product opens the 'Product settings' panel")
    public void isProductSettingsPanelOpenedTest() {
        flowHelper.moveProductToFlowBuilder(Products.IP_CHECK);
        assertThat(
                "'Product settings' panel was not open",
                flowHelper.isProductSettingsDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2733 - Clicking the product opens the \"Product settings\" pane")
    public void isProductSettingsPanelClosedTest() {
        flowHelper.moveProductToFlowBuilder(Products.EMAIL_CHECK);
        boolean panelOpen = flowHelper.isProductSettingsDisplayed();
        flowHelper.closeProductSettings();
        boolean panelClosed = !flowHelper.isProductSettingsDisplayed();
        boolean result = panelOpen && panelClosed;

        assertThat(
                "'Product settings' panel was not close",
                result,
                Matchers.equalTo(true)
        );
    }
}
