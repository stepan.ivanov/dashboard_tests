package com.mati.web.tests.dashboard.flowbuilder.graphui;

import com.mati.enums.verificationflows.Products;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class FlowBuilderTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        verificationFlowsHelper.navigateToPage().selectVerificationFlowByName(IConstants.AQA_FLOW_NAME);
        flowHelper.deleteProductsFromFlowBuilder();
    }

    @Test(description = "C2726 - Flow builder zone is scrollable")
    public void isFlowBuilderScrollableTest() {
        assertThat(
                "Flow builder is not scrollable",
                flowHelper.isFlowBuilderScrollable(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2727 - User can drag & drop Product to Flow Builder Zone")
    public void isProductMovedToFlowBuilderZoneTest() {
        flowHelper.moveProductToFlowBuilder(Products.IP_CHECK);
        assertThat(
                "The product is not moved to flow builder zone",
                flowHelper.isProductMovedToFlowBuilder(Products.IP_CHECK),
                Matchers.equalTo(true)
        );
    }

    //TODO BUG - SDK-only product can be moved to flow builder zone when API integration is selected
    @Test(description = "C2728 - User cannot drag & drop “SDK only” Product when API integration is selected")
    public void isSDKOnlyProductCanNotBeMovedToFlowBuilderZoneTest() {
        flowHelper.selectApiIntegration();
        flowHelper.moveProductToFlowBuilder(Products.EMAIL_CHECK);
        assertThat(
                "The 'SDK only' product is moved to flow builder zone",
                flowHelper.isProductMovedToFlowBuilder(Products.EMAIL_CHECK),
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C2729 - User cannot drag anything on Flow Builder area, user can only drag products to the drop zone")
    public void isDraggableOnlyProductsTest() {
        assertThat(
                "User can drag anything to flow builder except products",
                flowHelper.isDraggableOnlyProducts(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2735 - Drag & Drop zone always remains below all the products")
    public void isDropZoneAlwaysRemainsAtTheBottomTest() {
        flowHelper.deleteProductsFromFlowBuilder();
        boolean result = flowHelper.isDropZoneAlwaysRemainsAtTheBottom();
        flowHelper.deleteProductsFromFlowBuilder();
        assertThat(
                "Drag & Drop zone does not remain below all the products",
                result,
                Matchers.equalTo(true)
        );
    }
}