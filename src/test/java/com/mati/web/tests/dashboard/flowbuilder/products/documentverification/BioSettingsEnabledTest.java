package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class BioSettingsEnabledTest extends BaseWebTest {

    @BeforeClass
    public void navigateToFlow() {
        ApiService.createFlow(RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0));
    }

    @BeforeMethod
    public void selectDocumentProduct() {
         flowHelper.clickProductInFlowBuilder(Products.DOCUMENT_VERIFICATION);
    }

    @Test(description = "C2751 - Bio - Facematch can be enabled and is configurable")
    public void isRecommendedThresholdSelectedTest() {
        assertThat(
                "Bio setting is not enabled by default and recommended threshold setting is not selected",
                flowHelper.isSettingsRadioButtonSelected(DocumentationSettings.RECOMMENDED_THRESHOLD),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2751 - Bio - Facematch can be enabled and is configurable")
    public void isCustomLimitSelectiveTest() {
        assertThat(
                "Custom limit setting can not be selected",
                flowHelper.isProductSettingSelective(DocumentationSettings.CUSTOM_LIMIT),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2752 - Bio - Proof of ownership doesn't work with Voice Liveness enabled")
    public void isWarningMessageDisplayed() {
        flowHelper.selectSettingCheckbox(DocumentationSettings.PROOF_OF_OWNERSHIP, true)
                .clickProductInFlowBuilder(Products.BIOMETRIC_VERIFICATION);
        assertThat(
                "Proof of ownership and Voice liveness can be enabled simultaneously",
                flowHelper.isWarningWithContentDisplayed("Proof of ownership and Voice liveness cannot be enabled simultaneously."),
                Matchers.equalTo(true)
        );
    }
}
