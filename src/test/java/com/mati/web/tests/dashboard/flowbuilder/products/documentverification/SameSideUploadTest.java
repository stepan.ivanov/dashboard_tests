package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class SameSideUploadTest extends BaseWebTest {

    @BeforeClass
    public void login() {
        loginHelper.loginMatiSuccessful();
    }

    @BeforeMethod
    public void navigateToFlow() {
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .addDocumentToDVSettings("National ID", true);
    }

    @DataProvider
    public Object[] fileName() {
        return new Object[] {
                IConstants.frontMexico, IConstants.backMexico
        };
    }

    @Test(description = "C2748 - Same side upload fails")
    public void sameSideDocumentWithFrontPhotoIsRejectedTest() {
        flowHelper.selectSettingCheckbox(DocumentationSettings.SAME_SIDE_UPLOAD_VALIDATION, true)
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .clickStartVerificationButton()
                .selectCountry(VerificationInfoByCountry.MEXICO)
                .putFrontPhoto(IConstants.frontMexico)
                .putBackPhoto(IConstants.frontMexico);
        assertThat(
                "Same side document was enabled for verification with selected 'Same Side' validation setting",
                flowHelper.isErrorVerificationPopupAppeared(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2748 - Same side upload fails")
    public void sameSideDocumentWithBackPhotoIsRejectedTest() {
        flowHelper.selectSettingCheckbox(DocumentationSettings.SAME_SIDE_UPLOAD_VALIDATION, true)
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .clickStartVerificationButton()
                .selectCountry(VerificationInfoByCountry.MEXICO)
                .putFrontPhoto(IConstants.backMexico);
        assertThat(
                "Same side document was enabled for verification with selected 'Same Side' validation setting",
                flowHelper.isErrorVerificationPopupAppeared(),
                Matchers.equalTo(true)
        );
    }

    // TODO BUG - user can not upload back photo, when he puts backPhoto to front input then he see error popup with "No face was found on your document"
    @Test(description = "C2748 - Same side upload fails", dataProvider = "fileName")
    public void sameSideDocumentIsAcceptedTest(String fileName) {
        flowHelper.selectSettingCheckbox(DocumentationSettings.SAME_SIDE_UPLOAD_VALIDATION, false)
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .createVerification(VerificationInfoByCountry.MEXICO, fileName, fileName);
        verificationListHelper.navigateToPage();
        assertThat(
                "Same side document was enabled for verification without selected 'Same Side' validation setting",
                verificationListHelper.isVerificationExist(VerificationInfoByCountry.MEXICO.getPersonName()),
                Matchers.equalTo(true)
        );
    }
}
