package com.mati.web.tests.dashboard.collaborators;

import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.sql.Timestamp;
import java.time.Instant;

public class VerificationHistoryAreaOpensQuicklyTest extends BaseWebTest {

    @Test(description = "C2706 - Verification history area opens quickly, displays required data")
    public void isVerificationHistoryAreaOpenQuicklyTest() {
        loginHelper.loginMatiAsAgent();
        verificationListHelper.navigateToPage();

        Timestamp startTime = Timestamp.from(Instant.now());
        verificationListHelper
                .selectFirstVerification()
                .waitForPageLoad();
        double timeForPageLoading = (Timestamp.from(Instant.now()).getTime() - startTime.getTime()) / 1000.0;
        boolean isLoadingFinishedInTime = timeForPageLoading < 2.0;

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(isLoadingFinishedInTime, true,
                "Page loading less or equal to 2 seconds. Actual: " + timeForPageLoading);
        softAssert.assertEquals(verificationObjectHelper.isBackToVerificationListButtonDisplayed(), true,
                "Back to verification list button is displayed");
        softAssert.assertEquals(verificationObjectHelper.isVerificationStatusButtonDisplayed(), true,
                "Verification Status button is displayed");
        softAssert.assertEquals(verificationObjectHelper.isDownloadPdfButtonDisplayed(), true,
                "Download PDF button is displayed");
        softAssert.assertEquals(verificationObjectHelper.isVerificationDataButtonDisplayed(), true,
                "Verification Data button is displayed");
        softAssert.assertEquals(verificationObjectHelper.isPageHistoryButtonDisplayed(), true,
                "Page History button is displayed");
        softAssert.assertEquals(verificationObjectHelper.isVerificationDateDisplayed(), true,
                "Verification date is displayed");
        softAssert.assertEquals(verificationObjectHelper.isVerificationNumberDisplayed(), true,
                "Verification Number is displayed");
        softAssert.assertEquals(verificationObjectHelper.isVerificationFlowDisplayed(), true,
                "Verification Flow is displayed");
        softAssert.assertEquals(verificationObjectHelper.isVerificationSourceDisplayed(), true,
                "Verification Source is displayed");
        softAssert.assertAll();
    }
}
