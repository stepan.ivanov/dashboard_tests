package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class VerificationWizardTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerificationFlow() {
        ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomStringWithPrefix());
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(TokenContainer.getInstance().getFlowId().get().get(0))
                .clickProductInFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .selectSettingCheckbox(DocumentationSettings.RESTRICT_IMAGE_UPLOAD_FROM_GALLERY, true)
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved()
                .clickVerifyMeButton()
                .switchToVerificationWizard()
                .waitFlowWizardToBeLoaded();
    }

    @Test(description = " C2745 - Document upload can be restricted to gallery only")
    public void isVerificationWizardDisplayedTest() {
        assertThat(
                "'Verification wizard' was not opened",
                flowHelper.isVerificationWizardDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = " C2745 - Document upload can be restricted to gallery only")
    public void verificationWizardHasCorrectContentTest() {
        assertThat(
                "'Verification wizard' does not have correct content",
                flowHelper.getVerificationWizardTitle(),
                Matchers.equalTo("Please switch to your smartphone")
        );
    }
}
