package com.mati.web.tests.dashboard.settings;

import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SettingsTest extends BaseWebTest {

    @BeforeClass
    public void logIn() {
        loginHelper.loginMatiSuccessful();
    }

    @BeforeMethod
    public void navigateToPage() {
        settingsPageHelper.navigateToPage();
    }

    @Test(description = "C2649 - 'Profile' button is displayed by each agent")
    public void isAllPersonHasProfileButton() {
        assertThat("All person has Profile button",
                settingsPageHelper.isAllPersonHasProfileButton(),
                equalTo(true));
    }

    @Test(description = "C2650 - Agent profile displays required fields")
    public void isRequiredFieldsDisplayedTest() {
        settingsPageHelper.openRandomProfile();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(agentHistoryPageHelper.isNameDisplayed(), true, "Name is displayed");
        softAssert.assertEquals(agentHistoryPageHelper.isEmailDisplayed(), true, "Email is displayed");
        softAssert.assertEquals(agentHistoryPageHelper.isRoleDisplayed(), true, "Role is displayed");
        softAssert.assertEquals(agentHistoryPageHelper.isDateOfRegistration(), true, "Date of Registration is displayed");
        softAssert.assertAll();
    }
}
