package com.mati.web.tests.dashboard.verificationlist;

import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DownloadCsvSpinnerTest extends BaseWebTest {

    @Test(description = "C3177 - Clicking Download in CSV fires a download spinner")
    public void downloadCsvSpinnerTest() {
        loginHelper.loginMatiSuccessful();
        verificationListHelper
                .navigateToPage()
                .downloadVerificationCSV();

        assertThat("Download CSV spinner is displayed",
                verificationListHelper.isCsvLoadingSpinnerDisplayed(), equalTo(true));
    }
}
