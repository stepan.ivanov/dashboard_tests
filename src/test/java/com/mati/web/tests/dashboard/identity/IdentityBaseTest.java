package com.mati.web.tests.dashboard.identity;

import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class IdentityBaseTest extends BaseWebTest {

    @BeforeMethod
    public void loginAndCreateVerification() {
        loginHelper.loginMatiSuccessful();
        analyticsHelper.waitForPageLoad();
        rollUpMenuHelper.selectVerificationFlowsMenu();
        verificationFlowsHelper.selectVerificationFlowByName(IConstants.AQA_FLOW_NAME);
        flowHelper.createVerification(VerificationInfoByCountry.CANADA, IConstants.frontCanada, IConstants.backCanada);
    }

    @AfterMethod(alwaysRun = true)
    public void deleteVerification() {
        verificationListHelper
                .navigateToPage()
                .deleteVerificationByName(IConstants.verificationNameForCanada);
    }
}
