package com.mati.web.tests.dashboard.identity.info;

import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class DeviceCheckTest extends IdentityBaseTest {

    @Test(description = "C9233 - Device Check is displayed (via UI)")
    public void isDeviceCheckIsDisplayed() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                //Workaround for switching to new view
                .switchToNewView()
        //
                .clickDeviceCheckButton();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(verificationObjectHelper.isDeviceTypeDisplayed(), true,
                "DeviceType is displayed");
        softAssert.assertEquals(verificationObjectHelper.isOsDisplayed(), true,
                "OS is displayed");
        softAssert.assertEquals(verificationObjectHelper.isBrowserDisplayed(), true,
                "Browser is displayed");
        softAssert.assertAll();
    }
}
