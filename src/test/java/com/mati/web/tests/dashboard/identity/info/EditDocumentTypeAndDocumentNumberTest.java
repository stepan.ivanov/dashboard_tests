package com.mati.web.tests.dashboard.identity.info;

import com.mati.web.IConstants;
import com.mati.web.tests.dashboard.identity.IdentityBaseTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.Arrays;

public class EditDocumentTypeAndDocumentNumberTest extends IdentityBaseTest {

    private final String documentType = "Type 111";
    private final String documentNumber = "Doc 111";

    @Test(description = "C8431 - The fields 'Document number' and 'Document type' could be edited")
    public void editDocumentTypeAndDocumentNumberTest() {
        verificationListHelper
                .navigateToPage()
                .waitForPageLoad()
                .selectVerificationByName(IConstants.verificationNameForCanada)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .scrollToEditDataButton()
                .clickEditDataButton()
                .fillInputDocumentNumber(documentNumber)
                .fillDocumentType(documentType)
                .saveEdits()
                .waitSuccessfulAlertToBeVisibility();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(verificationObjectHelper.getDocumentType(), documentType,
                String.format("Document type should be %s", documentType));
        softAssert.assertEquals(verificationObjectHelper.getDocumentNumber(), documentNumber,
                String.format("Document number should be %s", documentNumber));

        softAssert.assertAll();
    }
}
