package com.mati.web.tests.dashboard.verificationlist;

import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class ReviewButtonInDashboardTest extends BaseWebTest {

    @BeforeClass
    public void logInAndGoToAnalyticsPage() {
        loginHelper.loginMatiSuccessful();
        rollUpMenuHelper.selectVerificationListMenu();
    }

    @Test(description = "C255 - Review button is available in Dashboard")
    public void isReviewModeButtonEnabledOnVerificationListPageTest() {
        assertThat(
                "'Review mode' button is disabled on 'Verification List' page",
                verificationListHelper.isReviewModeButtonEnabled(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "check review mode functionality - expand into individual test cases")
    public void isReviewModeButtonBlackOnVerificationListPageTest() {
        assertThat(
                "'Review mode' button is not black on 'Verification List' page",
                verificationListHelper.isReviewModeButtonBlack(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "check review mode functionality - expand into individual test cases")
    public void isReviewModePageOpenedFromVerificationListPageTest() {
        verificationListHelper.clickReviewModeButton();
        assertThat(
                "'Review mode' page is not opened from 'Verification List' page",
                reviewModeHelper.isReviewModePage(),
                Matchers.equalTo(true));
    }

}
