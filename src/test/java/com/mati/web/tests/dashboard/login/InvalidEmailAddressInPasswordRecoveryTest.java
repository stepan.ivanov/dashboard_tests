package com.mati.web.tests.dashboard.login;

import com.mati.enums.Errors;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class InvalidEmailAddressInPasswordRecoveryTest extends BaseWebTest {

    @BeforeClass
    public void clickForgotPasswordLinkAndFillInvalidEmailAddress() {
        loginHelper.navigateToPage()
                .clickForgotPasswordLink()
                .fillEmailField(RandomUtils.randomString())
                .clickSendResetEmailButton();
    }

    @Test(description = "C2636 - Invalid email address in password recovery")
    public void isInvalidEmailAddressErrorMessageDisplayedTest() {
        assertThat(
                "Error message is not displayed due to invalid password recovery email",
                recoveryPasswordHelper.isInvalidEmailAddressErrorMessageDisplayed(),
                Matchers.equalTo(true));
    }

    @Test(description = "C2636 - Invalid email address in password recovery")
    public void isInvalidEmailAddressErrorMessageCorrectTest() {
        assertThat(
                "Error message is not correct due to invalid password recovery email",
                recoveryPasswordHelper.getInvalidEmailAddressErrorMessage(),
                Matchers.equalTo(Errors.INVALID_EMAIL_ADDRESS.getMessage()));
    }
}
