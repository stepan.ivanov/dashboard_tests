package com.mati.web.tests.dashboard.flowbuilder.products.documentverification;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class BlackWhiteDocumentTest extends BaseWebTest {

    @BeforeClass
    public void login() {
        loginHelper.loginMatiSuccessful();
    }

    @BeforeMethod
    public void navigateToFlow() {
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .addDocumentToDVSettings("National ID", true);
    }

    @Test(description = "C2747 - Black & white documents can be accepted or rejected")
    public void bwDocumentIsRejectedTest() {
        flowHelper.selectSettingCheckbox(DocumentationSettings.BW_VALIDATION, true)
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .clickStartVerificationButton()
                .selectCountry(VerificationInfoByCountry.MEXICO)
                .putFrontPhoto(IConstants.frontMexicoBW);
        assertThat(
                "Black and white document was enabled for verification with selected BW validation setting",
                flowHelper.isErrorVerificationPopupAppeared(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2747 - Black & white documents can be accepted or rejected")
    public void bwDocumentIsAcceptedTest() {
        flowHelper.selectSettingCheckbox(DocumentationSettings.BW_VALIDATION, false)
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .createVerification(VerificationInfoByCountry.MEXICO, IConstants.frontMexicoBW, IConstants.backMexicoBW);
        verificationListHelper.navigateToPage();
        assertThat(
                "Black and white document was not enabled for verification without selected BW validation setting",
                verificationListHelper.isVerificationExist(VerificationInfoByCountry.MEXICO.getPersonName()),
                Matchers.equalTo(true)
        );
    }
}
