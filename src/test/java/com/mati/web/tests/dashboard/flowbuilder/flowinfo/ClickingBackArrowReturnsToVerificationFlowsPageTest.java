package com.mati.web.tests.dashboard.flowbuilder.flowinfo;

import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class ClickingBackArrowReturnsToVerificationFlowsPageTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        rollUpMenuHelper.selectVerificationFlowsMenu()
                .selectVerificationFlowByName(IConstants.AQA_FLOW_NAME);
    }

    @Test(description = "C2724 - Flow Info - Clicking the “Back” arrow icon returns to the Flow List page")
    public void isBackArrowReturnsToVerificationFlowsTest() {
        flowHelper.clickBackArrowButton();
        assertThat(
                "Back arrow button does not return to Verification flow page",
                verificationFlowsHelper.isVerificationFlowsPage(),
                Matchers.equalTo(true)
        );
    }
}
