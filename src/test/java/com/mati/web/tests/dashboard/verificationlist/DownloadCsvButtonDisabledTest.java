package com.mati.web.tests.dashboard.verificationlist;

import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DownloadCsvButtonDisabledTest extends BaseWebTest {

    @Test(description = "C15679 - Download in CSV button is disabled while another download is in progress")
    public void downloadCsvButtonDisabledTest() {
        loginHelper.loginMatiSuccessful();
        verificationListHelper
                .navigateToPage()
                .downloadVerificationCSV();

        assertThat("Button is disabled during loading",
                verificationListHelper.isCsvLoadingSpinnerDisplayed(), equalTo(!verificationListHelper.isDownloadCsvButtonEnabled()));

        verificationListHelper.waitUntilCsvLoadingSpinnerDisappear();

        assertThat("Button is enable",
                !verificationListHelper.isCsvLoadingSpinnerDisplayed(), equalTo(verificationListHelper.isDownloadCsvButtonEnabled()));
    }
}
