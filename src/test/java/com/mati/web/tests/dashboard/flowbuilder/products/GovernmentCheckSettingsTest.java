package com.mati.web.tests.dashboard.flowbuilder.products;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.settings.FlowSettings;
import com.mati.enums.verificationflows.settings.GovernmentCheckSettings;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class GovernmentCheckSettingsTest extends BaseWebTest {

    @BeforeClass
    public void loginAndGoToVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.GOVERNMENT_CHECK)
                .confirmAddingProductToFlowBuilder()
                .clickProductInFlowBuilder(Products.GOVERNMENT_CHECK);
    }

    @DataProvider
    public Object[] settings() {
        return new Object[]{GovernmentCheckSettings.DATABASES_REQUEST_TIMEOUT, GovernmentCheckSettings.COUNTIES_AND_CHECKS};
    }

    @Test(description = "C2766 - Required settings are available", dataProvider = "settings")
    public void isProductSettingAvailableTest(FlowSettings setting) {
        assertThat(
                "Product setting is not available in Gov check product settings",
                flowHelper.isProductSettingDisplayed(setting),
                Matchers.equalTo(true)
        );
    }

    @DataProvider
    public Object[] unit() {
        return new Object[]{"hours", "minutes"};
    }

    @Test(description = "C2766 - Required settings are available", dataProvider = "unit")
    public void isHoursInputDisplayedTest(String unit) {
        assertThat(
                "Input for hours to request DB is not displayed",
                flowHelper.isDBRequestInputDisplayed(unit),
                Matchers.equalTo(true)
        );
    }

    @DataProvider
    public Object[][] countryChecks() {
        return new Object[][]{
                {
                        "Argentina", List.of("RENAPER", "DNI Card Validity Check")
                },
                {
                        "Bolivia", List.of("OEP")
                },
                {
                        "Brazil", List.of("Receita Federal", "Government facematch")
                },
                {
                        "Chile", List.of("Registro Civil")
                },
                {
                        "Colombia", List.of("NIT", "Colombian Contraloria", "Registraduría", "National Police", "General Procuraduria")
                },
                {
                        "Costa Rica", List.of("ATV", "Costa Rican TSE", "Social Security")
                },
                {
                        "Dominican Republic", List.of("JCE")
                },
                {
                        "Ecuador", List.of("Registro Civil")
                },
                {
                        "Ghana", List.of("Ghanaian GRA")
                },
                {
                        "Guatemala", List.of("TSE")
                },
                {
                        "Honduras", List.of("RNP")
                },
                {
                        "Mexico", List.of("CURP", "INE", "PEP", "RFC")
                },
                {
                        "Paraguay", List.of("RCP")
                },
                {
                        "Peru", List.of("RENIEC")
                },
                {
                        "Salvador", List.of("TSE")
                },
                {
                        "Panama", List.of("Tribunal")
                },
                {
                        "Venezuela", List.of("CNE", "SENIAT")
                }
        };
    }

    @Test(description = "C2766 - Required settings are available", dataProvider = "countryChecks")
    public void isCountriesChecksDisplayedTest(String country, List checks) {
        assertThat(
                "Government checks are not displayed",
                flowHelper.isGovernmentChecksDisplayed(country, checks),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C2766 - Required settings are available", dataProvider = "countryChecks")
    public void isCountriesChecksSwitchableTest(String country, List checks) {
        assertThat(
                "Government checkboxes are not switchable",
                flowHelper.isGovernmentCheckboxSwitchable(country, checks),
                Matchers.equalTo(true)
        );
    }
}
