package com.mati.web.tests.sanitychecks.verificationcheck;

import com.mati.enums.verificationflows.Products;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CheckSavedTest extends BaseWebTest {

    private String flowName;

    @BeforeClass
    public void loginToMati() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(description = "C9468, C11948 - 'IP check' is saved" +
            "C9520, C11951 - Premium AML watchlists is saved",
            dataProvider = "data-provider")
    public void isCheckSaved(Products product) {
        flowName = RandomUtils.randomString();
        rollUpMenuHelper
                .selectVerificationFlowsMenu()
                .createFlow(flowName);
        flowHelper.moveProductToFlowBuilder(product)
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved();

        assertThat("Check saved in flow",
                flowHelper.isProductMovedToFlowBuilder(product), equalTo(true));
    }

    @AfterMethod
    public void deleteFlow() {
        verificationFlowsHelper
                .navigateToPage()
                .removeFlowByName(flowName);
    }

    @DataProvider(name = "data-provider")
    public Object[][] provideData() {
        return new Object[][]{
                {Products.IP_CHECK},
                {Products.ANTI_MONEY_LAUNDERING}
        };
    }
}
