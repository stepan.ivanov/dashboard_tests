package com.mati.web.tests.sanitychecks.verificationlist;

import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;

public class DownloadVerificationsTest extends BaseWebTest {

    @BeforeClass
    public void openVerification() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(description = "C99 - Download CSV")
    public void isCSVFileDownloadedTest() {
        rollUpMenuHelper.selectVerificationListMenu();
        verificationListHelper.downloadVerificationCSV();
        WaitUtils.sleepSeconds(15);
        boolean result =
                Arrays.asList(Objects.requireNonNull(new File(IConstants.currentFolder).list()))
                        .contains(IConstants.verificationCSVFileName);
        assertThat("Verification was not download in CSV file", result, Matchers.equalTo(true));
    }

    @Test(description = "C91 - Download verification in PDF file")
    public void isPDFFileDownloadedTest() {
        verificationListHelper
                .navigateToPage()
                .selectVerificationByStatus(VerificationStatus.VERIFIED);
        String verificationNumber = verificationObjectHelper.getVerificationNumber();
        verificationObjectHelper.downloadVerificationPDF();
        boolean result =
                Arrays.stream(Objects.requireNonNull(new File(IConstants.currentFolder).list()))
                        .anyMatch(
                                s ->
                                        s.equals(
                                                String.format(
                                                        IConstants.verificationPDFFileNameFormat,
                                                        verificationNumber)));
        assertThat("Verification was not download in PDF file", result, Matchers.equalTo(true));
    }
}
