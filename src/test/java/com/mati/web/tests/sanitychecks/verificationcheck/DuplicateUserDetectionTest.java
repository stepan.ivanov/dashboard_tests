package com.mati.web.tests.sanitychecks.verificationcheck;

import com.mati.enums.verificationflows.settings.DocumentationSettings;
import com.mati.enums.verificationflows.Products;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DuplicateUserDetectionTest extends BaseWebTest {

    private final String flowName = RandomUtils.randomString();

    @Test(description = "C9469, C11949 - Duplicate user detection is checked")
    public void isDuplicateUserDetectionChecked() {
        loginHelper.loginMatiSuccessful();
        rollUpMenuHelper
                .selectVerificationFlowsMenu()
                .createFlow(flowName);
        flowHelper
                .clickProductInFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .clickProductSettingSelective(DocumentationSettings.DUPLICATE_USER_DETECTION)
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved();

        assertThat("Duplicate user is selected",
                flowHelper.isCheckBoxSelected(DocumentationSettings.DUPLICATE_USER_DETECTION), equalTo(true));
    }

    @AfterMethod
    public void deleteFlow() {
        verificationFlowsHelper
                .navigateToPage()
                .removeFlowByName(flowName);
    }
}
