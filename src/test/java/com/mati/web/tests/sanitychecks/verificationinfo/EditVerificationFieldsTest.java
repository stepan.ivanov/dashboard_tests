package com.mati.web.tests.sanitychecks.verificationinfo;

import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class EditVerificationFieldsTest extends BaseWebTest {

    @BeforeClass
    public void logInAndCreateVerification() {
        loginHelper.loginMatiSuccessful();
        verificationListHelper.navigateToPage()
                .selectVerificationByName(VerificationInfoByCountry.MEXICO.getPersonName())
                .waitForPageLoad()
                .scrollToEditDataButton();
    }

    @Test(description = "C90 - Edit verification fields")
    public void isVerificationDataChangedTest() {
        List actual = verificationObjectHelper.getDataOfVerificationFields();
        verificationObjectHelper.clickEditDataButton()
                .fillVerificationFields()
                .clickSaveAndSendWebhookButton();
        List expected = verificationObjectHelper.getDataOfVerificationFields();
        assertThat("Verification date were not change", actual, Matchers.not(expected));
    }
}
