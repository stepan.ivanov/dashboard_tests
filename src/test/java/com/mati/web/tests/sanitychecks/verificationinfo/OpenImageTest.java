package com.mati.web.tests.sanitychecks.verificationinfo;

import com.codeborne.selenide.WebDriverRunner;
import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class OpenImageTest extends BaseWebTest {

    private final VerificationInfoByCountry argentinaVerification = VerificationInfoByCountry.ARGENTINA;
    private String flowId;

    @BeforeClass
    public void createVerification() {
        loginHelper.loginMatiSuccessful();
        flowId = ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomString())
                .getBody()
                .as(FlowModel.class)
                .getId();
        flowHelper
                .navigateToFlow(flowId)
                .deleteProductFromFlowBuilder(Products.BIOMETRIC_VERIFICATION)
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved()
                .createVerification(argentinaVerification);
    }

    @Test(description = "C14112, C14111 - Open images in a new tab")
    public void openImageTest() {
        verificationListHelper
                .navigateToPage()
                .selectVerificationByName(argentinaVerification.getPersonName())
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .clickDocumentVerificationButton()
                .clickOnFrontDocumentImage()
                .openZoomedImageInNewTab();

        assertThat("Image in new tab is displayed",
                verificationObjectHelper.isImageInNewTabDisplayed(), equalTo(true));

        WebDriverRunner.getWebDriver().navigate().back();

        verificationObjectHelper
                .clickOnBackDocumentImage()
                .openZoomedImageInNewTab();

        assertThat("Image in new tab is displayed",
                verificationObjectHelper.isImageInNewTabDisplayed(), equalTo(true));
    }

    @AfterMethod
    public void deleteFlow() {
        ApiService.deleteFlows(TokenContainer.getInstance().getMerchant(), flowId);
        verificationListHelper
                .navigateToPage()
                .deleteVerificationByName(argentinaVerification.getPersonName());
    }
}
