package com.mati.web.tests.sanitychecks.verificationlist;

import com.mati.enums.Period;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.*;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class VerificationListFilterTest extends BaseWebTest {

    @BeforeClass
    public void loginAndSelectVerificationList() {
        loginHelper.loginMatiSuccessful();
        verificationListHelper.navigateToPage();
    }

    @BeforeMethod
    public void openAndClearFilter() {
        verificationListHelper.openFilter().clearFilter();
    }

    @Test(description = "C98 - Check the filters")
    public void checkFilterByVerificationStatusTest() {
        VerificationStatus status = VerificationStatus.valueOf(
                RandomUtils.getRandomElementFromList(verificationListHelper.getVerificationStatuses()).toUpperCase());
        verificationListHelper.scrollToStatus(status)
                .selectFilterByStatus(status)
                .clickApplyFilterButton();
        assertThat(
                "Filter does not work correct by status",
                verificationListHelper.isVerificationListHaveCorrectStatus(status),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C98 - Check the filters")
    public void checkFilterByFlowTest() {
        List<String> verificationFlowList = verificationListHelper.getVerificationFlows();
        String flowName = RandomUtils.getRandomElementFromList(verificationFlowList);
        verificationListHelper.scrollToFlow(flowName)
                .selectFilterByFlow(flowName)
                .clickApplyFilterButton();
        assertThat(
                "Filter does not work correct by flow",
                verificationListHelper.isVerificationListHaveCorrectFlow(flowName),
                Matchers.equalTo(true)
        );
    }

    @DataProvider
    public Object[] period() {
        return Period.values();
    }

    @Test(description = "C98 - Check the filters", dataProvider = "period")
    public void checkFilterByDate(Period period) {
        List<LocalDate> dates = verificationListHelper.selectFilterByPeriod(period)
                .getFilterDates();
        verificationListHelper.clickApplyFilterButton();
        assertThat(
                "Filter does not work correct by periods",
                verificationListHelper.isVerificationListHaveCorrectPeriod(dates),
                Matchers.equalTo(true)
        );
    }

}
