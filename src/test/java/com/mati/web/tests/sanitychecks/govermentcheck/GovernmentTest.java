package com.mati.web.tests.sanitychecks.govermentcheck;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationchecks.GovernmentChecks;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.GovernmentCheckSettings;
import com.mati.model.requests.Body;
import com.mati.utils.JsonUtils;
import com.mati.utils.PostBinUtils;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class GovernmentTest extends BaseWebTest {

    private List<String> flows = new ArrayList<>();
    private List<VerificationInfoByCountry> verifications = new ArrayList<>();

    // TODO: need Mock for Document Verification step

    @BeforeClass
    public void loginToMati() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(dataProvider = "data-provider",
            description = "C9522, C9670 - e2e. Argentina - Full UI Flow - Check Webhook Steps" +
//                    "C9523 - e2e. Bolivia - Full UI Flow - Check Webhook Steps" +
//                    "C9524 - e2e. Brazil - Full UI Flow - Check Webhook Steps" +
                    "C9525, C9673 - e2e. Chile - Full UI Flow - Check Webhook Steps" +
//                    "C9527 - e2e. Costa Rica - Full UI Flow - Check Webhook Steps" +
                    "C9528, C9676 - e2e. Dominican Republic - Full UI Flow - Check Webhook Steps" +
//                    "C9529 - e2e. Ecuador - Full UI Flow - Check Webhook Steps" +
//                    "C9530 - e2e. Guatemala - Full UI Flow - Check Webhook Steps" +
//                    "C9531 - e2e. Honduras - Full UI Flow - Check Webhook Steps" +
                    "C9532, C9680 - e2e. Mexico - Full UI Flow - Check Webhook Steps" +
                    "C9534, C9682 - e2e. Peru - Full UI Flow - Check Webhook Steps")
//                    "C9535 - e2e. Salvador - Full UI Flow - Check Webhook Steps" +
//                    "C9536 - e2e. Panama - Full UI Flow - Check Webhook Steps")
    public void webHookTest(VerificationInfoByCountry verificationInfoByCountry, GovernmentCheckSettings governmentCheckSettings,
                            List<GovernmentChecks> governmentChecks, List<String> expectedValidationString) {
        String flowId = ApiService.createFlow(TokenContainer.getInstance().getMerchant(),
                String.format("%s_%s", verificationInfoByCountry.getName(), RandomUtils.randomString()))
                .getBody()
                .as(FlowModel.class)
                .getId();
        flows.add(flowId);
        String binId = PostBinUtils.createBin();
        verifications.add(verificationInfoByCountry);
        flowHelper
                .navigateToFlow(flowId)
                .deleteProductFromFlowBuilder(Products.BIOMETRIC_VERIFICATION)
                .moveProductToFlowBuilder(Products.GOVERNMENT_CHECK);

        SoftAssert flowChecker = new SoftAssert();
        flowChecker.assertEquals(flowHelper.isProductMovedToFlowBuilder(Products.GOVERNMENT_CHECK), true,
                "Government check moved to Flow builder");
        flowChecker.assertEquals(flowHelper.isProductSettingOpened(Products.GOVERNMENT_CHECK), true,
                "Government Check settings is opened");
        flowChecker.assertEquals(flowHelper.isCheckBoxesSetDefault(GovernmentCheckSettings.COUNTIES_AND_CHECKS), true,
                "All check boxes for countries set to default");
        flowChecker.assertAll();

        flowHelper.selectSettingCheckboxes(governmentCheckSettings, true)
                .closeProductSettings()
                .clickWebHookButton()
                .fillSecretInput(PostBinUtils.SECRET_KEY)
                .fillUrlInput(PostBinUtils.POST_BIN_URL + binId)
                .clickButtonSave()
                .waitUntilChangesWillBeSaved()
                .close();

        flowHelper
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved()
                .createVerification(verificationInfoByCountry);

        verificationListHelper
                .navigateToPage()
                .selectVerificationByName(verificationInfoByCountry.getPersonName())
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .clickDocumentVerificationButton()
                .waitUntilVerificationFinished();

        expectedValidationString.forEach(validation -> {
            Body expectedValidation = JsonUtils.readValue(validation, Body.class);
            String idValidation = ((HashMap<String, String>) expectedValidation.getAdditionalProperties().get("step")).get("id");
            Body actualRequest = PostBinUtils.getRequestsByValidationId(binId, idValidation);
            SoftAssert validationAssert = new SoftAssert();
            validationAssert.assertEquals(actualRequest.getEventName(), expectedValidation.getEventName(),
                    "Event name is: " + expectedValidation.getEventName());
            validationAssert.assertEquals(actualRequest.getAdditionalProperties(), expectedValidation.getAdditionalProperties(),
                    "Response body is the same");
            validationAssert.assertAll();
        });

        verificationObjectHelper
                .clickGovernmentCheckButton();

        governmentChecks.forEach(check ->
                assertThat(String.format("%s is success", check.name()),
                        verificationObjectHelper.isVerificationCheckSuccess(check), equalTo(true)));
    }

    @AfterClass(alwaysRun = true)
    public void deleteFlow() {
        flows.forEach(flowId -> ApiService.deleteFlows(TokenContainer.getInstance().getMerchant(), flowId));
        verifications.forEach(verificationInfoByCountry -> verificationListHelper
                .navigateToPage()
                .deleteVerificationByName(verificationInfoByCountry.getPersonName()));
    }

    @DataProvider(name = "data-provider")
    public Object[][] provideData() {
        return new Object[][]{
                {
                        VerificationInfoByCountry.ARGENTINA, GovernmentCheckSettings.ARGENTINA,
                        Arrays.asList(GovernmentChecks.ARGENTINA_DNI_DATABASE_MATCH, GovernmentChecks.ARGENTINA_DNI_VALIDATION),
                        Arrays.asList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a385f1fb7ec001b8991aa\", \"step\": { \"status\": 200, \"id\": \"argentinian-dni-validation\", \"error\": null, \"data\": { \"documentNumber\": \"26332842\", \"emissionDate\": \"2014-01-10\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a2cd84ddedd001bdfc33d\", \"timestamp\": \"2021-08-28T13:26:03.134Z\" }",
                                "{\"resource\": \"https://api.getmati.com/v2/verifications/612a429eddbdad001b42c48d\", \"step\": { \"status\": 200, \"id\": \"argentinian-renaper-validation\", \"error\": null, \"data\": { \"dateOfBirth\": \"1978-05-05\", \"fullName\": \"GUILLERON GASTON ANDRES\", \"dniNumber\": \"26332842\", \"cuit\": \"20263328427\", \"phoneNumbers\": [ \"1138915908\", \"343154000000\", \"3434064086\" ], \"deceased\": false }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a2cd84ddedd001bdfc33d\", \"timestamp\": \"2021-08-28T14:05:53.269Z\"}")
                },
//                {
//                        VerificationInfoByCountry.BOLIVIA, GovernmentCheckSettings.BOLIVIA,
//                        Collections.singletonList(GovernmentChecks.BOLIVIAN_OEP_VALIDATION),
//                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a47a8ddbdad001b42cf94\", \"step\": { \"status\": 200, \"id\": \"bolivian-oep-validation\", \"error\": null, \"data\": { \"documentNumber\": \"9122385\", \"fullName\": \"BRAYAN LUIS QUISPE CORINE\", \"country\": \"BOLIVIA\", \"city\": \"Murillo, Nuestra Señora de La Paz\", \"state\": \"La Paz\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a47a5f0de27001b2266ab\", \"timestamp\": \"2021-08-28T14:29:07.050Z\" }")
//                },
//                {
//                        VerificationInfoByCountry.BRAZIL, GovernmentCheckSettings.BRAZIL,
//                        Collections.singletonList(GovernmentChecks.BRAZIL_CPF_VALIDATION),
//                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a4b54bd1040001babface\", \"step\": { \"status\": 200, \"id\": \"brazilian-cpf-validation\", \"error\": null, \"data\": { \"documentType\": \"national-id\", \"cpfNumber\": \"025.554.570-38\", \"dateOfBirth\": \"1992-12-30\", \"gender\": \"M\", \"nationality\": \"Brazilian\", \"taxStatus\": \"regular\", \"fullName\": \"SAMUEL STORTI RAENCKE \", \"documentNumber\": \"1090080928\", \"expirationDate\": null }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a4b51f0de27001b2266b0\", \"timestamp\": \"2021-08-28T14:44:03.621Z\" }")
//                },
                {
                        VerificationInfoByCountry.MEXICO, GovernmentCheckSettings.MEXICO,
                        Arrays.asList(GovernmentChecks.MEXICO_PEP_VALIDATION, GovernmentChecks.MEXICO_CURP_VALIDATION, GovernmentChecks.MEXICO_RFC_VALIDATION),
                        Arrays.asList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a6091c48ec3001b547767\", \"step\": { \"status\": 200, \"id\": \"mexican-rfc-validation\", \"error\": null, \"data\": { \"curp\": \"BAME921103HCHLRN00\", \"registeredTaxPayer\": true }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a608ff0de27001b2266bc\", \"timestamp\": \"2021-08-28T16:13:50.586Z\" }",
                                "{ \"resource\": \"https://api.getmati.com/v2/verifications/612a6091c48ec3001b547767\", \"step\": { \"status\": 200, \"id\": \"mexican-ine-validation\", \"error\": null, \"data\": { \"documentNumber\": \"179297127\", \"cde\": \"BLMREN92110308H400\", \"ne\": \"3\", \"Distrito Federal\": \"12\", \"ocrNumber\": \"4526087814717\", \"registrationYear\": \"2010\", \"emissionYear\": \"2018\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a608ff0de27001b2266bc\", \"timestamp\": \"2021-08-28T16:13:44.078Z\" } PEP. The response should be: { \"resource\": \"https://api.getmati.com/v2/verifications/612a6091c48ec3001b547767\", \"step\": { \"status\": 200, \"id\": \"mexican-pep-validation\", \"error\": null, \"data\": { \"fullName\": \"ENRIQUE ARTURO BLANCO MIRANDA\", \"isPep\": false }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a608ff0de27001b2266bc\", \"timestamp\": \"2021-08-28T16:13:43.706Z\" }",
                                "{ \"resource\": \"https://api.getmati.com/v2/verifications/612a6091c48ec3001b547767\", \"step\": { \"status\": 200, \"id\": \"mexican-curp-validation\", \"error\": null, \"data\": { \"birthDate\": \"1992-11-03\", \"curp\": \"BAME921103HCHLRN00\", \"fullName\": \"BLANCO MIRANDA ENRIQUE ARTURO\", \"gender\": \"HOMBRE\", \"name\": \"ENRIQUE ARTURO\", \"nationality\": \"MEXICO\", \"surname\": \"BLANCO\", \"secondSurname\": \"MIRANDA\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a608ff0de27001b2266bc\", \"timestamp\": \"2021-08-28T16:13:44.421Z\" }")},
                {
                        VerificationInfoByCountry.CHILE, GovernmentCheckSettings.CHILE,
                        Collections.singletonList(GovernmentChecks.CHILE_CIVIL_VALIDATION),
                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a4d531fb7ec001b89cdee\", \"step\": { \"status\": 200, \"id\": \"chilean-registro-civil-validation\", \"error\": null, \"data\": { \"documentNumber\": \"12.142.658-7\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a4d50f0de27001b2266b1\", \"timestamp\": \"2021-08-28T14:51:39.902Z\" }")
                },
//                {
//                        VerificationInfoByCountry.COSTA_RICA, GovernmentCheckSettings.COSTA_RICA,
//                        Arrays.asList(GovernmentChecks.COSTA_RICO_ATV_VALIDATION, GovernmentChecks.COSTA_RICO_TSE_VALIDATION,
//                                GovernmentChecks.COSTA_RICO_SOCIAL_SECURITY_VALIDATION),
//                        Arrays.asList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a51f7bd1040001bac0c4b\", \"step\": { \"status\": 200, \"id\": \"costa-rican-atv-validation\", \"error\": null, \"data\": { \"fullName\": \"KENDALL ABRAHAN VARGAS SANDOVAL\", \"dateOfRegistration\": \"2014-05-06\", \"documentNumber\": \"011582017033\", \"registeredTaxPayer\": true, \"hasDebts\": false, \"documentStatus\": \"valid\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a51f4f0de27001b2266b4\", \"timestamp\": \"2021-08-28T15:12:14.725Z\" }",
//                                "{ \"resource\": \"https://api.getmati.com/v2/verifications/612a51f7bd1040001bac0c4b\", \"step\": { \"status\": 200, \"id\": \"costa-rican-tse-validation\", \"error\": null, \"data\": { \"documentNumber\": \"115820170\", \"firstName\": \"KENDALL ABRAHAN\", \"lastName\": \"VARGAS \", \"secondSurname\": \"SANDOVAL\", \"deceased\": false, \"dateOfBirth\": \"1994-09-10\", \"nationality\": \"COSTARRICENSE\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a51f4f0de27001b2266b4\", \"timestamp\": \"2021-08-28T15:12:14.323Z\" }",
//                                "{ \"resource\": \"https://api.getmati.com/v2/verifications/612a51f7bd1040001bac0c4b\", \"step\": { \"status\": 200, \"id\": \"costa-rican-social-security-validation\", \"error\": null, \"data\": { \"fullName\": \"KENDALL ABRAHAN VARGAS SANDOVAL\", \"insuranceStatus\": \"uninsured\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a51f4f0de27001b2266b4\", \"timestamp\": \"2021-08-28T15:12:14.244Z\" }")
//                },
                {
                        VerificationInfoByCountry.DOMINICAN_REPUBLIC, GovernmentCheckSettings.DOMINICAN_REPUBLIC,
                        Collections.singletonList(GovernmentChecks.DOMINICAN_REPUBLIC_JCE_VALIDATION),
                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a546abd1040001bac12f4\", \"step\": { \"status\": 200, \"id\": \"dominican-jce-validation\", \"error\": null, \"data\": { \"fullName\": \"ANGEL ENMANUEL KING CASTILLO\", \"documentNumber\": \"223-0063518-6\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a5467f0de27001b2266b5\", \"timestamp\": \"2021-08-28T15:21:47.191Z\" }")
                },
//                {
//                        VerificationInfoByCountry.ECUADOR, GovernmentCheckSettings.ECUADOR,
//                        Collections.singletonList(GovernmentChecks.ECUADOR_REGISTRO_VALIDATION),
//                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a5785ddbdad001b42f976\", \"step\": { \"status\": 200, \"id\": \"ecuadorian-registro-civil-validation\", \"error\": null, \"data\": { \"documentNumber\": \"1350743595\", \"fullName\": \"CEVALLOS FIGUEROA EVELYN MARILYN\", \"status\": \"CIUDADANO\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a5782f0de27001b2266b7\", \"timestamp\": \"2021-08-28T15:37:26.933Z\" }")
//                },
//                {
//                        VerificationInfoByCountry.GUATEMALA, GovernmentCheckSettings.GUATEMALA,
//                        Collections.singletonList(GovernmentChecks.GUATEMALA_TSE_VALIDATION),
//                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a5d3c3f295f001b83a946\", \"step\": { \"status\": 200, \"id\": \"guatemalan-tse-validation\", \"error\": null, \"data\": { \"fullName\": \"MARVELIO LEVI, LIMA , \" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a2cd84ddedd001bdfc33d\", \"timestamp\": \"2021-08-28T15:59:29.295Z\" }")
//                },
//                {
//                        VerificationInfoByCountry.HONDURAS, GovernmentCheckSettings.HONDURAS,
//                        Collections.singletonList(GovernmentChecks.HONDURAS_RNP_VALIDATION),
//                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a5ef6bd1040001bac3219\", \"step\": { \"status\": 200, \"id\": \"honduran-rnp-validation\", \"error\": null, \"data\": { \"documentNumber\": \"0704-1981-00221\", \"fullName\": \"GLEDIA YOLANY PASTOR AGUILAR\", \"city\": \"Distrito Central, Francisco Morazán\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a5ef3f0de27001b2266bb\", \"timestamp\": \"2021-08-28T16:10:23.170Z\" }")
//                },
                {
                        VerificationInfoByCountry.PERU, GovernmentCheckSettings.PERU,
                        Collections.singletonList(GovernmentChecks.PERU_RENIEC_VALIDATION),
                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a689bbc3851001b9d7695\", \"step\": { \"status\": 200, \"id\": \"peruvian-reniec-validation\", \"error\": null, \"data\": { \"firstName\": \"CARLOS\", \"middleName\": \"ANTONIO\", \"surname\": \"DE LA TORRE\", \"secondSurname\": \"HERNANDEZ\", \"dniNumber\": \"49001322\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a6899f0de27001b2266bf\", \"timestamp\": \"2021-08-28T16:47:53.206Z\" }")
                },
//                {
//                        VerificationInfoByCountry.PANAMA, GovernmentCheckSettings.PANAMA,
//                        Collections.singletonList(GovernmentChecks.PANAMA_TRIBUNAL_ELECTORAL_VALIDATION),
//                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a6c88c48ec3001b549ccd\", \"step\": { \"status\": 200, \"id\": \"panamenian-tribunal-electoral-validation\", \"error\": null, \"data\": { \"fullName\": \"Miguel Arturo Pereyra Garrido\", \"dateOfRequest\": \"2011-11-29\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a6c85f0de27001b2266c1\", \"timestamp\": \"2021-08-28T17:05:31.782Z\" }")
//                },
//                {
//                        VerificationInfoByCountry.SALVADOR, GovernmentCheckSettings.SALVADOR,
//                        Collections.singletonList(GovernmentChecks.SALVADOR_TSE_VALIDATION),
//                        Collections.singletonList("{ \"resource\": \"https://api.getmati.com/v2/verifications/612a6aa7ddbdad001b433165\", \"step\": { \"status\": 200, \"id\": \"salvadorian-tse-validation\", \"error\": null, \"data\": { \"documentNumber\": \"02836377-3\", \"fullName\": \"ANNER NOE RIVAS GRANADOS\", \"city\": \"SAN MIGUEL\", \"state\": \"SAN MIGUEL\", \"address\": null }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"612a6aa4f0de27001b2266c0\", \"timestamp\": \"2021-08-28T16:58:47.887Z\" }")
//                }
        };
    }
}
