package com.mati.web.tests.sanitychecks.verificationcheck;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.model.requests.Body;
import com.mati.model.requests.Requests;
import com.mati.utils.JsonUtils;
import com.mati.utils.PostBinUtils;
import com.mati.utils.RandomUtils;
import com.mati.utils.SchemaValidatorUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class IpCheck extends BaseWebTest {

    private String flowId;
    private final String idValidation = "ip-validation";
    private final VerificationInfoByCountry argentinaVerification = VerificationInfoByCountry.ARGENTINA;
    private final String schema = "src/test/resources/schema/ipCheckSchema.json";


    @Test(description = "C82, C14137 - IP check")
    public void isIpChecked() throws IOException, ProcessingException {
        loginHelper.loginMatiSuccessful();
        flowId = ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomString())
                .getBody()
                .as(FlowModel.class)
                .getId();

        String binId = PostBinUtils.createBin();
        flowHelper
                .navigateToFlow(flowId)
                .deleteProductFromFlowBuilder(Products.BIOMETRIC_VERIFICATION)
                .moveProductToFlowBuilder(Products.IP_CHECK);

        flowHelper
                .closeProductSettings()
                .clickWebHookButton()
                .fillSecretInput(PostBinUtils.SECRET_KEY)
                .fillUrlInput(PostBinUtils.POST_BIN_URL + binId)
                .clickButtonSave()
                .waitUntilChangesWillBeSaved()
                .close();

        flowHelper
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved()
                .createVerification(argentinaVerification);

        verificationListHelper
                .navigateToPage()
                .selectVerificationByName(argentinaVerification.getPersonName())
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .clickDocumentVerificationButton()
                .waitUntilVerificationFinished()
                .clickIpCheckButton();

        Body actualRequest = PostBinUtils.getAllRequestsToBin(binId).stream()
                .map(Requests::getBody)
                .filter(it -> !it.getAdditionalProperties().isEmpty() && it.getAdditionalProperties().containsKey("step"))
                .filter(it -> (((HashMap<String, String>) it.getAdditionalProperties().get("step")).get("id")).equals(idValidation))
                .findFirst().get();

        JsonNode actualJson = JsonUtils.objectToJson(actualRequest.getAdditionalProperties());

        assertThat("Schema is correct",
                SchemaValidatorUtils.isValidSchema(schema, actualJson), equalTo(true));

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(verificationObjectHelper.isImgMapDisplayed(), true,
                "Image map is displayed");
        softAssert.assertEquals(verificationObjectHelper.isCountryDisplayed(), true,
                "Is country name displayed");
        softAssert.assertEquals(verificationObjectHelper.isProvinceDisplayed(), true,
                "Is province displayed");
        softAssert.assertEquals(verificationObjectHelper.isCityDisplayed(), true,
                "Is city displayed");
        softAssert.assertEquals(verificationObjectHelper.isZipCodeDisplayed(), true,
                "Is ZipCode displayed");
        softAssert.assertEquals(verificationObjectHelper.isVpnDisplayed(), true,
                "Is VPN displayed");
        softAssert.assertEquals(verificationObjectHelper.isGeoRestrictionsDisplayed(), true,
                "Is Geo Restrictions displayed");
        softAssert.assertAll();
    }

    @AfterMethod
    public void deleteFlow() {
        ApiService.deleteFlows(TokenContainer.getInstance().getMerchant(), flowId);
        verificationListHelper
                .navigateToPage()
                .deleteVerificationByName(argentinaVerification.getPersonName());
    }
}
