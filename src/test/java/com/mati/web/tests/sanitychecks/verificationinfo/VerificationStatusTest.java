package com.mati.web.tests.sanitychecks.verificationinfo;

import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class VerificationStatusTest extends BaseWebTest {

    @BeforeClass
    public void moveToVerificationAndSelectMerchant() {
        loginHelper.loginMatiSuccessful();
        rollUpMenuHelper.selectVerificationListMenu()
                .waitForPageLoad();
        verificationListHelper.selectVerificationByName(IConstants.verificationNameForMexico);
        verificationListHelper.waitToBeInvisibilityDashboardLoader();
    }

    @DataProvider
    public Object[] verificationStatus() {
        return new VerificationStatus[] {VerificationStatus.VERIFIED, VerificationStatus.REVIEW_NEEDED, VerificationStatus.REJECTED};
    }

    @Test(description = "C88 - Change verification status, C314 - Set the final status (Verified/Review/Rejected)", dataProvider = "verificationStatus")
    public void isVerificationStatusButtonHasCorrectColorTest(VerificationStatus status) {
        verificationObjectHelper.selectVerificationStatus(status);
        verificationObjectHelper.waitSuccessfulAlertToBeVisibility();
        boolean result = verificationObjectHelper.isVerificationStatusButtonHasCorrectColor(status);
        assertThat("Verification status button does not have correct color", result, Matchers.equalTo(true));
    }

    @Test(description = "C88 - Change verification status, , C314 - Set the final status (Verified/Review/Rejected)", dataProvider = "verificationStatus")
    public void isVerificationStatusCorrectTest(VerificationStatus status) {
        verificationObjectHelper.selectVerificationStatus(status);
        verificationObjectHelper.waitSuccessfulAlertToBeVisibility();
        boolean result = verificationObjectHelper.isVerificationStatusCorrect(status);
        assertThat("Verification status does not match selected", result, Matchers.equalTo(true));
    }
}
