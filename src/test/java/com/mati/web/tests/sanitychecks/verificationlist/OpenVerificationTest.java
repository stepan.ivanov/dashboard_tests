package com.mati.web.tests.sanitychecks.verificationlist;

import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class OpenVerificationTest extends BaseWebTest {

    @BeforeClass
    public void logIn() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(description = "C100 - Open the new verification")
    public void isNewVerificationOpenedTest() {
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .clickVerifyMeButton()
                .createVerification(VerificationInfoByCountry.MEXICO, IConstants.frontMexico, IConstants.backMexico);
        verificationListHelper.navigateToPage()
                .selectNewVerification();
        verificationObjectHelper.waitForPageLoad();
        assertThat("New verification is not opened", verificationObjectHelper.isVerificationObjectPage(), Matchers.equalTo(true));
    }

    @Test(description = "C101 - Open the old verification (which was done 3 or more months ago)", enabled = false)
    public void isOldVerificationOpenedTest() {
        verificationListHelper.navigateToPage()
                .selectSortDescendingDate()
                .selectOldVerification();
        assertThat("Old verification is not opened", verificationObjectHelper.isVerificationObjectPage(), Matchers.equalTo(true));
    }
}
