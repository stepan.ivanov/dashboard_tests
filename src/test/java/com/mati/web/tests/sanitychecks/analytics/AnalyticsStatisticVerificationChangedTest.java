package com.mati.web.tests.sanitychecks.analytics;

import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AnalyticsStatisticVerificationChangedTest extends BaseWebTest {

    @Test(description = "C8451 - Analytic page: Check that statistic for final verification status changed")
    public void analyticsStatisticVerificationChangedTest() {
        loginHelper.loginMatiSuccessful();

        int expectedVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int expectedVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int expectedRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        verificationListHelper
                .navigateToPage()
                .selectVerificationByStatus(VerificationStatus.VERIFIED)
//
                .switchToNewView()
//
                .selectVerificationStatus(VerificationStatus.REJECTED)
                .waitSuccessfulAlertToBeVisibility();

        analyticsHelper
                .navigateToPage();

        int actualVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int actualVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int actualRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(actualVerificationsAmount, expectedVerificationsAmount,
                "Verifications amount the same");
        softAssert.assertEquals(actualRejectedUsersAmount, expectedRejectedUsersAmount + 1,
                "Rejected amount user was increased by 1");
        softAssert.assertEquals(actualVerifiedUsersAmount, expectedVerifiedUsersAmount - 1,
                "Verified amount user was decreased by 1");
        softAssert.assertAll();

        verificationListHelper
                .navigateToPage()
                .selectVerificationByStatus(VerificationStatus.REJECTED)
//
                .switchToNewView()
//
                .selectVerificationStatus(VerificationStatus.VERIFIED)
                .waitSuccessfulAlertToBeVisibility();

        analyticsHelper
                .navigateToPage();

        actualVerificationsAmount = analyticsHelper.getVerificationsAmount();
        actualVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        actualRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        softAssert = new SoftAssert();
        softAssert.assertEquals(actualVerificationsAmount, expectedVerificationsAmount,
                "Verifications amount the same");
        softAssert.assertEquals(actualRejectedUsersAmount, expectedRejectedUsersAmount,
                "Rejected amount user the same");
        softAssert.assertEquals(actualVerifiedUsersAmount, expectedVerifiedUsersAmount,
                "Verified amount user the same");
        softAssert.assertAll();
    }
}
