package com.mati.web.tests.sanitychecks.validationcheck;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.model.requests.Body;
import com.mati.utils.JsonUtils;
import com.mati.utils.PostBinUtils;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.mati.enums.verificationchecks.DocumentVerificationChecks.WATCHLIST_DETECTION;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class WatchListTest extends BaseWebTest {

    private List<String> flows = new ArrayList<>();
    private List<VerificationInfoByCountry> verifications = new ArrayList<>();

    @BeforeClass
    public void loginToMati() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(dataProvider = "data-provider",
            description = "C402 - Person is in watchlist" +
                    "C350 - Person is not in watchlist")
    public void watchListTest(VerificationInfoByCountry verificationInfoByCountry, String front_photo, String personName,
                              boolean expectedDetection, String expectedJson, String expectedResponse) {
        String flowId = ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomString())
                .getBody()
                .as(FlowModel.class)
                .getId();
        flows.add(flowId);
        String binId = PostBinUtils.createBin();
        verifications.add(verificationInfoByCountry);
        flowHelper
                .navigateToFlow(flowId)
                .deleteProductFromFlowBuilder(Products.BIOMETRIC_VERIFICATION);

        flowHelper
                .clickWebHookButton()
                .fillSecretInput(PostBinUtils.SECRET_KEY)
                .fillUrlInput(PostBinUtils.POST_BIN_URL + binId)
                .clickButtonSave()
                .waitUntilChangesWillBeSaved()
                .close();

        flowHelper
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved()
                .createVerification(verificationInfoByCountry, front_photo, verificationInfoByCountry.getBackSide());

        verificationListHelper
                .navigateToPage()
                .selectVerificationByName(personName)
                .waitForPageLoad()
                //Workaround for switching to new view
                .switchToNewView()
                //
                .clickDocumentVerificationButton()
                .waitUntilVerificationFinished();

        assertThat("Watch list detection passed",
                verificationObjectHelper.isVerificationCheckSuccess(WATCHLIST_DETECTION), equalTo(expectedDetection));

        Body expectedValidation = JsonUtils.readValue(expectedResponse, Body.class);
        String idValidation = ((HashMap<String, String>) expectedValidation.getAdditionalProperties().get("step")).get("id");
        Body actualRequest = PostBinUtils.getRequestsByValidationId(binId, idValidation);
        SoftAssert validationAssert = new SoftAssert();
        validationAssert.assertEquals(actualRequest.getEventName(), expectedValidation.getEventName(),
                "Event name is: " + expectedValidation.getEventName());
        validationAssert.assertEquals(actualRequest.getAdditionalProperties(), expectedValidation.getAdditionalProperties(),
                "Response body is the same");
        validationAssert.assertAll();

        verificationObjectHelper
                .clickOnVerificationDataButton();

        assertThat("verificationData contains necessary json",
                verificationDataObjectModelHelper.getResponsesAsText().contains(expectedJson), equalTo(true));
    }

    @AfterClass(alwaysRun = true)
    public void deleteFlow() {
        flows.forEach(flowId -> ApiService.deleteFlows(TokenContainer.getInstance().getMerchant(), flowId));
        verifications.forEach(verificationInfoByCountry -> verificationListHelper
                .navigateToPage()
                .deleteVerificationByName(verificationInfoByCountry.getPersonName()));
    }

    @DataProvider(name = "data-provider")
    public Object[][] provideData() {
        return new Object[][]{
                {
                        VerificationInfoByCountry.MEXICO, "mexico_front_brake", "Mugabe Robert Gabriel", false,
                        "{  status: 200,  id: \"watchlists\",  error: {type: \"LegacyError\",code: \"legacy.error\",message: \"We found a match inside a watchlist\"  }}  ",
                        "{ \"resource\": \"https://api.stage.getmati.com/v2/verifications/6140bcaf9ecb12001bf33177\", \"step\": { \"status\": 200, \"id\": \"watchlists\", \"error\": { \"type\": \"LegacyError\", \"code\": \"legacy.error\", \"message\": \"We found a match inside a watchlist\" }, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"61408a2e4b0a01001b794ee8\", \"timestamp\": \"2021-09-14T15:16:34.787Z\" }"
                },
                {VerificationInfoByCountry.MEXICO, VerificationInfoByCountry.MEXICO.getFrontSide(),
                        VerificationInfoByCountry.MEXICO.getPersonName(), true, "{  status: 200,  id: \"watchlists\",  error: null}",
                        "{ \"resource\": \"https://api.stage.getmati.com/v2/verifications/6140ba7c9ecb12001bf330d2\", \"step\": { \"status\": 200, \"id\": \"watchlists\", \"error\": null, \"documentType\": \"national-id\" }, \"eventName\": \"step_completed\", \"flowId\": \"61408a2e4b0a01001b794ee8\", \"timestamp\": \"2021-09-14T15:08:35.721Z\" }"}
        };
    }
}
