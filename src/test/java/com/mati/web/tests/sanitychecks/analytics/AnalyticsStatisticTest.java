package com.mati.web.tests.sanitychecks.analytics;

import com.mati.enums.Period;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AnalyticsStatisticTest extends BaseWebTest {

    @Test(description = "С491 - Check statistic for Last 7 Days / All flows")
    public void analyticsStatisticTest() {
        loginHelper.loginMatiSuccessful();
        analyticsHelper
                .openFilters()
                .setFilterPeriod(Period.LAST_SEVEN_DAYS)
                .applyFilter();

        int expectedVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int expectedVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int expectedRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        verificationListHelper
                .navigateToPage()
                .selectVerificationByStatus(VerificationStatus.VERIFIED)
//
                .switchToNewView()
//
                .selectVerificationStatus(VerificationStatus.REJECTED)
                .waitSuccessfulAlertToBeVisibility();

        analyticsHelper
                .navigateToPage()
                .openFilters()
                .setFilterPeriod(Period.LAST_SEVEN_DAYS)
                .applyFilter();

        int actualVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int actualVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int actualRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(actualVerificationsAmount, expectedVerificationsAmount,
                "Verifications amount the same");
        softAssert.assertEquals(actualRejectedUsersAmount, expectedRejectedUsersAmount + 1,
                "Rejected amount user was increased by 1");
        softAssert.assertEquals(actualVerifiedUsersAmount, expectedVerifiedUsersAmount - 1,
                "Verified amount user was decreased by 1");
        softAssert.assertAll();
    }
}
