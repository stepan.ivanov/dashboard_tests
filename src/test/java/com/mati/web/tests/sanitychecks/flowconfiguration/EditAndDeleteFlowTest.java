package com.mati.web.tests.sanitychecks.flowconfiguration;

import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class EditAndDeleteFlowTest extends BaseWebTest {

    @BeforeClass
    public void loginAndCreateVerificationFlow() {
        loginHelper.loginMatiSuccessful();
        verificationFlowsHelper.navigateToPage();
    }

    @Test(description = "C9539 - Edit flow name / change flow name")
    public void isFlowNameEditedTest() {
        String newFlowName = RandomUtils.randomStringWithPrefix();
        verificationFlowsHelper.createFlow(RandomUtils.randomStringWithPrefix());
        flowHelper.clickFlowSettingsButton()
                .clickEditFlowNameButton()
                .fillFlowName(newFlowName)
                .clickMarkIconButton()
                .clickSaveChangesButton();
        verificationFlowsHelper.navigateToPage();
        assertThat(
                "Flow name was not saved after editing and saving",
                verificationFlowsHelper.isFlowExistInList(newFlowName),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C9695 - Delete flow from the \"Verification flow\" list (in the flow)")
    public void isFLowDeletedThroughSettingsTest() {
        String flowName = RandomUtils.randomStringWithPrefix();
        verificationFlowsHelper.createFlow(flowName);
        flowHelper.clickFlowSettingsButton().clickDeleteFlowButton().clickConfirmDeleteFlowButton();
        assertThat(
                "Flow name was not deleted through settings",
                verificationFlowsHelper.isFlowExistInList(flowName),
                Matchers.equalTo(false)
        );
    }

    @Test(description = "C9696 - Delete flow from \"Verification flow\" list (out of the flow / from entire list)")
    public void isFLowDeletedThroughFlowsTest() {
        String flowName = RandomUtils.randomStringWithPrefix();
        verificationFlowsHelper.createFlow(flowName);
        verificationFlowsHelper.navigateToPage().removeFlowByName(flowName);
        assertThat(
                "Flow name was not deleted through settings",
                verificationFlowsHelper.isFlowExistInList(flowName),
                Matchers.equalTo(false)
        );
    }
}
