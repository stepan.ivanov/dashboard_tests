package com.mati.web.tests.sanitychecks.phonevalidation;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.PhoneCheckSettings;
import com.mati.model.requests.Body;
import com.mati.utils.JsonUtils;
import com.mati.utils.PostBinUtils;
import com.mati.utils.RandomUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

public class PhoneValidationTest extends BaseWebTest {

    private String flowId;

    @BeforeClass
    public void login() {
        loginHelper.loginMatiSuccessful();
    }

    @Test(dataProvider = "data-provider",
            description = "C751 - SMS Verification step functioning" +
                    "C753 - Make step optional")
    public void phoneVerificationTest(String request, boolean skip) {
        flowId = ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomString())
                .getBody()
                .as(FlowModel.class)
                .getId();
        String binId = PostBinUtils.createBin();

        flowHelper
                .navigateToFlow(flowId)
                .clickWebHookButton()
                .fillSecretInput(PostBinUtils.SECRET_KEY)
                .fillUrlInput(PostBinUtils.POST_BIN_URL + binId)
                .clickButtonSave()
                .waitUntilChangesWillBeSaved()
                .close();

        flowHelper
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.PHONE_CHECK)
                .selectSettingCheckboxes(PhoneCheckSettings.MAKE_STEP_OPTIONAL, true)
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved()
                .createPhoneCheckVerification(VerificationInfoByCountry.UNITED_STATES, IConstants.phoneNumberUS, skip);

        Body expectedValidation = JsonUtils.readValue(request, Body.class);
        String idValidation = ((HashMap<String, String>) expectedValidation.getAdditionalProperties().get("step")).get("id");

        Body actualRequest = PostBinUtils.getRequestsByValidationId(binId, idValidation);

        SoftAssert validationAssert = new SoftAssert();
        validationAssert.assertEquals(actualRequest.getEventName(), expectedValidation.getEventName(),
                "Event name is: " + expectedValidation.getEventName());
        validationAssert.assertEquals(actualRequest.getAdditionalProperties(), expectedValidation.getAdditionalProperties(),
                "Response body is the same");
        validationAssert.assertAll();
    }

    @AfterMethod
    public void deleteFlow() {
        ApiService.deleteFlows(TokenContainer.getInstance().getMerchant(), flowId);
    }

    @DataProvider(name = "data-provider")
    public Object[][] provideData() {
        return new Object[][]{
                {"{ \"resource\": \"https://api.stage.getmati.com/v2/verifications/60e2d41e316558001b71b374\", \"step\": { \"status\": 200, \"id\": \"phone-ownership-validation\", \"data\": { \"phoneNumber\": \"2163547758\", \"countryCode\": \"US\", \"dialingCode\": 1 }, \"error\": null }, \"eventName\": \"step_completed\", \"flowId\": \"60d4c747916413001b616d97\", \"timestamp\": \"2021-07-05T09:43:18.735Z\" }"
                        , false},
                {"{ \"resource\": \"https://api.stage.getmati.com/v2/verifications/60f7df2e7cce86001b8baf9d\", \"step\": { \"status\": 200, \"id\": \"phone-ownership-validation\", \"error\": { \"type\": \"StepError\", \"code\": \"phoneOwnership.skipped\", \"message\": \"User skipped this step\" } }, \"eventName\": \"step_completed\", \"flowId\": \"60f7df29ce39bd001b4c0a23\", \"timestamp\": \"2021-07-21T08:48:33.891Z\" }",
                        true}
        };
    }
}
