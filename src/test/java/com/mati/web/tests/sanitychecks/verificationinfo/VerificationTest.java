package com.mati.web.tests.sanitychecks.verificationinfo;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class VerificationTest extends BaseWebTest {

    @BeforeClass
    public void login() {
        loginHelper.loginMatiSuccessful();
    }

    @BeforeMethod
    public void createVerification() {
        flowHelper.navigateToFlow(IConstants.AQA_FLOW_ID)
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.DOCUMENT_VERIFICATION)
                .addDocumentToDVSettings("National ID", true)
                .clickSaveAndPublishButton()
                .clickVerifyMeButton()
                .createVerification(VerificationInfoByCountry.BRAZIL,
                             VerificationInfoByCountry.BRAZIL.getFrontSide(),
                            VerificationInfoByCountry.BRAZIL.getBackSide());
        verificationListHelper.navigateToPage();
    }

    @Test(description = "C316 - Delete verification")
    public void deleteVerificationOnVerificationListPageTest() {
        verificationListHelper.deleteVerificationByName(VerificationInfoByCountry.BRAZIL.getPersonName());
        assertThat(
                "Verification was not delete",
                verificationListHelper.isVerificationExist(VerificationInfoByCountry.BRAZIL.getPersonName()),
                Matchers.equalTo(false));
    }

    @Test(description = "C316 - Delete verification")
    public void deleteVerificationOnVerificationObjectPageTest() {
        verificationListHelper.selectVerificationByName(VerificationInfoByCountry.BRAZIL.getPersonName())
                .waitForPageLoad()
                .deleteVerification();
        assertThat("Verification was not delete",
                verificationListHelper.isVerificationExist(VerificationInfoByCountry.BRAZIL.getPersonName()),
                Matchers.equalTo(false));
    }
}
