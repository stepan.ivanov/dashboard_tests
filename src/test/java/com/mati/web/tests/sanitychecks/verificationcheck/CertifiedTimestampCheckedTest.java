package com.mati.web.tests.sanitychecks.verificationcheck;

import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CertifiedTimestampCheckedTest extends BaseWebTest {

    private final String flowName = RandomUtils.randomString();

    @Test(description = "C9519, C11950 - Certified Timestamp is checked")
    public void isCertifiedTimestampChecked() {
        loginHelper.loginMatiSuccessful();
        rollUpMenuHelper
                .selectVerificationFlowsMenu()
                .createFlow(flowName);

        flowHelper.clickFlowSettingsButton()
                .selectTimestampCheckBox(true)
                .clickSaveChangesButton();

        assertThat("Certified Timestamp is checked",
                flowHelper.clickFlowSettingsButton().isTimestampCheckBoxSelected(), equalTo(true));
    }

    @AfterMethod
    public void deleteFlow() {
        verificationFlowsHelper
                .navigateToPage()
                .removeFlowByName(flowName);
    }
}
