package com.mati.web.tests.sanitychecks.phonevalidation;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.PhoneCheckSettings;
import com.mati.model.requests.Body;
import com.mati.utils.JsonUtils;
import com.mati.utils.PostBinUtils;
import com.mati.utils.RandomUtils;
import com.mati.web.IConstants;
import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

public class PhoneValidationRiskTest extends BaseWebTest {

    private String flowId;
    private final String request = "{ \"resource\":\"https://api.stage.getmati.com/v2/verifications/614084b4711c10001bd7d392\", \"eventName\":\"step_completed\", \"flowId\":\"6140849f4b0a01001b794988\", \"timestamp\":\"2021-09-14T11:17:46.871Z\", \"step\":{ \"status\":200, \"id\":\"phone-risk-analysis-validation\", \"error\":{ \"type\":\"StepError\", \"code\":\"phoneRisk.highRisk\", \"message\":\"The phone number that the user provided appears have suspicious behavior and properties\" }, \"data\":{ \"phoneNumber\":\"2163547758\", \"dialingCode\":1, \"countryCode\":\"US\", \"phoneType\":\"Voip\", \"phoneTypeStatus\":\"Blocked\", \"riskLevel\":\"very-high\", \"riskInsights\":[ { \"name\":\"high-risk irregular activity\", \"description\":\"Highest-risk category, based on past behavior.\", \"impact\":\"Negative\", \"type\":\"Category\" }, { \"name\":\"no range activity\", \"description\":\"Very little activity, or none at all, for a risky range that this number belongs to over the past 90 days. Also returned if the number does not belong to a risky range.\", \"impact\":\"Positive\", \"type\":\"Automation to human activity\" }, { \"name\":\"seen in the last 1 day\", \"description\":\"Activity on this number in the last 24 hours.\", \"impact\":\"Neutral\", \"type\":\"Human to human activity\" }, { \"name\":\"very low number of completed calls\", \"description\":\"Much less success than expected, or no success at all, for this number over the past 90 days.\", \"impact\":\"Negative\", \"type\":\"Human to human activity\" }, { \"name\":\"regular call duration\", \"description\":\"Expected call duration for this number over the past 90 days.\", \"impact\":\"Positive\", \"type\":\"Human to human activity\" }, { \"name\":\"sparse long-term activity\", \"description\":\"Lower than expected activity on this number over a tenure greater than or equal to one week (long-term).\", \"impact\":\"Positive\", \"type\":\"Human to human activity\" }, { \"name\":\"no range activity\", \"description\":\"Very little activity, or none at all, for a risky range that this number belongs to over the past 90 days. Also returned if the number does not belong to a risky range.\", \"impact\":\"Positive\", \"type\":\"Human to human activity\" }, { \"name\":\"VOIP number\", \"description\":\"This is a VOIP number.\", \"impact\":\"Negative\", \"type\":\"Number type\" }, { \"name\":\"number used by application\", \"description\":\"TeleSign or BICS reserved this number for use by customers with our applications (for example to send verification messages), but it appears that it is being used for a different purpose.\", \"impact\":\"Negative\", \"type\":\"Number type\" } ], \"riskScore\":100, \"maxPossibleRiskScore\":100, \"recommendation\":\"block\", \"timezone\":\"-5\", \"lineType\":\"VOIP\", \"carrier\":\"Onvoy, LLC - OH\", \"location\":{ \"city\":\"Cleveland\", \"state\":null, \"zip\":null, \"metroCode\":null, \"county\":null, \"country\":\"United States\" } } } }";

    @Test(description = "C752 - SMS risk score works according to threshold")
    public void phoneVerificationTest() {
        loginHelper.loginMatiSuccessful();
        flowId = ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomString())
                .getBody()
                .as(FlowModel.class)
                .getId();

        String binId = PostBinUtils.createBin();

        flowHelper
                .navigateToFlow(flowId)
                .clickWebHookButton()
                .fillSecretInput(PostBinUtils.SECRET_KEY)
                .fillUrlInput(PostBinUtils.POST_BIN_URL + binId)
                .clickButtonSave()
                .waitUntilChangesWillBeSaved()
                .close();

        flowHelper
                .deleteProductsFromFlowBuilder()
                .moveProductToFlowBuilder(Products.PHONE_CHECK)
                .selectSettingCheckboxes(PhoneCheckSettings.RISK_ANALYSIS, true)
                .clickSaveAndPublishButton()
                .waitUntilChangesWillBeSaved()
                .createPhoneCheckVerification(VerificationInfoByCountry.UNITED_STATES, IConstants.phoneNumberUS, false);

        verificationListHelper
                .navigateToPage()
                .selectFirstVerification()
//
                .switchToNewView()
//
                .clickPhoneCheckButton();


        Body expectedValidation = JsonUtils.readValue(request, Body.class);
        String idValidation = ((HashMap<String, String>) expectedValidation.getAdditionalProperties().get("step")).get("id");

        Body actualRequest = PostBinUtils.getRequestsByValidationId(binId, idValidation);

        SoftAssert validationAssert = new SoftAssert();
        validationAssert.assertEquals(actualRequest.getEventName(), expectedValidation.getEventName(),
                "Event name is: " + expectedValidation.getEventName());
        validationAssert.assertEquals(actualRequest.getAdditionalProperties(), expectedValidation.getAdditionalProperties(),
                "Response body is the same");
        validationAssert.assertAll();


        SoftAssert uiAssert = new SoftAssert();
        uiAssert.assertEquals(verificationObjectHelper.isCountryCodeInValidationsWindowDisplayed(), true,
                "CountryCode in validation window is displayed");
        uiAssert.assertEquals(verificationObjectHelper.isStatusDisplayed(), true,
                "Status is displayed");
        uiAssert.assertEquals(verificationObjectHelper.isCellphoneNumberInValidationsWindowDisplayed(), true,
                "Cell phone number in validation window is Displayed");
        uiAssert.assertEquals(verificationObjectHelper.isCountryCodeInRiskWindowDisplayed(), true,
                "CountryCode in risk window is displayed");
        uiAssert.assertEquals(verificationObjectHelper.isRiskLevelDisplayed(), true,
                "Risk level is displayed");
        uiAssert.assertEquals(verificationObjectHelper.isCellphoneNumberInRiskWindowDisplayed(), true,
                "Cell phone number in risk window is Displayed");
        uiAssert.assertEquals(verificationObjectHelper.isRiskScoreDisplayed(), true,
                "Risk score is displayed");
        uiAssert.assertEquals(verificationObjectHelper.isTimeZoneDisplayed(), true,
                "Time zone is displayed");
        uiAssert.assertEquals(verificationObjectHelper.isLineTypeDisplayed(), true,
                "Line Type is displayed");
        uiAssert.assertEquals(verificationObjectHelper.isCarrierDisplayed(), true,
                "Carrier is displayed");
        uiAssert.assertAll();
    }

    @AfterMethod
    public void deleteFlow() {
        ApiService.deleteFlows(TokenContainer.getInstance().getMerchant(), flowId);
    }
}
