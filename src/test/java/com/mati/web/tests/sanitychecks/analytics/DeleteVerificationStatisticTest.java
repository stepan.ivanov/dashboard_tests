package com.mati.web.tests.sanitychecks.analytics;

import com.mati.web.tests.BaseWebTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class DeleteVerificationStatisticTest extends BaseWebTest {

    @Test(description = "C15485 - Analytic page: Check that statistic doesn't changed if merchant deleted the verification")
    public void deleteVerificationStatisticTest() {
        loginHelper.loginMatiSuccessful();
        int expectedVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int expectedVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int expectedRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        verificationListHelper
                .navigateToPage()
                .selectFirstVerification()
                .deleteVerification();

        analyticsHelper
                .navigateToPage()
                .waitForPageLoad();

        int actualVerificationsAmount = analyticsHelper.getVerificationsAmount();
        int actualVerifiedUsersAmount = analyticsHelper.getVerifiedUsersAmount();
        int actualRejectedUsersAmount = analyticsHelper.getRejectedUsersAmount();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(actualVerificationsAmount, expectedVerificationsAmount,
                "Verifications amount the same");
        softAssert.assertEquals(actualRejectedUsersAmount, expectedRejectedUsersAmount,
                "Rejected amount user must be the same");
        softAssert.assertEquals(actualVerifiedUsersAmount, expectedVerifiedUsersAmount,
                "Verified amount user must be the same");
        softAssert.assertAll();
    }
}
