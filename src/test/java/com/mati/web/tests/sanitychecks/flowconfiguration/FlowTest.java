package com.mati.web.tests.sanitychecks.flowconfiguration;

import com.mati.api.ApiService;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.verificationflows.Products;
import com.mati.utils.RandomUtils;
import com.mati.web.tests.BaseWebTest;
import org.hamcrest.Matchers;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class FlowTest extends BaseWebTest {

    private String flowId;

    @BeforeClass
    public void loginAndCreateVerificationFlow() {
        flowId = ApiService.createFlow(TokenContainer.getInstance().getMerchant(), RandomUtils.randomStringWithPrefix())
                .getBody()
                .as(FlowModel.class)
                .getId();
        loginHelper.loginMatiSuccessful();
        flowHelper.navigateToFlow(flowId);
    }

    @Test(description = "C9538 - Create new flow")
    public void isFlowCreatedTest() {
        assertThat(
                "Flow was not created",
                flowHelper.isFlowNameDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C9539 - Edit flow name / change flow name")
    public void isFlowOpenedTest() {
        assertThat(
                "Flow was not opened",
                flowHelper.isFlowNameDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C9692 - Insert webhook link into the flow")
    public void isWebhookLinkAddedSuccessfullyTest() {
        boolean isWebhookSaved = flowHelper.clickWebHookButton()
                .fillUrlInput(RandomUtils.randomURL())
                .fillSecretInput(RandomUtils.randomString())
                .clickButtonSave()
                .isAlertMessageDisplayed();
        flowHelper.closeModalWindow();
        assertThat(
                "'Webhook was changed' alert is not displayed",
                isWebhookSaved,
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C9693 - Edit and save \"User flow configuration\"")
    public void isChangesSavedTest() {
        flowHelper.moveProductToFlowBuilder(Products.IP_CHECK).clickSaveAndPublishButton();
        assertThat(
                "'Changes have been saved' alert is not displayed",
                flowHelper.isSavedChangesAlertDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @Test(description = "C9694 - Delete any product from \"User's flow\"")
    public void isProductDeletedFromFLowBuilderTest() {
        flowHelper.moveProductToFlowBuilder(Products.PHONE_CHECK)
                .deleteProductFromFlowBuilder(Products.PHONE_CHECK);
        assertThat(
                "Product was delete from flow builder",
                flowHelper.isUnsavedChangesAlertDisplayed(),
                Matchers.equalTo(true)
        );
    }

    @AfterClass
    public void deleteTestFlow() {
        ApiService.deleteFlows(TokenContainer.getInstance().getMerchant(), flowId);
    }
}