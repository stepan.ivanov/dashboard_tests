package com.mati.web.tests;

import com.mati.web.DriverUtils;
import com.mati.web.helpers.GmailHelper;
import com.mati.web.helpers.login.ContactUsHelper;
import com.mati.web.helpers.login.LoginHelper;
import com.mati.web.helpers.login.RecoveryPasswordHelper;
import com.mati.web.helpers.login.ResetPasswordHelper;
import com.mati.web.helpers.merchantdashboard.*;
import com.mati.web.helpers.signup.SignUpHelper;
import com.mati.web.listeners.TestListener;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import java.util.AbstractSequentialList;
import java.util.LinkedList;

@Listeners({TestListener.class
//        , ReportPortalTestNGListener.class
})
public class BaseWebTest extends BaseAPITest {

    protected LoginHelper loginHelper = new LoginHelper();
    protected AnalyticsHelper analyticsHelper = new AnalyticsHelper();
    protected ReviewModeHelper reviewModeHelper = new ReviewModeHelper();
    protected VerificationListHelper verificationListHelper = new VerificationListHelper();
    protected GmailHelper gmailHelper = new GmailHelper();
    protected RecoveryPasswordHelper recoveryPasswordHelper = new RecoveryPasswordHelper();
    protected VerificationObjectHelper verificationObjectHelper = new VerificationObjectHelper();
    protected VerificationFlowsHelper verificationFlowsHelper = new VerificationFlowsHelper();
    protected FlowHelper flowHelper = new FlowHelper();
    protected RollUpMenuHelper rollUpMenuHelper = new RollUpMenuHelper();
    protected SignUpHelper signUpHelper = new SignUpHelper();
    protected InvitationPageHelper invitationPageHelper = new InvitationPageHelper();
    protected SettingsPageHelper settingsPageHelper = new SettingsPageHelper();
    protected AgentHistoryPageHelper agentHistoryPageHelper = new AgentHistoryPageHelper();
    protected DevHelper devHelper = new DevHelper();
    protected WebHookMenuHelper webHookMenuHelper = new WebHookMenuHelper();
    protected ResetPasswordHelper resetPasswordHelper = new ResetPasswordHelper();
    protected VerificationDataObjectModalHelper verificationDataObjectModelHelper =
            new VerificationDataObjectModalHelper();
    protected VerificationHistoryPageHelper verificationHistoryPageHelper =
            new VerificationHistoryPageHelper();
    protected ContactUsHelper contactUsHelper = new ContactUsHelper();

    @BeforeClass(alwaysRun = true)
    public void setChrome() {
        DriverUtils.runBrowser();
//        JavascriptExecutor js = (JavascriptExecutor) WebDriverRunner.getWebDriver();
//        Set<String> keySet = ((WebStorage) WebDriverRunner.getWebDriver()).getLocalStorage().keySet();
//        Map<String, String> values = new HashMap<>();
//        keySet.forEach(
//                key -> values.put(key, ((WebStorage) WebDriverRunner.getWebDriver()).getLocalStorage().getItem(key))
//        );
//        rollUpMenuHelper.clickLogoutButton().clickConfirmPopUpButton();
//        values.keySet()
//                .forEach(
//                        key -> js.executeScript("localStorage.setItem(arguments[0],arguments[1])", key, values.get(key))
//                );
    }

    @AfterClass(alwaysRun = true)
    public void closeAll() {
        DriverUtils.tearDownBrowser();
    }
}
