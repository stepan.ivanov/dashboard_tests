package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

public class VerificationDataObjectModal {

    private WebElement modalWindow = Selenide.$x("//div[@data-role='modal']");
    private WebElement responses = Selenide.$x("//pre[@data-qa='verification-data-json-value']").lastChild();

    public boolean isModalWindowDisplayed() {
        return modalWindow.isDisplayed();
    }

    public VerificationDataObjectModal waitPageLoaded() {
        WaitUtils.waitElementToBeVisible(modalWindow);
        return this;
    }

    public String getResponsesAsText() {
        return responses.getText()
                .replaceAll("\n", "")
                .replaceAll("    ", "");
    }
}
