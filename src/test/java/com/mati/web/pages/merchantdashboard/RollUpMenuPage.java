package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.enums.rollup.Language;
import com.mati.utils.WaitUtils;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class RollUpMenuPage extends AbstractPage {

    private final WebElement leftMenu =
            Selenide.$x("//div[@id = 'root']//div[contains(@class, 'MuiDrawer-root')]");

    private final WebElement rollUpMenuButton = Selenide.$x("//button[@data-qa='menu-rollUp']");
    private final WebElement analyticsButton = Selenide.$x("//li[@data-qa='menu-metrics']");
    private final WebElement verificationListButton =
            Selenide.$x("//li[@data-qa='menu-verificationList']");
    private final WebElement verificationFlowButton =
            Selenide.$x("//li[@data-qa='menu-verificationFlows']");
    private final WebElement forDevelopersButton =
            Selenide.$x("//li[@data-qa='menu-forDevelopers']");
    private final WebElement faqButton = Selenide.$x("//li[@data-qa='menu-faq']");
    private final WebElement whatsNewButton = Selenide.$x("//li[@data-qa='menu-whats-new']");
    private final WebElement inviteTeammateButton =
            Selenide.$x("//button[@data-qa='menu-inviteTeammate']");
    private final WebElement settingsButton = Selenide.$x("//li[@data-qa='menu-account']");
    private final WebElement logoutButton = Selenide.$x("//button[@data-qa='menu-logout']");
    private final WebElement quiteConfirmButton =
            Selenide.$x("//button[@data-qa='logout-button-confirm']");
    private final WebElement languageButton = Selenide.$x("//div[@data-qa='menu-languageSelect']");
    private final ElementsCollection languagesList = Selenide.$$x("//ul[@role='listbox']//li");
    private final String languagesElement = "//ul[@role='listbox']//li[contains(text(),'%s')]";

    public boolean isLanguageButtonDisplayed() {
        ((JavascriptExecutor) WebDriverRunner.getWebDriver())
                .executeScript("arguments[0].scrollIntoView(true);", leftMenu);
        return languageButton.isDisplayed();
    }

    public RollUpMenuPage clickOnLanguageButton() {
        WaitUtils.waitElementToBeClickable(languageButton).click();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    public RollUpMenuPage selectLanguage(Language language) {
        Selenide.$x(String.format(languagesElement, language.getLanguage())).click();
        WaitUtils.sleepSeconds(1);
        return this;
    }

    public void clickLogoutButton() {
        logoutButton.click();
    }

    public void clickConfirmPopUpButton() {
        quiteConfirmButton.click();
    }

    public List<String> getLanguagesList() {
        return languagesList.stream().map(SelenideElement::getText).collect(Collectors.toList());
    }

    public String getRollUpMenuElementText() {
        return rollUpMenuButton.getText();
    }

    public String getAnalyticsElementText() {
        return analyticsButton.getText();
    }

    public String getVerificationListElementText() {
        return verificationListButton.getText();
    }

    public String getVerificationFlowsElementText() {
        return verificationFlowButton.getText();
    }

    public String getForDevelopersElementText() {
        return forDevelopersButton.getText();
    }

    public String getFAQElementText() {
        return faqButton.getText();
    }

    public String getWhatsNewElementText() {
        return whatsNewButton.getText();
    }

    public String getInviteTeammateElementText() {
        return inviteTeammateButton.getText();
    }

    public String getSettingsElementText() {
        return settingsButton.getText();
    }

    public String getLogoutElementText() {
        return logoutButton.getText();
    }

    public void selectAnalyticsMenu() {
        analyticsButton.click();
    }

    public void selectVerificationListMenu() {
        verificationListButton.click();
    }

    public void selectVerificationFlowsMenu() {
        WaitUtils.waitElementToBeClickable(verificationFlowButton).click();
    }

    public void clickOnInviteTeamButton() {
        inviteTeammateButton.click();
    }
}
