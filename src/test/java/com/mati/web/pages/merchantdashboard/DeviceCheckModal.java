package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.WebElement;

public class DeviceCheckModal {

    private WebElement deviceType = Selenide.$x("//p[text()='Device Type']/parent::div/parent::div/following-sibling::div/h6");
    private WebElement os = Selenide.$x("//p[text()='OS']/parent::div/parent::div/following-sibling::div/h6");
    private WebElement browser = Selenide.$x("//p[text()='Browser']/parent::div/parent::div/following-sibling::div/h6");

    public boolean isDeviceTypeDisplayed() {
        return deviceType.isDisplayed();
    }

    public boolean isOsDisplayed() {
        return os.isDisplayed();
    }

    public boolean isBrowserDisplayed() {
        return browser.isDisplayed();
    }
}
