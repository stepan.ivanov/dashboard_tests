package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.mati.enums.Period;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

public class FilterModalWindow {

    WebElement viewResultsButton = Selenide.$x("//button[@data-qa='filter-button-apply']");

    public void setPeriod(Period period) {
        Selenide.$x(String.format("//span[text()='%s']", period.getPeriod())).parent().click();
    }

    public void clickViewResultsButton() {
        viewResultsButton.click();
    }

    public void waitForPageLoaded() {
        WaitUtils.waitElementToBeVisible(viewResultsButton);
    }
}
