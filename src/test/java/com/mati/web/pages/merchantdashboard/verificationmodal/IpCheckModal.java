package com.mati.web.pages.merchantdashboard.verificationmodal;

import com.codeborne.selenide.Selenide;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

public class IpCheckModal {

    private WebElement imgMap = Selenide.$x("//img[contains(@src,'maps')]");
    private WebElement country = Selenide.$x("//p[text()='Country']/preceding-sibling::h6");
    private WebElement province = Selenide.$x("//p[text()='Province']/preceding-sibling::h6");
    private WebElement city = Selenide.$x("//p[text()='City']/preceding-sibling::h6");
    private WebElement zipCode = Selenide.$x("//p[text()='Zip Code']/preceding-sibling::h6");
    private WebElement vpn = Selenide.$x("//h4[contains(text(),'VPN')]");
    private WebElement geoRestrictions = Selenide.$x("//h4[text()='Geo restrictions']");

    public boolean isImgMapDisplayed() {
        return imgMap.isDisplayed();
    }

    public boolean isCountryDisplayed() {
        return country.isDisplayed();
    }

    public boolean isProvinceDisplayed() {
        return province.isDisplayed();
    }

    public boolean isCityDisplayed() {
        return city.isDisplayed();
    }

    public boolean isZipCodeDisplayed() {
        return zipCode.isDisplayed();
    }

    public boolean isVpnDisplayed() {
        return vpn.isDisplayed();
    }

    public boolean isGeoRestrictionsDisplayed() {
        return geoRestrictions.isDisplayed();
    }

    public void waitForPageLoad(){
        WaitUtils.waitElementToBeVisible(imgMap);
    }
}
