package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.mati.enums.Role;
import com.mati.utils.DateUtils;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class AgentHistoryPage {

    private final WebElement name = Selenide.$x("//div[text()='Name']/preceding-sibling::div");
    private final WebElement email = Selenide.$x("//div[text()='Email']/preceding-sibling::div");
    private final WebElement role = Selenide.$x("//div[text()='Role']/preceding-sibling::div");
    private final WebElement dateOfRegistration = Selenide.$x("//div[text()='Date of registration']/preceding-sibling::div");
    private final WebElement deleteAgentButton = Selenide.$x("//button[@data-qa='collaborators-button-delete-member']");
    private final WebElement alertUserWasDeleted = Selenide.$x("//div[@role='alert' and text()='Your teammate was deleted']");
    private final WebElement confirmDeletingButton = Selenide.$x("//button[@data-qa='modal-button-confirm-delete']");

    public boolean isNameDisplayed() {
        return name.isDisplayed();
    }

    public boolean isEmailDisplayed() {
        return email.isDisplayed();
    }

    public boolean isRoleDisplayed() {
        return role.isDisplayed();
    }

    public boolean isDateOfRegistration() {
        return dateOfRegistration.isDisplayed();
    }

    public void clickOnDeleteAgentButton() {
        deleteAgentButton.click();
    }

    public void waitAlertUserIsDeleted() {
        WaitUtils.waitElementToBeVisible(alertUserWasDeleted);
    }

    public void waitUntilPageLoading() {
        WaitUtils.waitElementToBeVisible(deleteAgentButton);
    }

    public void confirmDeleting() {
        confirmDeletingButton.click();
    }

    public String getEmail(){
        return email.getText();
    }

    public String getName(){
        return name.getText();
    }

    public Role getRole(){
        return Role.valueOf(role.getText().toUpperCase());
    }

    public LocalDateTime getDateOfRegistration() {
        return DateUtils.parseDateOfRegistration(dateOfRegistration.getText());
    }
}
