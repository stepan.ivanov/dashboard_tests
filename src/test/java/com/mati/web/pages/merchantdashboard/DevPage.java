package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.open;

public class DevPage {

    private String url = EnvironmentConfig.url + "/dev";

    private WebElement webHookButton = Selenide.$x("//button[@data-qa='webhook-button-configure']");

    public DevPage navigateToPage() {
        open(url);
        WaitUtils.waitElementToBeVisible(webHookButton);
        return this;
    }

    public void clickOnWebHookButton() {
        webHookButton.click();
    }

}
