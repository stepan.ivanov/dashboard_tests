package com.mati.web.pages.merchantdashboard.verificationmodal;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class DocumentVerificationModal extends Modal {

    private ElementsCollection documentImages = Selenide.$$x("//img[@alt]");
    private WebElement inputDocumentNumber = Selenide.$x("//input[@name='documentNumber']");
    private WebElement inputDocumentType = Selenide.$x("//input[@name='documentType']");
    private WebElement documentNumber = Selenide.$x("//label[text()='Document Number']/preceding-sibling::p");
    private WebElement documentType = Selenide.$x("//label[text()='Document Type']/preceding-sibling::p");
    private SelenideElement inputDayOfExpirationDate = Selenide.$x("(//p[text()='Expiration Date']//parent::div//input)[1]");
    private SelenideElement inputMonthOfExpirationDate = Selenide.$x("(//p[text()='Expiration Date']//parent::div//input)[2]");
    private SelenideElement inputYearOfExpirationDate = Selenide.$x("(//p[text()='Expiration Date']//parent::div//input)[3]");
    private ElementsCollection dayOfExpiration = Selenide.$$x("//label[text()='Expiration Date']//parent::div//input");
    private WebElement inputFirstName = Selenide.$x("//input[@name='firstName']");
    private WebElement inputName = Selenide.$x("//input[@name='fullName']");
    private WebElement inputLastName = Selenide.$x("//input[@name='lastName']");
    private WebElement documentChangeButtonSave = Selenide.$x("//button[@data-qa='document-change-button-save']");
    private WebElement firstName = Selenide.$x("//label[text()='First Name']/preceding-sibling::p");
    private WebElement fullName = Selenide.$x("//label[text()='Name']/preceding-sibling::p");
    private WebElement lastName = Selenide.$x("//label[text()='Last Name']/preceding-sibling::p");
    private ElementsCollection dateOfBirth = Selenide.$$x("//label[text()='Date of Birth']//parent::div//input");
    private SelenideElement inputDayOfBirth = Selenide.$x("(//p[text()='Date of Birth']//parent::div//input)[1]");
    private SelenideElement inputMonthOfBirth = Selenide.$x("(//p[text()='Date of Birth']//parent::div//input)[2]");
    private WebElement editDataButton = Selenide.$x("//button[@data-qa = 'document-change-button-editData']");
    private WebElement saveAndSendWebhookButton = Selenide.$x("//button[@data-qa = 'document-change-button-save']");
    private SelenideElement inputYearOfBirth = Selenide.$x("(//p[text()='Date of Birth']//parent::div//input)[3]");
    private WebElement verificationCircle = Selenide.$x("//h4[text()='Checking...']");
    private WebElement zoomedImage = Selenide.$x("//img[@class='zoomedImage']");
    private WebElement imageInNewTab = Selenide.$x("//img");
    private int currentTimeOfWaiting = 0;


    public void clickEditDataButton() {
        editDataButton.click();
    }

    public void clickSaveAndSendWebhookButton() {
        saveAndSendWebhookButton.click();
    }

    public void scrollToEditDataButton() {
        scrollToWebElement(editDataButton);
    }

    public void fillLastName(String lastName) {
        fillField(inputLastName, lastName);
    }

    public void fillFullName(String fullName) {
        fillField(inputName, fullName);
    }

    public void fillFirstName(String firstName) {
        fillField(inputFirstName, firstName);
    }

    public void saveEdits() {
        documentChangeButtonSave.click();
    }

    public String getFirstName() {
        return firstName.getText();
    }

    public String getFullName() {
        return fullName.getText();
    }

    public String getSurName() {
        return lastName.getText();
    }

    public String getDateOfBirth() {
        StringBuilder date = new StringBuilder();
        dateOfBirth.forEach(it -> {
            date.append(it.getValue());
            date.append(" ");
        });
        return date.toString();
    }

    public void fillDayOfBirth(String day) {
        fillField(inputDayOfBirth, day);
    }

    public void fillMonthOfBirth(String month) {
        fillField(inputMonthOfBirth, month);
    }

    public void fillYearOfBirth(String year) {
        fillField(inputYearOfBirth, year);
    }

    public void fillInputDocumentNumber(String documentNumber) {
        fillField(inputDocumentNumber, documentNumber);
    }

    public void fillInputDocumentType(String documentType) {
        fillField(inputDocumentType, documentType);
    }

    public String getDocumentNumber() {
        return documentNumber.getText();
    }

    public String getDocumentType() {
        return documentType.getText();
    }

    public void fillDayOfExpirationDate(String day) {
        fillField(inputDayOfExpirationDate, day);
    }

    public void fillMonthOfExpirationDate(String month) {
        fillField(inputMonthOfExpirationDate, month);
    }

    public void fillYearOfExpirationDate(String year) {
        fillField(inputYearOfExpirationDate, year);
    }

    public String getDateOfExpiration() {
        StringBuilder date = new StringBuilder();
        dayOfExpiration.forEach(it -> {
            date.append(it.getValue());
            date.append(" ");
        });
        return date.toString();
    }

    public boolean isDocumentImageDisplayed() {
        WaitUtils.waitElementsToBeVisibility(documentImages);
        return true;
    }

    public void waitUntilVerificationFinished() {
        WaitUtils.sleepSeconds(10);
        int maxTimeToWait = 180;
        if (verificationCircle.isDisplayed() && currentTimeOfWaiting < maxTimeToWait) {
            refreshPage();
            currentTimeOfWaiting += 10;
            waitUntilVerificationFinished();
        } else if (maxTimeToWait == currentTimeOfWaiting) {
            Assert.fail("Verification in progress more than 3 minutes");
        }
    }

    public void clickOnFrontDocumentImage() {
        WaitUtils.waitElementToBeClickable(documentImages.first().parent()).click();
        WaitUtils.waitElementToBeVisible(zoomedImage);
    }

    public void clickOnBackDocumentImage() {
        WaitUtils.waitElementToBeClickable(documentImages.get(1).parent()).click();
        WaitUtils.waitElementToBeVisible(zoomedImage);
    }

    public void openZoomedImageInNewTab() {
        WebDriverRunner.getWebDriver().navigate().to(zoomedImage.getAttribute("src"));
    }

    public boolean isImageInNewTabDisplayed(){
        return imageInNewTab.isDisplayed();
    }

}
