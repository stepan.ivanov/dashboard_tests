package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.EnvironmentConfig;
import com.mati.web.pages.AbstractPage;

public class ReviewModePage extends AbstractPage {

    private final String URL = EnvironmentConfig.url + "/review";

    public boolean isReviewModePage() {
        return WebDriverRunner.getWebDriver().getCurrentUrl().equals(URL);
    }
}
