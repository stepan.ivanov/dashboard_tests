package com.mati.web.pages.merchantdashboard.verificationmodal;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.WebElement;

public class PhoneCheckModal extends Modal {

    private String phoneValidationElements = "//div[@id='panel-phone-ownership-validation-content']//div[text()='%s']/preceding-sibling::div";
    private WebElement countryCodeInValidationsWindow = Selenide.$x(String.format(phoneValidationElements, "Country code"));
    private WebElement status = Selenide.$x(String.format(phoneValidationElements, "Status"));
    private WebElement cellPhoneNumberInValidationsWindow = Selenide.$x(String.format(phoneValidationElements, "Cellphone Number"));
    private String phoneRiskElements = "//div[@id='panel-phone-risk-analysis-validation-content']//div[text()='%s']/preceding-sibling::div";
    private WebElement countryCodeInRiskWindow = Selenide.$x(String.format(phoneRiskElements, "Country code"));
    private WebElement cellPhoneNumberInRiskWindow = Selenide.$x(String.format(phoneRiskElements, "Cellphone Number"));
    private WebElement riskLevel = Selenide.$x(String.format(phoneRiskElements, "Risk Level"));
    private WebElement riskScore = Selenide.$x(String.format(phoneRiskElements, "Risk Score"));
    private WebElement timeZone = Selenide.$x(String.format(phoneRiskElements, "Timezone"));
    private WebElement lineType = Selenide.$x(String.format(phoneRiskElements, "Line Type"));
    private WebElement carrier = Selenide.$x(String.format(phoneRiskElements, "Carrier"));

    public boolean isCountryCodeInValidationsWindowDisplayed() {
        return countryCodeInValidationsWindow.isDisplayed();
    }

    public boolean isStatusDisplayed() {
        return status.isDisplayed();
    }

    public boolean isCellphoneNumberInValidationsWindowDisplayed() {
        return cellPhoneNumberInValidationsWindow.isDisplayed();
    }

    public boolean isCountryCodeInRiskWindowDisplayed() {
        return countryCodeInRiskWindow.isDisplayed();
    }

    public boolean isCellphoneNumberInRiskWindowDisplayed() {
        return cellPhoneNumberInRiskWindow.isDisplayed();
    }

    public boolean isRiskLevelDisplayed() {
        return riskLevel.isDisplayed();
    }

    public boolean isRiskScoreDisplayed() {
        return riskScore.isDisplayed();
    }

    public boolean isTimeZoneDisplayed() {
        return timeZone.isDisplayed();
    }

    public boolean isLineTypeDisplayed() {
        return lineType.isDisplayed();
    }

    public boolean isCarrierDisplayed() {
        return carrier.isDisplayed();
    }
}
