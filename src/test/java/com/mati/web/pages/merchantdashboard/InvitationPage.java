package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.mati.enums.Role;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static com.mati.enums.Role.ADMIN;
import static com.mati.enums.Role.AGENT;

public class InvitationPage {

    private WebElement invitationScreen = Selenide.$x("//div[@data-role='modal']");
    private WebElement firstNameInput = Selenide.$x("//input[@name='firstName']");
    private WebElement lastNameInput = Selenide.$x("//input[@name='lastName']");
    private WebElement emailInput = Selenide.$x("//input[@name='email']");
    private String errorMessage = "//div/p[text()='%s']";
    private WebElement buttonSend = Selenide.$x("//button[@data-qa='collaborators-button-send']");
    private WebElement buttonCancel = Selenide.$x("//button//span[text()='Cancel']/parent::*");
    private String roleRadioButton = "//input[@type='radio' and @value='%s']";

    public InvitationPage fillEmailInput(String email) {
        emailInput.sendKeys(Keys.CONTROL + "A");
        emailInput.sendKeys(Keys.BACK_SPACE);
        emailInput.sendKeys(email);
        return this;
    }

    public InvitationPage fillFirstNameInput(String email) {
        firstNameInput.sendKeys(Keys.CONTROL + "A");
        firstNameInput.sendKeys(Keys.BACK_SPACE);
        firstNameInput.sendKeys(email);
        return this;
    }

    public InvitationPage fillLastNameInput(String email) {
        lastNameInput.sendKeys(Keys.CONTROL + "A");
        lastNameInput.sendKeys(Keys.BACK_SPACE);
        lastNameInput.sendKeys(email);
        return this;
    }

    public void clickOnButtonSend() {
        buttonSend.click();
    }

    public boolean isErrorMessageDisplayed(String message) {
        return Selenide.$x(String.format(errorMessage, message)).isDisplayed();
    }

    public boolean isFirstNameFieldDisplayed() {
        return firstNameInput.isDisplayed();
    }

    public boolean isLastNameFieldDisplayed() {
        return lastNameInput.isDisplayed();
    }

    public boolean isEmailFieldDisplayed() {
        return emailInput.isDisplayed();
    }

    public boolean isSendButtonDisplayed() {
        return buttonSend.isDisplayed();
    }

    public boolean isCancelButtonDisplayed() {
        return buttonCancel.isDisplayed();
    }

    public boolean isAdminRoleRadioButtonDisplayed() {
        return Selenide.$x(String.format(roleRadioButton, ADMIN.getValue())).parent().isDisplayed();
    }

    public boolean isAgentRoleRadioButtonDisplayed() {
        return Selenide.$x(String.format(roleRadioButton, AGENT.getValue())).parent().isDisplayed();
    }

    public boolean isInvitationScreenDisplayed() {
        return invitationScreen.isDisplayed();
    }

    public InvitationPage setRole(Role role) {
        Selenide.$x(String.format(roleRadioButton, role.getValue())).click();
        return this;
    }

    public boolean isRoleChecked(Role role) {
        Selenide.$x(String.format(roleRadioButton, role.getValue())).isSelected();
        return true;
    }
}
