package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.mati.utils.WaitUtils;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class WebHooksMenu extends AbstractPage {

    private WebElement urlInput = Selenide.$x("//input[@data-qa='webhook-input-url']");
    private WebElement alertMessage = Selenide.$x("//div[@role = 'alert' and text()='Webhook was changed']");
    private WebElement secretInput = Selenide.$x("//input[@data-qa='webhook-input-secret']");
    private WebElement webHooksDocumentation = Selenide.$x("//button[@data-qa='webhook-button-documentation']");
    private WebElement toggleSecretVisibility = Selenide.$x("//button[@aria-label='toggle password visibility']");
    private WebElement saveButton = Selenide.$x("//button[@data-qa='webhook-button-save']");
    private WebElement closeButton = Selenide.$x("//button[@data-qa='modal-button-close']");

    public WebHooksMenu fillUrlInput(String url) {
        fillField(urlInput, url);
        return this;
    }

    public WebHooksMenu fillSecretInput(String secret) {
        fillField(secretInput, secret);
        return this;
    }

    public WebHooksMenu removeLastCharactersInSecretInput(int amount) {
        secretInput.sendKeys(Keys.END);
        for (int i = 0; i < amount; i++) {
            secretInput.sendKeys(Keys.BACK_SPACE);
        }
        return this;
    }

    public WebHooksMenu waitUntilPageIsOpening() {
        WaitUtils.waitElementToBeVisible(webHooksDocumentation);
        return this;
    }

    public String getSecret() {
        return secretInput.getAttribute("Value");
    }

    public WebHooksMenu clickToggleSecretVisibility() {
        toggleSecretVisibility.click();
        return this;
    }

    public void clickSaveButton() {
        scrollToWebElement(saveButton).click();
    }

    public void waitUntilChangesWillBeSaved() {
        WaitUtils.waitElementToBeVisible(alertMessage);
    }

    public void close() {
        closeButton.click();
        WaitUtils.waitElementToBeInvisibility(alertMessage);
    }

    public boolean isAlertMessageDisplayed() {
        WaitUtils.sleepSeconds(3);
        return alertMessage.isDisplayed();
    }
}
