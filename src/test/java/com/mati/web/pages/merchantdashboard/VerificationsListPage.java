package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.mati.enums.Period;
import com.codeborne.selenide.SelenideElement;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.DateUtils;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.open;

public class VerificationsListPage extends AbstractPage {

    private final String URL = EnvironmentConfig.url + "/identities";
    private WebElement reviewModeButton = Selenide.$x("//button[@data-qa = 'review-banner-button-goToReviewMode']");
    private String verificationObject = "//table[@data-qa = 'verificationList-table']//h6[text() = '%s']";
    private ElementsCollection verificationObjectList =
            Selenide.$$x("//table[@data-qa = 'verificationList-table']//div[contains(@style, 'position: absolute')]");
    private String bucketButton = "//table[@data-qa = 'verificationList-table']//h6[text() = '%s']/../../following-sibling::div//button[@tabindex = '-1']";
    private WebElement deleteButton = Selenide.$x("//button[contains(@class, 'btn-delete')]");
    private WebElement downloadCSVButton = Selenide.$x("//button[@data-qa = 'verificationList-button-downloadCsv']");
    private ElementsCollection verificationCreationDates = Selenide.$$x("//div[text() = 'Date']/parent::div");
    private ElementsCollection verificationStatuses = Selenide.$$x("//div[text() = 'Status']/preceding-sibling::span");
    private WebElement sortButton = Selenide.$x("//table/thead/tr/th[3]");
    private WebElement filterButton = Selenide.$x("//button[@data-qa = 'verificationList-button-openFilter']");
    private WebElement clearFilterButton = Selenide.$x("//button[@data-qa = 'filter-button-clear-all']");
    private String statusCheckBox = "//p[text() = '%s']/ancestor::label";
    private WebElement applyFilterButton = Selenide.$x("//button[@data-qa = 'filter-button-apply']");
    private ElementsCollection flowsCheckboxList = Selenide.$$x("//div[@data-qa = 'filter-block-by-flow']//span[contains(@class, 'MuiTypography-body1')]");
    private String flowCheckbox = "//span[text() = '%s']/ancestor::label";
    private ElementsCollection verificationFlows = Selenide.$$x("//div[text() = 'Flow']/preceding-sibling::div");
    private String period = "//div[@data-qa = 'filter-buttons-periods']//span[text() = '%s']/parent::button";
    private WebElement filterDates = Selenide.$x("//div[contains(@class, 'MuiGrid-wrap-xs-nowrap')]/parent::div");
    private WebElement alert = Selenide.$x("//button[contains(@class, 'Toastify__close-button--error')]");
    private WebElement filterByFlowLabel = Selenide.$x("//div[@data-qa = 'filter-block-by-flow']/parent::div/preceding-sibling::*//span");
    private WebElement filterByStatusLabel = Selenide.$x("//div[@data-qa = 'filter-block-by-status']/parent::div//span");
    private WebElement csvLoadingSpinner = Selenide.$x("//div[@class='Toastify']/div");

    public boolean isReviewModeButtonEnabled() {
        return reviewModeButton.isEnabled();
    }

    public boolean isReviewModeButtonBlack() {
        return reviewModeButton.getCssValue("background-color").equals(IConstants.reviewModeButtonColor);
    }

    public void clickReviewModeButton() {
        reviewModeButton.click();
    }

    public void selectVerificationByName(String name) {
        int sizeListVerification = verificationObjectList.size();
        WebElement verification = Selenide.$x(String.format(verificationObject, name));
        if (!verification.isDisplayed()) {
            scrollToWebElement(verificationObjectList.last());
            waitForPageLoad();
            if (sizeListVerification < verificationObjectList.size()) {
                selectVerificationByName(name);
            } else {
                Assert.fail("Can't find verification with name: " + name);
            }
        } else {
            verification.click();
        }
    }

    public void waitForPageLoad() {
        WaitUtils.waitElementToBeVisible(verificationObjectList.get(0));
//        WaitUtils.waitElementToBeClickable(verificationObjectList.get(0));
    }

    public void navigateToPage() {
        open(URL);
    }

    public void deleteVerificationByName(String name) {
        WaitUtils.sleepSeconds(5);
        Selenide.$x(String.format(bucketButton, name)).click();
        deleteButton.click();
    }

    public boolean isVerificationExist(String name) {
        return verificationObjectList.stream().anyMatch(s -> s.getText().split("\n")[0].equals(name));
    }

    public void downloadVerificationCSV() {
        downloadCSVButton.click();
        WaitUtils.waitElementToBeVisible(csvLoadingSpinner);
    }

    public void selectOldVerification() {
        verificationCreationDates.stream().filter(s -> DateUtils.parseVerificationCreationDate(s.getText()).isBefore(LocalDate.now().minusMonths(3))).findFirst().get().click();
    }

    public void selectFirstVerification() {
        verificationObjectList.get(0).click();
    }

    public void selectNewVerification() {
        verificationCreationDates.stream().filter(s -> DateUtils.parseVerificationCreationDate(s.getText()).isEqual(LocalDate.now())).findFirst().get().click();
    }

    public void selectVerificationByStatus(VerificationStatus status) {
        for (SelenideElement element : verificationStatuses) {
            scrollToWebElement(element);
            if (element.getText().equals(status)) element.click();
        }
//        verificationStatuses.stream().filter(s -> s.getText().equals(status.getStatus())).findFirst().get().click();
    }

    public String getURL() {
        return URL;
    }

    public void selectSortDescendingDate() {
        do {
            sortButton.click();
        } while (!sortButton.getAttribute("aria-sort").equals("descending"));
    }

    public void openFilter() {
        filterButton.click();
        WaitUtils.waitElementToBeVisible(filterByFlowLabel);
    }

    public List getVerificationStatuses() {
        return verificationStatuses.stream().map(s -> s.getText()).collect(Collectors.toList());
    }

    public void clearFilter() {
        clearFilterButton.click();
    }

    public void scrollToStatus(VerificationStatus status) {
        scrollToWebElement(Selenide.$x(String.format(statusCheckBox, status.getStatus())));
    }

    public void selectFilterByStatus(VerificationStatus status) {
        Selenide.$x(String.format(statusCheckBox, status.getStatus())).click();
    }

    public void clickApplyFilterButton() {
        applyFilterButton.click();
    }

    public void selectFilterByFlow(String flowName) {
        Selenide.$x(String.format(flowCheckbox, flowName)).click();
    }

    public void scrollToFlow(String flowName) {
        scrollToWebElement(Selenide.$x(String.format(flowCheckbox, flowName)));
    }

    public List getVerificationFlows() {
        return verificationFlows.stream().map(f -> f.getText()).collect(Collectors.toList());
    }

    public void selectFilterByPeriod(Period value) {
        Selenide.$x(String.format(this.period, value.getPeriod())).click();
    }

    public boolean isFilterByPeriodDisplayed(String period) {
        return Selenide.$x(String.format(this.period, period)).isDisplayed();
    }

    public List<LocalDate> getVerificationDates() {
        return verificationCreationDates.stream().map(s ->
                DateUtils.parseVerificationCreationDate(s.getText().split("\n")[0])).collect(Collectors.toList());
    }

    public List<LocalDate> getFilterDates() {
        return DateUtils.parseFilterDate(filterDates.getText());
    }

    public void closeAlert() {
        if (alert.isDisplayed()) alert.click();
    }

    public String getFilterByFlowLabel() {
        return filterByFlowLabel.getText();
    }

    public String getFilterByStatusLabel() {
        return filterByStatusLabel.getText();
    }

    public boolean isCsvLoadingSpinnerDisplayed() {
        return csvLoadingSpinner.isDisplayed();
    }

    public void waitUntilCsvLoadingSpinnerDisappear() {
        WaitUtils.waitElementToBeInvisibility(csvLoadingSpinner);
    }

    public boolean isDownloadCsvButtonEnabled() {
        return downloadCSVButton.isEnabled();
    }
}
