package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.RandomUtils;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.open;

public class SettingsPage {

    private final String url = EnvironmentConfig.url + "/settings";
    private final String table = "//tbody[@data-qa='collaborators-team-table']%s";
    private final ElementsCollection listOfPersonInTeam = Selenide.$$x(String.format(table, "//tr"));
    private final ElementsCollection listOfProfileButton = Selenide.$$x(String.format(table, "//tr//td[3]//button"));
    private final WebElement inviteButtonInTable = Selenide.$x("//div//span[text() = 'Invite a teammate']//parent::button[not(@data-qa='menu-inviteTeammate')]");

    public SettingsPage navigateToPage() {
        open(url);
        return this;
    }

    public SettingsPage waitUntilPageIsLoading() {
        WaitUtils.waitElementToBeVisible(inviteButtonInTable);
        return this;
    }

    public int getAmountOfPersonInTeam() {
        return listOfPersonInTeam.size();
    }

    public boolean isAllPersonHasProfileButton() {
        List<WebElement> personsWithProfileButton =
                listOfPersonInTeam.stream()
                        .map(SelenideElement::lastChild)
                        .map(SelenideElement::lastChild)
                        .filter(WebElement::isDisplayed)
                        .collect(Collectors.toList());
        return personsWithProfileButton.size() == getAmountOfPersonInTeam();
    }

    public void openRandomProfile() {
        listOfProfileButton.get(RandomUtils.randomInt(listOfProfileButton.size())).click();
    }

    public void openProfileByEmail(String email) {
        listOfPersonInTeam.stream()
                .filter(it -> it.getText()
                        .split("\\n")[2]
                        .equals(email.toLowerCase()))
                .findFirst()
                .get()
                .lastChild()
                .lastChild()
                .click();
    }
}
