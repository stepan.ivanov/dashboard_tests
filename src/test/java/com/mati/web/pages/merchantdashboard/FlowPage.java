package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.*;
import com.codeborne.selenide.ex.ElementNotFound;
import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.Region;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.FlowSettings;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.SmsUtils;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.open;

public class FlowPage extends AbstractPage {

    private final String modal = "//div[@data-role = 'modal']";

    // verification wizard
    private String regionButton =
            "//div[text() = '%s']/ancestor::div[@class = 'CountrySelectItem_item__cvvsI']";
    private WebElement shadowMainElement = Selenide.$x("//mati-button");
    private WebElement startVerificationButton = Selenide.$x("//button[@data-qa = 'StartSubmit']");
    private String countryButton =
            "//img[@alt = '%s']/ancestor::div[@class = 'CountrySelectItem_item__cvvsI']";
    private WebElement inputFrontPhoto = Selenide.$x("//input[@data-qa = 'UploadDropzone']");
    private WebElement inputBackPhoto = Selenide.$x("//input[@data-qa = 'UploadDropzone']");
    private WebElement readyFrontButton =
            Selenide.$x("//button[@data-qa = 'DocumentRotateSubmit-front']");
    private WebElement readyBackButton =
            Selenide.$x("//button[@data-qa = 'DocumentRotateSubmit-back']");
    private WebElement verificationWizardFrame = Selenide.$x("//mati-frame");
    private WebElement searchCountryField = Selenide.$x("//input[@data-qa = 'CountryInputSearch']");
    private WebElement finishWindow = Selenide.$x("//div[@data-qa='Finish']");
    private WebElement verificationWizardTitle = Selenide.$x("//div[@class = 'text-title']");
    private WebElement verificationWizard = Selenide.$x("//div[contains(@class, 'PopupWindow_root')][@data-qa]");

    // product list
    private WebElement productsContainer = Selenide.$x("//div[text() = 'Products']/parent::div");
    private WebElement productList =
            Selenide.$x("//div[text() = 'Products']/following-sibling::div");
    private String product = "//div[text() = '%s']/ancestor::div[@draggable]";
    private ElementsCollection productTitle =
            Selenide.$$x("//div[@draggable]//div[@aria-expanded]//div[text()][1]");
    private ElementsCollection userInputs =
            Selenide.$$x("//div[@draggable]//div[@aria-expanded]//div[text()][2]");
    private ElementsCollection sdkOnlyBadge =
            Selenide.$$x("//div[@draggable]//div[@aria-expanded]//div[text()][3]");
    private ElementsCollection productIcon =
            Selenide.$$x(
                    "//div[@draggable]//div[@aria-expanded]//div[contains(@class, 'MuiGrid')]/*[local-name() = 'svg']");
    private ElementsCollection products =
            Selenide.$$x("//div[text() = 'Products']/following-sibling::div/div[@draggable]");
    private ElementsCollection draggableElements = Selenide.$$x("//*[@draggable]");

    // flow builder
    private WebElement flowBuilder =
            Selenide.$x("//div[contains(text(), 'User’s flow')]/following-sibling::div");
    private WebElement dropZone = Selenide.$x("//div[@data-id = 'dropZone']/div");
    private ElementsCollection productApplicableChecks =
            Selenide.$$x(
                    "//div[text() = 'Products']/following-sibling::div/div[@draggable]//div[@role = 'button']");
    private ElementsCollection selectedProducts =
            Selenide.$$x("//div[@class = 'react-flow']//div[@data-id]");
    private String selectedProduct = "//div[@data-id = '%s']";
    private String basketButton = "//div[@data-id = '%s']//button";
    private WebElement deleteProductButton =
            Selenide.$x("//span[contains(text(), 'Remove')]/parent::button");

    private WebElement closeProductSettingButton =
            Selenide.$x("//div[text() = 'Product settings']/parent::div/button");
    private WebElement sdkIntegrationButton =
            Selenide.$x("//button[@data-qa = 'flow-builder-integration-api']/span[text() = 'SDK']");
    private WebElement apiIntegrationButton =
            Selenide.$x("//button[@data-qa = 'flow-builder-integration-api']/span[text() = 'API']");
    private WebElement saveAndPublishButton =
            Selenide.$x("//span[text() = 'Save and publish']/parent::button");
    private WebElement modalWindow = Selenide.$x(modal);
    private WebElement productSettingsTitle =
            Selenide.$x("//div[text() = 'Product settings']/parent::div");
    private WebElement alertMessage = Selenide.$x("//div[@role = 'alert' and text() = 'Changes have been saved']");
    private WebElement issuesMessage = Selenide.$x("//div[text() = 'You have issues']");
    private WebElement webHooksButton = Selenide.$x("//span[text()='Webhooks']//ancestor::button");

    // flow settings
    private WebElement flowSettingsButton =
            Selenide.$x("//button[contains(@class, 'MuiIconButton-colorPrimary')]");
    private WebElement flowNameInPopUp =
            Selenide.$x(modal + "//button/preceding-sibling::div");
    private WebElement flowIdInPopUp =
            Selenide.$x("//div[text() = 'Flow ID']/preceding-sibling::div");
    private WebElement creationDateInPopUp =
            Selenide.$x(modal + "//div[contains(text(), 'Created on')]");
    private WebElement flowChecksInPopUp =
            Selenide.$x(modal + "//div[contains(text(), 'Output')]");
    private WebElement checkBoxGDPR =
            Selenide.$x("//div[text() = 'GDPR']/parent::div//following-sibling::div//input");
    private WebElement policyDaysGDPRInput =
            Selenide.$x(modal + "//div//input[@type = 'text']");
    private WebElement checkBoxTimestamp =
            Selenide.$x(
                    "//div[text() = 'Certified Timestamp']/parent::div//following-sibling::div//input");
    private WebElement saveChangesButtonInPopUp =
            Selenide.$x(
                    modal + "//span[contains(text(), 'Save')]/parent::button");
    private WebElement unsavedChangesAlertPopup =
            Selenide.$x(
                    modal + "//span[contains(text(), 'Save')]/parent::button/preceding-sibling::div");
    private WebElement unsavedChangesAlert =
            Selenide.$x("//span[contains(text(), 'Save')]/parent::button/preceding-sibling::div");
    private WebElement confirmDeleteButton = Selenide.$x("//button[@data-role = 'confirm']");
    private WebElement deleteButton =
            Selenide.$x(
                    modal + "//button[.//*[contains(text(), 'Delete')]]");
    private WebElement confirmationPopUp = Selenide.$x("//div[@data-role = 'confirmationModal']");
    private WebElement closeModalWindowButton =
            Selenide.$x("//button[@data-qa= 'modal-button-close']");
    private WebElement settingCDPR =
            Selenide.$x(
                    "//div[text() = 'GDPR']/parent::div//following-sibling::div//span[@aria-disabled]");
    private WebElement cancelDeletingButton = Selenide.$x("//button[@data-role = 'cancel']");
    private WebElement iAgreeButton =
            Selenide.$x("//span[contains(text(), 'I agree to add this product')]/parent::button");
    private WebElement editFlowNameButton =
            Selenide.$x(modal + "//button/preceding-sibling::div/following-sibling::button");
    private WebElement flowNameInput = Selenide.$x("//input[@id = 'editable-field']");
    private WebElement markIconButton = Selenide.$x("//span[@role = 'button'][1]");

    // product settings
    private final String productSettingsPanel = "//div[text() = 'Product settings']/ancestor::div[3]";
    private String productCheck = productSettingsPanel + "//following::ul//div[contains(text(), '%s')]";
    private String productSetting = productSettingsPanel + "//following::*[text() = '%s']";
    private String settingCheckBox = "//*[text() = '%s']/..//following-sibling::*//input";
    private String settingCheckBoxes = "//div[text() = 'Product settings']/parent::div/following::div[text() = '%s']/parent::*/parent::*/parent::*//input";
    private String settingRadioButton = "//div[text() = '%s']/preceding::input[@type = 'radio'][1]";
    private String warningWithContent = "//div[contains(@class, 'undefined')]//div[contains(text(), '%s')]";
    private String dbRequestInput = "//div[@id = '%s-select']";
    private String governmentCheck = "//div[./div[text() = '%s']]/following::div[text() = '%s']";
    private String governmentCheckbox = governmentCheck + "/preceding::input[1]";
    private WebElement addStepButton = Selenide.$x("//button[./span[contains(text(), 'Add Step')]]");
    private String documentsCheckbox = "//label/span[.//input[@type = 'checkbox']][./following-sibling::span[text() = '%s']]";
    private WebElement addDocumentButton = Selenide.$x("//button[./span[text() = 'Add']]");
    private String documentInVerificationList = productSettingsPanel + "//div[text() = '%s']";
    private String editStepButton = "//div[text() = 'Step %d']/following-sibling::div//button[1]";
    private String deleteStepButton = "//div[text() = 'Step %d']/following-sibling::div//button[2]";
    private WebElement ageThresholdInput = Selenide.$x("//input[@placeholder][@type = 'number']");
    private WebElement errorAgeThreshold = Selenide.$x("//input[@placeholder][@type = 'number']/following::p");
    private WebElement countryRestrictionInput = Selenide.$x("//input[@id][./preceding::div[text() = 'Country restriction']]");
    private String countryRestrictionButton = "//div[text() = 'Country restriction']/following::button[text() = '%s']";
    private WebElement crossCountryRestrictionButton = Selenide.$x("//div[contains(@class, 'indicatorContainer')][1]");

    private WebElement flowName =
            Selenide.$x("//a[@href = '/flow']/following-sibling::div/div/div[1]");
    private WebElement flowCreationDate =
            Selenide.$x("//a[@href = '/flow']/following-sibling::div/div/div[1]");
    private WebElement backArrowButton =
            Selenide.$x("//div[contains(@class, 'MuiGrid')]/a[@href = '/flow']");
    private WebElement saveChangesAlert = Selenide.$x("//div[@role = 'alert'][text() = 'Changes have been saved']");

    //    phone verification
    private WebElement countryCode = Selenide.$x("//img");
    private WebElement inputPhoneNumber = Selenide.$x("//input[@type='number']");
    private WebElement inputCode = Selenide.$x("//input[@placeholder]");
    private WebElement buttonSendSms = Selenide.$x("//button[@data-qa='SendSms']");
    private String productIntoWarningPopup = modal + "//div[text() = 'Anti-Money Laundering']";
    private WebElement incorrectTypeDocumentWarning =
            Selenide.$x("//div[contains(text(), \"Products Gov Check and AML won't work with this type of document\")]");
    private WebElement skipSms = Selenide.$x("//button[@data-qa='SkipSms']");
    private WebElement errorVerificationPopup = Selenide.$x("//div[contains(@data-qa, 'Error')]");

    public String getPageUrl(String flowId) {
        return EnvironmentConfig.url + "/flow/" + flowId;
    }

    public void navigate(String id) {
        open(getPageUrl(id));
        waitForPageLoad();
    }

    public void waitForPageLoad() {
        WaitUtils.waitElementToBeVisible(shadowMainElement);
    }

    public void createVerification(VerificationInfoByCountry verificationInfoByCountry, String frontFileName, String backFileName) {
        String urlToWizard = getWizardUrl();
        navigateToVerificationWizard(urlToWizard);
        clickStartVerificationButton();
        selectCountry(verificationInfoByCountry);
        if (verificationInfoByCountry.equals(VerificationInfoByCountry.CANADA)) {
            selectRegion(Region.MANITOBA);
        }
        putFrontPhoto(frontFileName);
        putBackPhoto(backFileName);
    }

    public void createVerificationWithCountryRestriction(VerificationInfoByCountry verificationInfoByCountry, String frontFileName, String backFileName) {
        String urlToWizard = getWizardUrl();
        navigateToVerificationWizard(urlToWizard);
        clickStartVerificationButton();
        if (verificationInfoByCountry.equals(VerificationInfoByCountry.CANADA)) {
            selectRegion(Region.MANITOBA);
        }
        putFrontPhoto(frontFileName);
        putBackPhoto(backFileName);
    }

    public void createPhoneCheckVerification(VerificationInfoByCountry verificationInfoByCountry, String phoneNumber, boolean skip) {
        String urlToWizard = getWizardUrl();
        navigateToVerificationWizard(urlToWizard);
        clickStartVerificationButton();
        if (skip) {
            WaitUtils.waitElementToBeClickable(skipSms).click();
        } else {
            WaitUtils.waitElementToBeClickable(countryCode).click();
            selectCountry(verificationInfoByCountry);
            WaitUtils.waitElementToBeVisible(inputPhoneNumber).sendKeys(phoneNumber);
            buttonSendSms.click();
            String smsCode = SmsUtils.getSmsCode("Your verification code is: ");
            inputCode.sendKeys(smsCode);
        }
        WaitUtils.waitElementToBeVisible(finishWindow);
    }

    public void createVerification(VerificationInfoByCountry verificationInfoByCountry) {
        clickVerifyMeButton();
        clickStartVerificationButton();
        selectCountry(verificationInfoByCountry);
        if (verificationInfoByCountry.equals(VerificationInfoByCountry.CANADA)) {
            selectRegion(Region.MANITOBA);
        }
        putFrontPhoto(verificationInfoByCountry.getFrontSide());
        putBackPhoto(verificationInfoByCountry.getBackSide());
    }

    public void putBackPhoto(String backFileName) {
        inputBackPhoto.sendKeys(String.format(IConstants.filePath, backFileName));
        readyBackButton.click();
        waitSpinnerToBeInvisibility();
    }

    public void putFrontPhoto(String frontFileName) {
        inputFrontPhoto.sendKeys(String.format(IConstants.filePath, frontFileName));
        readyFrontButton.click();
        waitSpinnerToBeInvisibility();
    }

    public void selectRegion(Region region) {
        searchCountryField.sendKeys(region.getName());
        Selenide.$x(String.format(regionButton, region.getName())).click();
    }

    public void selectCountry(VerificationInfoByCountry verificationInfoByCountry) {
        WaitUtils.waitElementToBeVisible(searchCountryField).sendKeys(verificationInfoByCountry.getName());
        Selenide.$x(String.format(countryButton, verificationInfoByCountry.getName())).click();
        waitSpinnerToBeInvisibility();
    }

    public void clickStartVerificationButton() {
        if (!startVerificationButton.isDisplayed()) {
            WebElement shadowRootElement = getShadowRootElement(Selenide.$x("//mati-frame"));
            String newUrl = shadowRootElement.findElement(By.cssSelector("iframe")).getAttribute("src");
            WebDriverRunner.getWebDriver().navigate().to(newUrl);
            if (isAlertPresent()) {
                WebDriverRunner.getWebDriver().switchTo().alert().accept();
            }
            WaitUtils.waitElementToBeVisible(startVerificationButton);
        }
        startVerificationButton.click();
        waitSpinnerToBeInvisibility();
    }

    public void navigateToVerificationWizard(String url) {
        WebDriverRunner.getWebDriver().navigate().to(url);
        waitSpinnerToBeInvisibility();
    }

    public String getWizardUrl() {
        String signupHost = shadowMainElement.getAttribute("signuphost");
        String flowId = shadowMainElement.getAttribute("flowid");
        String clientId = shadowMainElement.getAttribute("clientid");
        return signupHost + "/?merchantToken=" + clientId + "&flowId=" + flowId;
    }

    public void clickVerifyMeButton() {
        shadowMainElement.click();
    }

    public boolean isVerificationWizardDisplayed() {
        return verificationWizard.isDisplayed();
    }

    public WebElement getVerificationWizard() {
        JavascriptExecutor jse = ((JavascriptExecutor) WebDriverRunner.getWebDriver());
        return (WebElement)
                jse.executeScript(
                        "return arguments[0].shadowRoot.firstElementChild",
                        verificationWizardFrame);
    }

    public boolean isNotProductsResizable() {
        return productsContainer.getCssValue("display").equals("block");
    }

    public boolean isProductListScrollable() {
        return productList.getCssValue("overflow-y").equals("auto");
    }

    public void moveProductToFlowBuilder(Products value) {
        WebElement source = Selenide.$x(String.format(product, value.getProduct()));
        dragAndDrop(source, dropZone);
    }

    public boolean isProductInFlowBuilder(Products value) {
        return Selenide.$x(String.format(selectedProduct, value.getDataID())).isDisplayed();
    }

    public void clickSaveAndPublishButton() {
        saveAndPublishButton.click();
    }

    public boolean isSaveAndPublishButtonEnabled() {
        return saveAndPublishButton.isEnabled();
    }

    public boolean isFlowBuilderScrollable() {
        return flowBuilder.getCssValue("overflow-y").equals("auto");
    }

    public void selectApiIntegration() {
        if (closeProductSettingButton.isDisplayed()) closeProductSettings();
        apiIntegrationButton.click();
    }

    public void selectSdkIntegration() {
        if (closeProductSettingButton.isDisplayed()) closeProductSettings();
        sdkIntegrationButton.click();
    }

    public void closeProductSettings() {
        closeProductSettingButton.click();
    }

    public boolean isDraggableOnlyProducts() {
        return products.size() == draggableElements.size();
    }

    public boolean isProductsAlignedVertically() {
        return products.stream().allMatch(p -> p.getCssValue("display").equals("block"));
    }

    public boolean isProductsExpandable() {
        return productApplicableChecks.stream()
                .allMatch(p -> isAttributePresent(p, "aria-expanded"));
    }

    public void moveAllProductsToFlowBuilder() {
        products.forEach(p -> dragAndDrop(p, dropZone));
    }

    public boolean isProductInRightPosition(Products product, int position) {
        return selectedProducts.get(position - 1).getText().contains(product.getProduct());
    }

    public void deleteProductsFromFlowBuilder() {
        for (Products p : Products.values()) {
            WebElement element = Selenide.$x(String.format(selectedProduct, p.getDataID()));
            if (element.isDisplayed()) {
                deleteProductFromFlowBuilder(p);
            }
        }
    }

    public void clickBasketButtonIntoProduct(Products prod) {
        hoverMouseToElement(Selenide.$x(String.format(selectedProduct, prod.getDataID())));
        Selenide.$x(String.format(basketButton, prod.getDataID())).click();
    }

    public void deleteProductFromFlowBuilder(Products prod) {
        clickBasketButtonIntoProduct(prod);
        if (deleteProductButton.isDisplayed()) deleteProductButton.click();
        WaitUtils.waitElementToBeVisible(Selenide.$x(String.format(product, prod.getProduct())));
    }

    public boolean isProductInProductsList(Products value) {
        return Selenide.$x(String.format(product, value.getProduct())).isDisplayed();
    }

    public boolean isProductTitleDisplayed() {
        return productTitle.stream().allMatch(t -> t.isDisplayed());
    }

    public boolean isUserInputsDisplayed() {
        return userInputs.stream().allMatch(t -> t.isDisplayed());
    }

    public boolean isProductIconDisplayed() {
        return productIcon.stream().allMatch(t -> t.isDisplayed());
    }

    public boolean isSDKOnlyBadgeDisplayed() {
        return sdkOnlyBadge.stream().anyMatch(t -> t.isDisplayed());
    }

    public boolean isModalWindowDisplayed() {
        return modalWindow.isDisplayed();
    }

    public boolean isProductSettingsDisplayed() {
        return productSettingsTitle.isDisplayed();
    }

    public boolean isDropZoneAtBottom() {
        int lastElement = selectedProducts.size() - 1;
        return selectedProducts.get(lastElement).getAttribute("data-id").equals("dropZone");
    }

    public void clickFlowSettingsButton() {
        flowSettingsButton.click();
    }

    public boolean isFlowNamePopUpDisplayed() {
        return flowNameInPopUp.isDisplayed();
    }

    public boolean isFlowIdPopUpDisplayed() {
        return flowIdInPopUp.isDisplayed();
    }

    public boolean isDateCreationFlowPopUpDisplayed() {
        return creationDateInPopUp.isDisplayed();
    }

    public boolean isFlowChecksPopUpDisplayed() {
        return flowChecksInPopUp.isDisplayed();
    }

    public boolean isCDPRCheckBoxSwitchable() {
        return checkBoxGDPR.getAttribute("type").equals("checkbox");
    }

    public void selectGDPRCheckBox(boolean flag) {
        selectCheckbox(checkBoxGDPR, flag);
    }

    public boolean isPoliceDaysEnabled() {
        return policyDaysGDPRInput.getAttribute("type").equals("text");
    }

    public void fillPolicyDays(String value) {
        policyDaysGDPRInput.sendKeys(value);
    }

    public boolean isTimestampCheckBoxSwitchable() {
        return checkBoxTimestamp.getAttribute("type").equals("checkbox");
    }

    public boolean isUnsavedChangesAlertPopupDisplayed() {
        return unsavedChangesAlertPopup.isDisplayed();
    }

    public boolean isUnsavedChangesAlertDisplayed() {
        return unsavedChangesAlert.isDisplayed();
    }

    public void closeModalWindow() {
        closeModalWindowButton.click();
    }

    public boolean isCDPRCheckBoxSelected() {
        return checkBoxGDPR.isSelected();
    }

    public boolean isTimestampCheckBoxSelected() {
        return checkBoxTimestamp.isSelected();
    }

    public void selectTimestampCheckBox(boolean flag) {
        selectCheckbox(checkBoxTimestamp, flag);
    }

    public boolean isPolicyDaysFilled() {
        return !policyDaysGDPRInput.getAttribute("value").isEmpty();
    }

    public boolean isConfirmationPopupDisplayed() {
        return confirmationPopUp.isDisplayed();
    }

    public void clickDeleteFlowButton() {
        deleteButton.click();
    }

    public void clickSaveChangesButton() {
        saveChangesButtonInPopUp.click();
        WaitUtils.waitElementToBeInvisibility(modalWindow);
    }

    public void clickCancelDeletingButton() {
        cancelDeletingButton.click();
    }

    public boolean isFlowSettingsPopupDisplayed() {
        return modalWindow.isDisplayed();
    }

    public void clickConfirmDeleteButton() {
        confirmDeleteButton.click();
    }

    public boolean isAlertMessageDisplayed() {
        return alertMessage.isDisplayed();
    }

    public void waitUntilChangesWillBeSaved() {
        WaitUtils.waitElementToBeVisible(alertMessage);
    }

    public boolean isIssuesMessageDisplayed() {
        return issuesMessage.isDisplayed();
    }

    public boolean isProductCheckDisplayed(String check) {
        return Selenide.$x(String.format(productCheck, check)).isDisplayed();
    }

    public boolean isProductSettingDisplayed(FlowSettings setting) {
        return Selenide.$x(String.format(productSetting, setting.getValue())).isDisplayed();
    }

    public boolean isProductSettingOpened(Products setting) {
        return Selenide.$x(String.format(productSetting, setting.getProduct())).isDisplayed();
    }

    public boolean isSettingCheckBoxSwitchable(FlowSettings setting) {
        return Selenide.$x(String.format(settingCheckBox, setting.getValue()))
                .getAttribute("type")
                .equals("checkbox");
    }

    public void clickProductInFlowBuilder(Products product) {
        Selenide.$x(String.format(selectedProduct, product.getDataID())).click();
    }

    public boolean isCheckBoxesSetDefault(FlowSettings setting) {
        ElementsCollection checkBoxes = Selenide.$$x(String.format(settingCheckBoxes, setting.getValue()));
        return checkBoxes.stream().filter(SelenideElement::isSelected).findFirst().orElse(null) == null;
    }

    public void confirmAddingProductToFlowBuilder() {
        iAgreeButton.click();
    }

    public boolean isFlowNameDisplayed() {
        return flowName.isDisplayed();
    }

    public boolean isFlowCreationDateDisplayed() {
        return flowCreationDate.isDisplayed();
    }

    public boolean isFlowNameNotEditable() {
        return !flowName.getTagName().equals("input")
                && !isAttributePresent(flowName, "contenteditable");
    }

    public void clickBackArrowButton() {
        backArrowButton.click();
    }

    public void closeDependencyWizard() {
        closeModalWindowButton.click();
    }

    public boolean isProductSettingSelective(FlowSettings setting) {
        return Selenide.$x(String.format(settingRadioButton, setting.getValue())).getAttribute("type").equals("radio");
    }

    public void clickEditFlowNameButton() {
        editFlowNameButton.click();
    }

    public void fillFlowName(String value) {
        fillField(flowNameInput, value);
    }

    public void clickMarkIconButton() {
        markIconButton.click();
    }

    public boolean isSavedChangesAlertDisplayed() {
        WaitUtils.sleepSeconds(3);
        return saveChangesAlert.isDisplayed();
    }

    public boolean isProductIntoWarningPopup(Products product) {
        return Selenide.$x(String.format(productIntoWarningPopup, product.getProduct())).isDisplayed();
    }

    public void clickAddStepButton() {
        addStepButton.click();
    }

    public void selectDocumentsCheckbox(String document, boolean flag) {
        WebElement documentCheckbox = Selenide.$x(String.format(documentsCheckbox, document));
        if (documentCheckbox.getAttribute("class").contains("Mui-checked") != flag) documentCheckbox.click();
    }

    public void clickAddDocumentButton() {
        addDocumentButton.click();
    }

    public boolean isDocumentDisplayed(String document) {
        boolean result;
        try {
            result = Selenide.$x(String.format(documentInVerificationList, document)).scrollTo().isDisplayed();
        } catch (ElementNotFound elementNotFound) {
            return false;
        }
        return result;
    }

    public boolean isWarningWithContentDisplayed(String text) {
        return Selenide.$x(String.format(warningWithContent, text)).isDisplayed();
    }

    public boolean isDBRequestInputDisplayed(String unit) {
        return Selenide.$x(String.format(dbRequestInput, unit)).isDisplayed();
    }

    public boolean isGovernmentCheckDisplayed(String country, String check) {
        return Selenide.$x(String.format(governmentCheck, country, check)).isDisplayed();
    }

    public boolean isGovernmentCheckboxSwitchable(String country, String check) {
        return Selenide.$x(String.format(governmentCheckbox, country, check)).getAttribute("type").equals("checkbox");
    }

    public void clickWebHooksButton() {
        WaitUtils.waitElementToBeClickable(webHooksButton).click();
    }

    public boolean isCheckBoxSelected(FlowSettings setting) {
        return Selenide.$x(String.format(settingCheckBox, setting.getValue())).isSelected();
    }

    public void selectSettingCheckbox(FlowSettings setting, boolean flag) {
        WebElement checkbox = Selenide.$x(String.format(settingCheckBox, setting.getValue()));
        selectCheckbox(checkbox, flag);
    }

    public void selectSettingCheckboxes(FlowSettings setting, boolean flag) {
        Selenide.$$x(String.format(settingCheckBox, setting.getValue()))
                .forEach(checkbox -> selectCheckbox(checkbox, flag));
    }

    public void deleteDocumentStep(int stepNumber) {
        Selenide.$x(String.format(deleteStepButton, stepNumber)).click();
    }

    public void fillAgeThresholdInput(String age) {
        fillField(ageThresholdInput, age);
    }

    public boolean isErrorAgeThresholdDisplayed() {
        return errorAgeThreshold.isDisplayed();
    }

    public void clickProductSettingSelective(FlowSettings setting) {
        WebElement settings = Selenide.$x(String.format(settingCheckBox, setting.getValue()));
        scrollToWebElement(settings);
        settings.click();
    }

    public void clickProductSettingRadioButton(FlowSettings setting) {
        Selenide.$x(String.format(settingRadioButton, setting.getValue())).click();
    }

    public boolean isIncorrectTypeDocumentWarningDisplayed() {
        return incorrectTypeDocumentWarning.isDisplayed();
    }

    public void switchToVerificationWizard() {
        WebDriverRunner.getWebDriver()
                .switchTo()
                .frame(getVerificationWizard());
    }

    public String getVerificationWizardTitle() {
        return verificationWizardTitle.getText();
    }

    public boolean isAddStepButtonNotDisplayed() {
        return addStepButton.isDisplayed();
    }

    public boolean isErrorVerificationPopupAppeared() {
        return errorVerificationPopup.isDisplayed();
    }

    public void clickEditStepButton(int number) {
        Selenide.$x(String.format(editStepButton, number)).click();
    }

    public void fillCountryRestriction(String value) {
        countryRestrictionInput.sendKeys(value);
        countryRestrictionInput.sendKeys(Keys.ENTER);
    }

    public void clickCountryRestriction(String country) {
        Selenide.$x(String.format(countryRestrictionButton, country)).click();
    }

    public boolean isCountryRestrictionDisplayed(String country) {
        return Selenide.$x(String.format(countryRestrictionButton, country)).isDisplayed();
    }

    public void clickCrossCountryRestrictionButton() {
        crossCountryRestrictionButton.click();
    }

    public boolean isSettingsRadioButtonSelected(FlowSettings settings) {
        return Selenide.$x(String.format(settingRadioButton + "/ancestor::span[contains(@class, 'MuiRadio')]", settings.getValue())).getAttribute("class").contains("Mui-checked");
    }
}