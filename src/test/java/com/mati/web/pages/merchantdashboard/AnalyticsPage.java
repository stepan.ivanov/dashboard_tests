package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.enums.CountriesFilter;
import com.mati.enums.Period;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.open;

public class AnalyticsPage extends AbstractPage {

    private FilterModalWindow filterModalWindow = new FilterModalWindow();
    private final String URL = EnvironmentConfig.url + "/analytics";
    private WebElement reviewModeButton = Selenide.$x("//button[@data-qa = 'review-banner-button-goToReviewMode']");
    private WebElement closeInformationPopUpButton = Selenide.$x("//a[@id = 'pushActionRefuse']");
    private WebElement informationPopUp = Selenide.$x("//div[@id = 'beamerPushModal']");
    private WebElement popUpFrame = Selenide.$x("//iframe[@id = 'beamerAnnouncementPopup']");
    private WebElement closePostButton = Selenide.$x("//div[@class = 'popupClose']");
    private WebElement map = Selenide.$x("//div[@id = 'map']");
    private WebElement alert = Selenide.$x("//div[@role='alert']");
    private WebElement verifications = Selenide.$x("//div[text()='Verifications']").preceding(0);
    private WebElement verifiedUsers = Selenide.$x("//div[text()='Verified users']").preceding(0);
    private WebElement rejectedUsers = Selenide.$x("//div[text()='Rejected users']").preceding(0);
    private WebElement filtersButton = Selenide.$x("//button[@data-qa='analytics-button-open-filter']");
    private String countriesFilter = "//input[@value='%s']";


    public boolean isAnalyticsPage() {
        return WebDriverRunner.getWebDriver().getCurrentUrl().equals(URL);
    }

    public AnalyticsPage navigateToPage() {
        open(URL);
        return this;
    }

    public boolean isReviewModeButtonEnabled() {
        return reviewModeButton.isEnabled();
    }

    public boolean isReviewModeButtonBlack() {
        return reviewModeButton
                .getCssValue("background-color")
                .equals(IConstants.reviewModeButtonColor);
    }

    public void clickReviewModeButton() {
        reviewModeButton.click();
    }

    public void closeInformationWizard() {
        if (informationPopUp.isDisplayed()) closeInformationPopUpButton.click();
    }

    public void closePostPopUp() {
        if (popUpFrame.isDisplayed()) {
            WebDriverRunner.getWebDriver().switchTo().frame(popUpFrame);
            closePostButton.click();
        }
    }

    public String getAlertMessage() {
        return WaitUtils.waitElementToBeVisible(alert).getText();
    }

    public int getVerificationsAmount() {
        return Integer.parseInt(WaitUtils.waitElementToBeVisible(verifications).getText());
    }

    public int getVerifiedUsersAmount() {
        return Integer.parseInt(verifiedUsers.getText());
    }

    public int getRejectedUsersAmount() {
        return Integer.parseInt(rejectedUsers.getText());
    }

    public void openFilters() {
        filtersButton.click();
        filterModalWindow.waitForPageLoaded();
    }

    public void applyFilter() {
        filterModalWindow.clickViewResultsButton();
        WaitUtils.waitElementToBeVisible(verifications);
    }

    public void setFilterPeriod(Period period) {
        filterModalWindow.setPeriod(period);
    }

    public void setFilterByCountries(CountriesFilter country) {
        SelenideElement selenideElement = Selenide.$x(String.format(countriesFilter, country.getValue()));
        scrollToWebElement(selenideElement).click();
    }
}
