package com.mati.web.pages.merchantdashboard.verificationmodal;

import com.codeborne.selenide.Selenide;
import com.mati.enums.verificationchecks.Checks;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.By;

import java.util.Objects;

public class Modal extends AbstractPage {

    private final String documentCheck = "//*[@id='%s']/div/div";

    public boolean isVerificationCheckSuccess(Checks checks) {
        return Objects.equals(Selenide.$x(String.format(documentCheck, checks.getValue()))
                .find(By.cssSelector("path"))
                .getAttribute("stroke"), "#5AC794");
    }
}
