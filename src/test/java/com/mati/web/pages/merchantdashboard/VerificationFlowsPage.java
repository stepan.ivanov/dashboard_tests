package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.WaitUtils;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.open;

public class VerificationFlowsPage extends AbstractPage {

    private final String URL = EnvironmentConfig.url + "/flow";
    private String verificationFlowName = "//h4[text() = '%s']";
    private WebElement createFlowButton = Selenide.$x("//button[@data-qa = 'flows-button-create']");
    private WebElement inputFLowName = Selenide.$x("//input[@data-qa = 'flows-create-input-name']");
    private WebElement createFlowSuccessButton = Selenide.$x("//button[@data-qa = 'flows-create-button-success']");
    private String removeFlowButton = "//h4[text() = '%s']/ancestor::td/following-sibling::td/button";
    private WebElement deleteButton = Selenide.$x("//div[@id = 'overlayRoot']//button[2]");
    private String flowId = "//h4[text() = '%s']/ancestor::td/following-sibling::td//div[text() = 'Flow ID']/preceding-sibling::span";
    private ElementsCollection flowNamesList = Selenide.$$x("//td/div/h4");

    public void selectVerificationFlowByName(String flowName) {
        Selenide.$x(String.format(verificationFlowName, flowName)).click();
        waitToBeInvisibilityDashboardLoader();
    }

    public VerificationFlowsPage navigateToPage() {
        open(URL);
        WaitUtils.waitElementToBeVisible(createFlowButton);
        return this;
    }

    public void clickCreateFlowButton() {
        createFlowButton.click();
    }

    public void fillFlowName(String flowName) {
        inputFLowName.sendKeys(flowName);
    }

    public void clickCreateFlowsSuccessButton() {
        createFlowSuccessButton.click();
        WaitUtils.sleepSeconds(5);
    }

    public void removeFlowByName(String flowName) {
        Selenide.$x(String.format(removeFlowButton, flowName)).click();
        deleteButton.click();
        WaitUtils.waitElementToBeVisible(createFlowButton);
    }

    public boolean isVerificationFlowsPage() {
        waitForPageLoad();
        return WebDriverRunner.getWebDriver().getCurrentUrl().equals(URL);
    }

    public String getFlowId(String flowName) {
        return Selenide.$x(String.format(flowId, flowName)).getText();
    }

    public boolean isFlowExistInList(String value) {
        return flowNamesList.stream().anyMatch(flowName -> flowName.getText().equals(value));
    }
}
