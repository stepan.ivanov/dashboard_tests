package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

import java.util.List;

public class VerificationHistoryPage {

    private WebElement historyTable = Selenide.$x("//tbody[@data-qa='verificationHistory-table']");
    private ElementsCollection historyTableActions = Selenide.$$x("//tbody[@data-qa='verificationHistory-table']//tr//td[2]");

    public void waitPageIsLoaded(){
        WaitUtils.waitElementToBeVisible(historyTable);
    }

    public List<String> getListHistoryActions(){
        return historyTableActions.texts();
    }
}
