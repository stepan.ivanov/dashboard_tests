package com.mati.web.pages.merchantdashboard;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.enums.verificationchecks.Checks;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.RandomUtils;
import com.mati.utils.WaitUtils;
import com.mati.web.pages.AbstractPage;
import com.mati.web.pages.merchantdashboard.verificationmodal.DocumentVerificationModal;
import com.mati.web.pages.merchantdashboard.verificationmodal.IpCheckModal;
import com.mati.web.pages.merchantdashboard.verificationmodal.Modal;
import com.mati.web.pages.merchantdashboard.verificationmodal.PhoneCheckModal;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class VerificationObjectPage extends AbstractPage {

    private DeviceCheckModal deviceCheckModal = new DeviceCheckModal();
    private DocumentVerificationModal documentVerificationModal = new DocumentVerificationModal();
    private IpCheckModal ipCheckModal = new IpCheckModal();
    private PhoneCheckModal phoneCheckModal = new PhoneCheckModal();
    private WebElement verificationStatusButton = Selenide.$x("//div[@data-qa = 'verification-statusSelector-button']");
    private String verificationStatus = "//div[@data-qa = 'verification-statusSelector-content']//h6[text() = '%s']";
    private WebElement colorButton = Selenide.$x("//div[@data-qa = 'verification-statusSelector-button']/div");
    private WebElement activeStatus = Selenide.$x("//div[@data-qa = 'verification-statusSelector-button']/div/h3");
    private WebElement downloadPDFButton = Selenide.$x("//button[@data-qa = 'verification-button-downloadPdf']");
    private WebElement verificationNumber = Selenide.$x("//span[@data-qa = 'verification-number-value']");
    private WebElement alert = Selenide.$x("//div[@class = 'Toastify__toast-body']");
    private WebElement deleteVerificationButton = Selenide.$x("//button[@data-qa = 'verification-button-delete']");
    private WebElement deleteButton = Selenide.$x("//button[contains(@class, 'btn-delete')]");
    private ElementsCollection verificationFields = Selenide.$$x("//div/label/preceding-sibling::p|//input[@type]");
    private WebElement backToVerificationListButton = Selenide.$x("//button[@data-qa='verification-button-backToList']");
    private WebElement verificationDataButton = Selenide.$x("//button[@data-qa='verification-button-data']");
    private WebElement pageHistoryButton = Selenide.$x("//button[@data-qa='verification-button-history']");
    private WebElement verificationDate = Selenide.$x("//h6[@data-qa='verification-date']");
    private WebElement verificationFlow = Selenide.$x("//h6[@data-qa='verification-flow']");
    private WebElement verificationSource = Selenide.$x("//h6[@data-qa='verification-source']");
    private WebElement verificationNotFoundBanner = Selenide.$x("//div/h4[text()='Verification Not Found']");
    private WebElement name = Selenide.$x("//div[text()='Name']/following-sibling::div/div");
    private WebElement copyIdentityIdButton = Selenide.$x("//div[text()='Identity Id']/parent::div//button");
    private WebElement identityId = Selenide.$x("//div[text()='Identity Id']/following-sibling::div/div[last()]");
    private WebElement ipGeolocation = Selenide.$x("//div[text()='IP Geolocation']/following-sibling::div/div[last()]");
    private WebElement dateOfBirthAge = Selenide.$x("//div[text()='Date of birth (age)']/following-sibling::div/div[last()]");
    private WebElement documentVerificationButton = Selenide.$x("//div[text()='Document Verification']");
    private WebElement ipCheckButton = Selenide.$x("//div[text()='IP check']");
    private WebElement governmentCheckButton = Selenide.$x("//div[text()='Government check']");
    private WebElement deviceCheckButton = Selenide.$x("//div[text()='Device Check']");
    private WebElement phoneCheckButton = Selenide.$x("//div[text()='Phone check']");
    private WebElement certifiedTimestampButton = Selenide.$x("//div[text()='Certified Timestamp']");
    private WebElement certifiedTimestamp = Selenide.$x("//h6[text()='Certified timestamp']/parent::div/parent::div//code");
    private WebElement uniqueID = Selenide.$x("//h6[text()='Unique ID']/parent::div/following-sibling::div/div/div");
    private WebElement copyUniqueIdButton = Selenide.$x("//h6[text()='Unique ID']/parent::div/following-sibling::div//button");

    public void selectVerificationStatus(VerificationStatus status) {
        verificationStatusButton.click();
        Selenide.$x(String.format(verificationStatus, status.getStatus())).click();
    }

    public boolean isVerificationStatusButtonHasCorrectColor(VerificationStatus status) {
        return colorButton.getCssValue("background-color").equals(status.getRgba());
    }

    public boolean isVerificationStatusCorrect(VerificationStatus status) {
        return activeStatus.getText().equals(status.getStatus());
    }

    public void downloadVerificationPDF() {
        downloadPDFButton.click();
        WaitUtils.sleepSeconds(10);
    }

    public String getVerificationNumber() {
        return verificationNumber.getText();
    }

    public void waitSuccessfulAlertToBeVisibility() {
        WaitUtils.waitElementToBeVisible(alert);
    }

    public void deleteVerification() {
        deleteVerificationButton.click();
        deleteButton.click();
    }

    public String gerVerificationPageURL() {
        return WebDriverRunner.getWebDriver().getCurrentUrl();
    }

    public void clickEditDataButton() {
        documentVerificationModal.clickEditDataButton();
    }

    public void fillVerificationFields() {
        verificationFields.stream().forEach(f -> fillField(f, RandomUtils.randomNumber()));
    }

    public List getDataOfVerificationFields() {
        List verificationValues = new ArrayList();
        String value;
        for (WebElement element : verificationFields) {
            if (element.getTagName().equals("input")) {
                value = element.getAttribute("value");
            } else {
                value = element.getText();
            }
            verificationValues.add(value);
        }
        return verificationValues;
    }

    public void clickSaveAndSendWebhookButton() {
        documentVerificationModal.clickSaveAndSendWebhookButton();
    }

    public void scrollToEditDataButton() {
        documentVerificationModal.scrollToEditDataButton();
    }

    public VerificationObjectPage waitUntilPageIsLoading() {
        WaitUtils.waitElementToBeVisible(verificationStatusButton);
        return this;
    }

    public boolean isBackToVerificationListButtonDisplayed() {
        return backToVerificationListButton.isDisplayed();
    }

    public boolean isVerificationStatusButtonDisplayed() {
        return verificationStatusButton.isDisplayed();
    }

    public boolean isDownloadPdfButtonDisplayed() {
        return downloadPDFButton.isDisplayed();
    }

    public boolean isVerificationDataButtonDisplayed() {
        return verificationDataButton.isDisplayed();
    }

    public boolean isPageHistoryButtonDisplayed() {
        return pageHistoryButton.isDisplayed();
    }

    public boolean isVerificationDateDisplayed() {
        return verificationDate.isDisplayed();
    }

    public boolean isVerificationNumberDisplayed() {
        return verificationNumber.isDisplayed();
    }

    public boolean isVerificationFlowDisplayed() {
        return verificationFlow.isDisplayed();
    }

    public boolean isVerificationSourceDisplayed() {
        return verificationSource.isDisplayed();
    }

    public void clickOnVerificationDataButton() {
        verificationDataButton.click();
    }

    public void clickOnPageHistoryButton() {
        pageHistoryButton.click();
    }

    public void clickOnBackToVerificationListButton() {
        backToVerificationListButton.click();
    }

    public boolean isVerificationNotFoundBannerDisplayed() {
        WaitUtils.waitElementToBeVisible(verificationNotFoundBanner);
        return verificationNotFoundBanner.isDisplayed();
    }

    public void fillLastName(String lastName) {
        documentVerificationModal.fillLastName(lastName);
    }

    public void fillFullName(String fullName) {
        documentVerificationModal.fillFullName(fullName);
    }

    public void fillFirstName(String firstName) {
        documentVerificationModal.fillFirstName(firstName);
    }

    public void saveEdits() {
        documentVerificationModal.saveEdits();
    }

    public String getFirstName() {
        return documentVerificationModal.getFirstName();
    }

    public String getFullName() {
        return documentVerificationModal.getFullName();
    }

    public String getSurName() {
        return documentVerificationModal.getSurName();
    }

    public String getDateOfBirth() {
        return documentVerificationModal.getDateOfBirth();
    }

    public void fillDayOfBirth(String day) {
        documentVerificationModal.fillDayOfBirth(day);
    }

    public void fillMonthOfBirth(String month) {
        documentVerificationModal.fillMonthOfBirth(month);
    }

    public void fillYearOfBirth(String year) {
        documentVerificationModal.fillYearOfBirth(year);
    }

    public String getName() {
        return name.getText();
    }

    public void fillInputDocumentNumber(String documentNumber) {
        documentVerificationModal.fillInputDocumentNumber(documentNumber);
    }

    public void fillInputDocumentType(String documentType) {
        documentVerificationModal.fillInputDocumentType(documentType);
    }

    public String getDocumentNumber() {
        return documentVerificationModal.getDocumentNumber();
    }

    public String getDocumentType() {
        return documentVerificationModal.getDocumentType();
    }

    public void fillDayOfExpirationDate(String day) {
        documentVerificationModal.fillDayOfExpirationDate(day);
    }

    public void fillMonthOfExpirationDate(String month) {
        documentVerificationModal.fillMonthOfExpirationDate(month);
    }

    public void fillYearOfExpirationDate(String year) {
        documentVerificationModal.fillYearOfExpirationDate(year);
    }

    public String getDateOfExpiration() {
        return documentVerificationModal.getDateOfExpiration();
    }

    public void clickCopyIdentityIdButton() {
        copyIdentityIdButton.click();
    }

    public String getIdentityId() {
        return identityId.getText();
    }

    public void clickDocumentVerificationButton() {
        documentVerificationButton.click();
    }

    public void clickIpCheckButton() {
        ipCheckButton.click();
    }

    public void clickGovernmentCheckButton() {
        governmentCheckButton.click();
    }

    public boolean isDocumentImageDisplayed() {
        return documentVerificationModal.isDocumentImageDisplayed();
    }

    public void clickOnDeviceCheckButton() {
        deviceCheckButton.click();
    }

    public boolean isOsDisplayed() {
        return deviceCheckModal.isOsDisplayed();
    }

    public boolean isBrowserDisplayed() {
        return deviceCheckModal.isBrowserDisplayed();
    }

    public boolean isDeviceTypeDisplayed() {
        return deviceCheckModal.isDeviceTypeDisplayed();
    }

    public boolean isNameDisplayed() {
        return name.isDisplayed();
    }

    public boolean isDateOfBirthAgeDisplayed() {
        return dateOfBirthAge.isDisplayed();
    }

    public boolean isIdentityIdDisplayed() {
        return identityId.isDisplayed();
    }

    public boolean isIpGeolocationDisplayed() {
        return ipGeolocation.isDisplayed();
    }

    public void clickCertifiedTimestampButton() {
        certifiedTimestampButton.click();
        WaitUtils.waitElementToBeClickable(copyUniqueIdButton);
    }

    public String getUniqueId() {
        return uniqueID.getText();
    }

    public void clickOnCopyUniqueIdButton() {
        copyUniqueIdButton.click();
    }

    public boolean isCertifiedTimestampDisplayed() {
        return certifiedTimestamp.isDisplayed();
    }

    public boolean isUniqueIdDisplayed() {
        return uniqueID.isDisplayed();
    }

    public void waitUntilVerificationFinished() {
        documentVerificationModal.waitUntilVerificationFinished();
    }

    public boolean isVerificationCheckSuccess(Checks checks) {
        return new Modal().isVerificationCheckSuccess(checks);
    }

    public boolean isImgMapDisplayed() {
        return ipCheckModal.isImgMapDisplayed();
    }

    public boolean isCountryDisplayed() {
        return ipCheckModal.isCountryDisplayed();
    }

    public boolean isProvinceDisplayed() {
        return ipCheckModal.isProvinceDisplayed();
    }

    public boolean isCityDisplayed() {
        return ipCheckModal.isCityDisplayed();
    }

    public boolean isZipCodeDisplayed() {
        return ipCheckModal.isZipCodeDisplayed();
    }

    public boolean isVpnDisplayed() {
        return ipCheckModal.isVpnDisplayed();
    }

    public boolean isGeoRestrictionsDisplayed() {
        return ipCheckModal.isGeoRestrictionsDisplayed();
    }

    public void waitForIpCheckPageLoaded() {
        ipCheckModal.waitForPageLoad();
    }

    public void clickOnFrontDocumentImage() {
        documentVerificationModal.clickOnFrontDocumentImage();
    }

    public void clickOnBackDocumentImage() {
        documentVerificationModal.clickOnBackDocumentImage();
    }

    public void openZoomedImageInNewTab() {
        documentVerificationModal.openZoomedImageInNewTab();
    }

    public boolean isImageInNewTabDisplayed() {
        return documentVerificationModal.isImageInNewTabDisplayed();
    }

    public void clickPhoneCheckButton() {
        phoneCheckButton.click();
    }

    public boolean isCountryCodeInValidationsWindowDisplayed() {
        return phoneCheckModal.isCountryCodeInValidationsWindowDisplayed();
    }

    public boolean isStatusDisplayed() {
        return phoneCheckModal.isStatusDisplayed();
    }

    public boolean isCellphoneNumberInValidationsWindowDisplayed() {
        return phoneCheckModal.isCellphoneNumberInValidationsWindowDisplayed();
    }

    public boolean isCountryCodeInRiskWindowDisplayed() {
        return phoneCheckModal.isCountryCodeInRiskWindowDisplayed();
    }

    public boolean isCellphoneNumberInRiskWindowDisplayed() {
        return phoneCheckModal.isCellphoneNumberInRiskWindowDisplayed();
    }

    public boolean isRiskLevelDisplayed() {
        return phoneCheckModal.isRiskLevelDisplayed();
    }

    public boolean isRiskScoreDisplayed() {
        return phoneCheckModal.isRiskScoreDisplayed();
    }

    public boolean isTimeZoneDisplayed() {
        return phoneCheckModal.isTimeZoneDisplayed();
    }

    public boolean isLineTypeDisplayed() {
        return phoneCheckModal.isLineTypeDisplayed();
    }

    public boolean isCarrierDisplayed() {
        return phoneCheckModal.isCarrierDisplayed();
    }
}
