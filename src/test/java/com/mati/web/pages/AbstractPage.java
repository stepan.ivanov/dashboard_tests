package com.mati.web.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import static com.codeborne.selenide.Selenide.executeJavaScript;

public class AbstractPage {

    private WebElement closeInformationPopUpButton = Selenide.$x("//a[@id = 'pushActionRefuse']");
    private WebElement informationPopUp = Selenide.$x("//div[@id = 'beamerPushModal']");
    private WebElement popUpFrame = Selenide.$x("//iframe[@id = 'beamerAnnouncementPopup']");
    private WebElement closePostButton = Selenide.$x("//div[@class = 'popupClose']");
    private WebElement spinner = Selenide.$x("//div[@class = 'lds-spinner']");
    private WebElement dashboardLoader =
            Selenide.$x("//*[local-name() = 'svg'][@data-qa = 'dashboard-loader']");
    private WebElement newDesignButton =
            Selenide.$x("//span[text() = 'Switch to new design']/parent::button");
    private ElementsCollection listOfAlerts =
            Selenide.$$x("//div[@role='alert']/following::button");

    public void waitForPageLoad() {
        WaitUtils.sleepSeconds(5);
    }

    public void closeInformationWizard() {
        if (informationPopUp.isDisplayed()) closeInformationPopUpButton.click();
    }

    public void closePostPopUp() {
        if (popUpFrame.isDisplayed()) {
            WebDriverRunner.getWebDriver().switchTo().frame(popUpFrame);
            closePostButton.click();
        }
    }

    public void waitSpinnerToBeInvisibility() {
        WaitUtils.waitElementToBeInvisibility(spinner);
    }

    public WebElement scrollToWebElement(WebElement element) {
        ((JavascriptExecutor) WebDriverRunner.getWebDriver())
                .executeScript("arguments[0].scrollIntoView();", element);
        return element;
    }

    public void waitToBeInvisibilityDashboardLoader() {
        WaitUtils.waitElementToBeInvisibility(dashboardLoader);
    }

    public void dragAndDrop(WebElement source, WebElement target) {
        JavascriptExecutor js = (JavascriptExecutor) WebDriverRunner.getWebDriver();
        js.executeScript(
                "function createEvent(typeOfEvent) {"
                        + "var event =document.createEvent(\"CustomEvent\");"
                        + "event.initCustomEvent(typeOfEvent,true, true, null);"
                        + "event.dataTransfer = {"
                        + "data: {},"
                        + "setData: function (key, value) {"
                        + "this.data[key] = value;"
                        + "},"
                        + "getData: function (key) {"
                        + "return this.data[key];"
                        + "}"
                        + "};"
                        + "return event;"
                        + "}"
                        + ""
                        + "function dispatchEvent(element, event,transferData) {"
                        + "if (transferData !== undefined) {"
                        + "event.dataTransfer = transferData;"
                        + "}"
                        + "if (element.dispatchEvent) {"
                        + "element.dispatchEvent(event);"
                        + "} else if (element.fireEvent) {"
                        + "element.fireEvent(\"on\" + event.type, event);"
                        + "}"
                        + "}"
                        + ""
                        + "function simulateHTML5DragAndDrop(element, destination) {"
                        + "var dragStartEvent =createEvent('dragstart');"
                        + "dispatchEvent(element, dragStartEvent);"
                        + "var dropEvent = createEvent('drop');"
                        + "dispatchEvent(destination, dropEvent,dragStartEvent.dataTransfer);"
                        + "var dragEndEvent = createEvent('dragend');"
                        + "dispatchEvent(element, dragEndEvent,dropEvent.dataTransfer);"
                        + "}"
                        + ""
                        + "var source = arguments[0];"
                        + "var destination = arguments[1];"
                        + "simulateHTML5DragAndDrop(source,destination);",
                source,
                target);
    }

    public boolean isAttributePresent(WebElement element, String attribute) {
        return element.getAttribute(attribute) != null;
    }

    public void hoverMouseToElement(WebElement element) {
        Actions action = new Actions(WebDriverRunner.getWebDriver());
        action.moveToElement(element).build().perform();
    }

    public void closeAlerts() {
        listOfAlerts.forEach(WebElement::click);
    }

    public void refreshPage() {
        WebDriverRunner.getWebDriver().navigate().refresh();
        if (isAlertPresent()) WebDriverRunner.getWebDriver().switchTo().alert().accept();
    }

    public Alert getAlertPopup() {
        return WebDriverRunner.getWebDriver().switchTo().alert();
    }

    public void switchToNewDesign() {
        if (newDesignButton.isDisplayed()) {
            newDesignButton.click();
        }
    }

    public void clickCancelButtonInAlertPopup() {
        getAlertPopup().dismiss();
    }

    public boolean isAlertPresent() {
        try {
            WebDriverRunner.getWebDriver().switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public void fillField(WebElement element, String value) {
        WaitUtils.waitElementToBeVisible(element);
        element.sendKeys(Keys.CONTROL + "A");
        element.sendKeys(Keys.BACK_SPACE);
        element.sendKeys(value);
    }

    public WebElement getShadowRootElement(WebElement element) {
        return executeJavaScript("return arguments[0].shadowRoot", element);
    }

    public void selectCheckbox(WebElement checkbox, boolean flag) {
        scrollToWebElement(checkbox);
        if (checkbox.isSelected() != flag) checkbox.click();
    }
}
