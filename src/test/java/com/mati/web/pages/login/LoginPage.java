package com.mati.web.pages.login;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.WaitUtils;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.open;

public class LoginPage extends AbstractPage {

    private final String URL = EnvironmentConfig.url + "/auth/signin";
    private final WebElement singInButton = Selenide.$x("//button[@data-qa = 'login-button-submit']");
    private final WebElement emailErrorMessage = Selenide.$x("//input[@data-qa = 'login-input-email']/ancestor::div/p");
    private final WebElement passwordErrorMessage = Selenide.$x("//input[@data-qa = 'login-input-password']/ancestor::div/p");
    private final WebElement emailField = Selenide.$x("//input[@data-qa = 'login-input-email']");
    private final WebElement passwordField = Selenide.$x("//input[@data-qa = 'login-input-password']");
    private final WebElement signInTitle = Selenide.$x("//div/h1[@mb = '1']");
    private final WebElement languagesDropDown = Selenide.$x("//div[@data-qa = 'menu-languageSelect']");
    private final WebElement matiLogo = Selenide.$x("//header/*[name() = 'svg']");
    private final WebElement forgotPasswordLink = Selenide.$x("//div/a[@data-qa = 'recovery-button-forgot-password']");
    private final WebElement contactUsLink = Selenide.$x("//div/h1[@mb = '1']/following-sibling::div/a[@href]");
    private final WebElement flexStart = Selenide.$x("//div[contains(@class, 'MuiGrid-align-items-xs-flex-start')]");

    public LoginPage navigateToPage() {
        open(EnvironmentConfig.url);
        return this;
    }

    public void clickSignInButton() {
        singInButton.click();
        WaitUtils.sleepSeconds(2);
    }

    public boolean isEmailErrorMessageDisplayed() {
        return emailErrorMessage.isDisplayed();
    }

    public boolean iPasswordErrorMessageDisplayed() {
        return passwordErrorMessage.isDisplayed();
    }

    public String getEmailErrorMessage() {
        return emailErrorMessage.getText();
    }

    public String getPasswordErrorMessage() {
        return passwordErrorMessage.getText();
    }

    public void fillEmailField(String email) {
        emailField.sendKeys(email);
    }

    public void fillPasswordField(String password) {
        passwordField.sendKeys(password);
    }

    public boolean isSignInTitleDisplayed() {
        return signInTitle.isDisplayed();
    }

    public boolean isLanguageMenuDisplayed() {
        return languagesDropDown.isDisplayed();
    }

    public boolean isMatiLogoDisplayed() {
        return matiLogo.isDisplayed();
    }

    public boolean isSignInButtonDisplayed() {
        return singInButton.isDisplayed();
    }

    public boolean isEmailFieldDisplayed() {
        return emailField.isDisplayed();
    }

    public boolean isPasswordFieldDisplayed() {
        return passwordField.isDisplayed();
    }

    public void clickForgotPasswordLink() {
        forgotPasswordLink.click();
    }

    public boolean isContactUsLinkDisplayed() {
        return contactUsLink.isDisplayed();
    }

    public boolean isForgotPasswordLinkDisplayed() {
        return forgotPasswordLink.isDisplayed();
    }

    public boolean isFlexStartDisplayed() {
        return flexStart.isDisplayed();
    }

    public boolean isLoginPage() {
        return WebDriverRunner.getWebDriver().getCurrentUrl().equals(URL);
    }

    public LoginPage waitUntilPageLoading() {
        WaitUtils.waitElementToBeVisible(emailField);
        return this;
    }

    public void clickContactUsLink() {
        contactUsLink.click();
    }
}
