package com.mati.web.pages.login;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.EnvironmentConfig;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.WebElement;

public class RecoveryPasswordPage extends AbstractPage {

    private final String URL = EnvironmentConfig.url + "/auth/password-recovery";
    private WebElement emailField = Selenide.$x("//input[@data-qa = 'recovery-input-email']");
    private WebElement sendResetEmailButton = Selenide.$x("//button[@data-qa = 'recovery-button-reset-password-submit']");
    private WebElement invalidEmailAddressErrorMessage = Selenide.$x("//input[@data-qa = 'recovery-input-email']/ancestor::div/p");

    public void fillEmailField(String email) {
        emailField.sendKeys(email);
    }

    public void clickSendResetEmailButton() {
        sendResetEmailButton.click();
    }

    public boolean isInvalidEmailAddressErrorMessageDisplayed() {
        return invalidEmailAddressErrorMessage.isDisplayed();
    }

    public String getInvalidEmailAddressErrorMessage() {
        return invalidEmailAddressErrorMessage.getText();
    }

    public boolean isRecoveryPasswordPage() {
        return WebDriverRunner.getWebDriver().getCurrentUrl().equals(URL);
    }
}
