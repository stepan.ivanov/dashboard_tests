package com.mati.web.pages.login;

import com.codeborne.selenide.Selenide;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class ResetPasswordPage extends AbstractPage {

    private WebElement inputPassword = Selenide.$x("//input[@name='password']");
    private WebElement resetPasswordButton = Selenide.$x("//span[text()='Reset password']/parent::*");

    public ResetPasswordPage fillInputPassword(String password){
        inputPassword.sendKeys(Keys.CONTROL + "A");
        inputPassword.sendKeys(Keys.BACK_SPACE);
        inputPassword.sendKeys(password);
        return this;
    }

    public void clickButtonResetPassword(){
        resetPasswordButton.click();
    }




}
