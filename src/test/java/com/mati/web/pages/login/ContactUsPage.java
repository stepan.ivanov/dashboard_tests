package com.mati.web.pages.login;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.WaitUtils;
import com.mati.web.pages.AbstractPage;
import org.openqa.selenium.WebElement;

public class ContactUsPage extends AbstractPage {

    private WebElement frame = Selenide.$x("//iframe");
    private WebElement emailInput = Selenide.$x("//label[text()='Your professional email*']").sibling(0);
    private WebElement firstNameInput = Selenide.$x("//label[text()='First name*']").sibling(0);
    private WebElement lastNameInput = Selenide.$x("//label[text()='Last name*']").sibling(0);
    private WebElement phoneNumberInput = Selenide.$x("//label[text()='Your phone number*']").sibling(0);
    private WebElement goalsForUsingInput = Selenide.$x("//label[text()='How are you planning to use Mati?']").sibling(0);
    private WebElement submitButton = Selenide.$x("//input[@type='submit']");
    private WebElement scheduleFrame = Selenide.$x("//iframe[@class='chilipiper-frame']");

    public void fillEmailInput(String email) {
        fillField(emailInput, email);
    }

    public void fillFirstNameInput(String firstName) {
        fillField(WaitUtils.waitElementToBeVisible(firstNameInput), firstName);
    }

    public void fillLastNameInput(String lastName) {
        fillField(lastNameInput, lastName);
    }

    public void fillYourPhoneNumber(String number) {
        fillField(phoneNumberInput, number);
    }

    public void fillGoalsForUsing(String goals) {
        fillField(goalsForUsingInput, goals);
    }

    public void clickSubmitButton() {
        WaitUtils.waitElementToBeClickable(submitButton).click();
        WebDriverRunner.getWebDriver().switchTo().defaultContent();
        WaitUtils.waitElementToBeVisible(scheduleFrame);
    }

    public void waitForPageLoaded() {
        WebDriverRunner.getWebDriver().switchTo().frame(frame);
        WaitUtils.waitElementToBeVisible(submitButton);
    }

    public boolean isScheduleFrameDisplayed() {
        return scheduleFrame.isDisplayed();
    }
}
