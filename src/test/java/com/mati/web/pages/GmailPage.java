package com.mati.web.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.open;

public class GmailPage {

    private final String URL = "https://gmail.com";
    private final String emailToField = "//b//span[@email='%s']";
    private WebElement emailField = Selenide.$x("//input[@id = 'identifierId']");
    private WebElement nextButton = Selenide.$x("//button[@jsname = 'LgbsSe']");
    private WebElement profileIdentifier = Selenide.$x("//div[@id = 'profileIdentifier']");
    private WebElement passwordField = Selenide.$x("//input[@jsname = 'YPqjbf']");
    private WebElement gmailLogo = Selenide.$x("//img[contains(@src, 'logo_gmail')]");
    private WebElement currentMessage = Selenide.$x("//span[@email = 'no-reply@getmati.com']/ancestor::tr");
    private WebElement resetPasswordButton = Selenide.$x("//a[contains(@href, 'password-reset')]");
    private WebElement deleteMessageButton = Selenide.$x("//div[@jslog = '20283; u014N:cOuCgd,Kr2w4b']");
    private WebElement setPasswordButton = Selenide.$x("//a[text()='Set Password']");
    private WebElement hideElementButton = Selenide.$x("//div[@class='ajR']");

    public GmailPage navigateToPage() {
        open(URL);
        return this;
    }

    public GmailPage fillEmailField(String email) {
        emailField.click();
        emailField.sendKeys(email);
        return this;
    }

    public GmailPage clickNextButton() {
        nextButton.click();
        return this;
    }

    public GmailPage fillPasswordField(String password) {
        passwordField.click();
        passwordField.sendKeys(password);
        return this;
    }

    public void waitProfileIdentifierToLoaded() {
        WaitUtils.waitElementToBeVisible(profileIdentifier);
    }

    public void waitGmailLogoToLoaded() {
        WaitUtils.waitElementToBeVisible(gmailLogo);
    }

    public GmailPage selectGetMatiMessage() {
        currentMessage.click();
        return this;
    }

    public boolean isResetPasswordButtonDisplayed() {
        return resetPasswordButton.isDisplayed();
    }

    public void deleteMessage() {
        deleteMessageButton.click();
    }

    public boolean isEmailForPresent(String emailFor) {
        return Selenide.$x(String.format(emailToField, emailFor.toLowerCase())).exists();
    }

    public void clickOnSetPasswordButton(){
        setPasswordButton.click();
    }

    public void clickOnHideButton(){
        hideElementButton.click();
    }
}
