package com.mati.web.pages.signup;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.EnvironmentConfig;
import com.mati.utils.WaitUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.open;

public class SignUpPage {

    private final String URL = EnvironmentConfig.url + "/auth/signup?token=RAW6M43YJVAT6P8";
    private String errorMessage = "//div//p[text()='%s']";
    private String errorMessageSpan = "//div//p//span[text()='%s']";

    private WebElement emailInput = Selenide.$x("//input[@name='email']");
    private WebElement passwordInput = Selenide.$x("//input[@name='password']");
    private WebElement signUpButton = Selenide.$x("//button[@data-qa='sign-up-button-submit']");

    public SignUpPage navigateToPage() {
        open(URL);
        return this;
    }

    public SignUpPage fillEmailInput(String email) {
        emailInput.sendKeys(Keys.CONTROL + "A");
        emailInput.sendKeys(Keys.BACK_SPACE);
        emailInput.sendKeys(email);
        return this;
    }

    public SignUpPage fillPasswordInput(String password) {
        passwordInput.sendKeys(Keys.CONTROL + "A");
        passwordInput.sendKeys(Keys.BACK_SPACE);
        passwordInput.sendKeys(password);
        return this;
    }

    public void clickButtonSignUp() {
        signUpButton.click();
    }

    public boolean isErrorMessageDisplayed(String message) {
        return WaitUtils.waitElementToBeVisible(Selenide.$x(String.format(errorMessage, message)))
                .isDisplayed();
    }

    public boolean isErrorMessageSpanDisplayed(String message) {
        return WaitUtils.waitElementToBeVisible(Selenide.$x(String.format(errorMessageSpan, message)))
                .isDisplayed();
    }
}
