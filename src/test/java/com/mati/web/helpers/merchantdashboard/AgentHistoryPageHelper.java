package com.mati.web.helpers.merchantdashboard;

import com.mati.enums.Role;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.AgentHistoryPage;

import java.time.LocalDateTime;

public class AgentHistoryPageHelper extends AbstractHelper {

    private AgentHistoryPage agentHistoryPage = new AgentHistoryPage();

    public boolean isNameDisplayed() {
        boolean isDisplayed = agentHistoryPage.isNameDisplayed();
        log.info("Name is displayed: {}", isDisplayed);
        return isDisplayed;
    }

    public boolean isEmailDisplayed() {
        boolean isDisplayed = agentHistoryPage.isEmailDisplayed();
        log.info("Email is displayed: {}", isDisplayed);
        return isDisplayed;
    }

    public boolean isRoleDisplayed() {
        boolean isDisplayed = agentHistoryPage.isRoleDisplayed();
        log.info("Role is displayed: {}", isDisplayed);
        return isDisplayed;
    }

    public boolean isDateOfRegistration() {
        boolean isDisplayed = agentHistoryPage.isDateOfRegistration();
        log.info("Date of Registration is displayed: {}", isDisplayed);
        return isDisplayed;
    }

    public SettingsPageHelper clickOnButtonDeleteAgentConfirmAndWaitConfirmation() {
        log.info("Click on button 'Delete agent'");
        agentHistoryPage.clickOnDeleteAgentButton();
        log.info("Confirm deleting");
        agentHistoryPage.confirmDeleting();
        agentHistoryPage.waitAlertUserIsDeleted();
        log.info("Confirmation alert is displayed");
        return new SettingsPageHelper();
    }

    public AgentHistoryPageHelper waitUntilPageLoading() {
        log.info("Wait until Agent History page is loading...");
        agentHistoryPage.waitUntilPageLoading();
        return this;
    }

    public String getEmail() {
        String email = agentHistoryPage.getEmail();
        log.info("Current email: {}", email);
        return email;
    }

    public String getName() {
        String name = agentHistoryPage.getName();
        log.info("Current name: {}", name);
        return name;
    }

    public Role getRole() {
        Role role = agentHistoryPage.getRole();
        log.info("Current role: {}", role);
        return role;
    }

    public LocalDateTime getDateOfRegistration() {
        LocalDateTime date = agentHistoryPage.getDateOfRegistration();
        log.info("Current date of registration: {}", date);
        return date;
    }
}
