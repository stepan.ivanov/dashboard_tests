package com.mati.web.helpers.merchantdashboard;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.VerificationDataObjectModal;

public class VerificationDataObjectModalHelper extends AbstractHelper {

    private VerificationDataObjectModal verificationDataObject = new VerificationDataObjectModal();

    public boolean isModalWindowDisplay(){
        boolean isModalWindowDisplay = verificationDataObject.isModalWindowDisplayed();
        log.info("Modal window is displayed: {}", isModalWindowDisplay);
        return isModalWindowDisplay;
    }

    public VerificationDataObjectModalHelper waitPageLoaded(){
        log.info("Wait until page is loading...");
        verificationDataObject.waitPageLoaded();
        return this;
    }

    public String getResponsesAsText(){
        log.info("Get responses as text");
        return verificationDataObject.getResponsesAsText();
    }
}
