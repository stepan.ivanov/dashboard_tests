package com.mati.web.helpers.merchantdashboard;

import com.mati.enums.CountriesFilter;
import com.mati.enums.Period;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.AnalyticsPage;

public class AnalyticsHelper extends AbstractHelper {

    private AnalyticsPage analyticsPage = new AnalyticsPage();

    public AnalyticsHelper navigateToPage() {
        log.info("Navigate to 'Analytics' page");
        analyticsPage.navigateToPage();
        waitForPageLoad();
        return this;
    }

    public boolean isAnalyticsPage() {
        boolean result = analyticsPage.isAnalyticsPage();
        log.info("Check if this is 'Analytics' page - " + result);
        return result;
    }

    public void waitForPageLoad() {
        analyticsPage.waitForPageLoad();
        log.info("Wait for 'analytics' page is loaded");
    }

    public boolean isReviewModeButtonEnabled() {
        boolean result = analyticsPage.isReviewModeButtonEnabled();
        log.info("Check if 'Review mode' button is enabled - " + result);
        return result;
    }

    public boolean isReviewModeButtonBlack() {
        boolean result = analyticsPage.isReviewModeButtonBlack();
        log.info("Check if 'Review mode' button is black - " + result);
        return result;
    }

    public void clickReviewModeButton() {
        analyticsPage.clickReviewModeButton();
        log.info("Click 'Review mode' button");
    }

    public AnalyticsHelper closeInformationWizard() {
        analyticsPage.closeInformationWizard();
        log.info("Close information wizard");
        return this;
    }

    public AnalyticsHelper closePostPopUp() {
        analyticsPage.closePostPopUp();
        log.info("Close post pop up");
        return this;
    }

    public void switchToNewDesign() {
        analyticsPage.switchToNewDesign();
        log.info("Switch to new design pages");
    }

    public String getAlertMessage() {
        String message = analyticsPage.getAlertMessage();
        log.info(String.format("Alert message: %s", message));
        return message;
    }

    public int getVerificationsAmount() {
        int amount = analyticsPage.getVerificationsAmount();
        log.info("Verifications amount : {}", amount);
        return amount;
    }

    public int getVerifiedUsersAmount() {
        int amount = analyticsPage.getVerifiedUsersAmount();
        log.info("Verified user amount: {}", amount);
        return amount;
    }

    public int getRejectedUsersAmount() {
        int amount = analyticsPage.getRejectedUsersAmount();
        log.info("Rejected user amount: {}", amount);
        return amount;
    }

    public AnalyticsHelper openFilters() {
        log.info("Open filters");
        analyticsPage.openFilters();
        return this;
    }

    public AnalyticsHelper applyFilter() {
        log.info("Apply filters");
        analyticsPage.applyFilter();
        return this;
    }

    public AnalyticsHelper setFilterPeriod(Period period) {
        log.info("Set filter period: {}", period.getPeriod());
        analyticsPage.setFilterPeriod(period);
        return this;
    }

    public AnalyticsHelper setFilterByCountry(CountriesFilter country) {
        log.info("Set filter by country: {}", country.getValue());
        analyticsPage.setFilterByCountries(country);
        return this;
    }
}
