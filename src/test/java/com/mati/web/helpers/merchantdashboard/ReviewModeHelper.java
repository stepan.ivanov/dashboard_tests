package com.mati.web.helpers.merchantdashboard;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.ReviewModePage;

public class ReviewModeHelper extends AbstractHelper {

    private ReviewModePage reviewModePage = new ReviewModePage();

    public boolean isReviewModePage() {
        boolean result = reviewModePage.isReviewModePage();
        log.info("Check if this is 'Review mode' page - " + result);
        return result;
    }
}
