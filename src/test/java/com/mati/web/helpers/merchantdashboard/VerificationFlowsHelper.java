package com.mati.web.helpers.merchantdashboard;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.VerificationFlowsPage;

public class VerificationFlowsHelper extends AbstractHelper {

    private VerificationFlowsPage verificationFlowsPage = new VerificationFlowsPage();
    private FlowHelper flowHelper = new FlowHelper();

    public FlowHelper selectVerificationFlowByName(String flowName) {
        verificationFlowsPage.selectVerificationFlowByName(flowName);
        log.info("Select verification flow by name - " + flowName);
        flowHelper.switchToNewDesign();
        return flowHelper;
    }

    public VerificationFlowsHelper navigateToPage() {
        log.info("Navigate to 'Verification Flows' page");
        verificationFlowsPage.navigateToPage();
        return this;
    }

    public void refreshPage() {
        log.info("Refresh 'Verification Flows' page");
        verificationFlowsPage.refreshPage();
    }

    public VerificationFlowsHelper clickCreateFlowButton() {
        verificationFlowsPage.clickCreateFlowButton();
        log.info("Click 'create flows' button");
        return this;
    }

    public VerificationFlowsHelper fillFlowName(String flowName) {
        verificationFlowsPage.fillFlowName(flowName);
        log.info("Input flow name with - " + flowName);
        return this;
    }

    public void clickCreateFlowsSuccessButton() {
        verificationFlowsPage.clickCreateFlowsSuccessButton();
        log.info("Click 'create flows success' button");
    }

    public void createFlow(String flowName) {
        clickCreateFlowButton()
                .fillFlowName(flowName)
                .clickCreateFlowsSuccessButton();
    }

    public VerificationFlowsHelper removeFlowByName(String flowName) {
        verificationFlowsPage.removeFlowByName(flowName);
        log.info("Remove flow by name - " + flowName);
        return this;
    }

    public boolean isVerificationFlowsPage() {
        boolean result = verificationFlowsPage.isVerificationFlowsPage();
        log.info("Check if this is verification flows page - " + result);
        return result;
    }

    public String getFlowId(String flowName) {
        String flowId = verificationFlowsPage.getFlowId(flowName);
        log.info("Get flow id - " + flowId);
        return flowId;
    }

    public boolean isFlowExistInList(String value) {
        boolean result = verificationFlowsPage.isFlowExistInList(value);
        log.info("Check if '" + value + "' flow is exists in verification flows list - " + result);
        return result;
    }
}
