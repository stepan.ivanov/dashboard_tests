package com.mati.web.helpers.merchantdashboard;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.SettingsPage;
import io.qameta.allure.Step;

public class SettingsPageHelper extends AbstractHelper {

    private SettingsPage settingsPage = new SettingsPage();
    private AgentHistoryPageHelper agentHistoryPageHelper = new AgentHistoryPageHelper();

    public SettingsPageHelper navigateToPage() {
        log.info("Navigate to Settings page");
        settingsPage
                .navigateToPage();
//                .waitUntilPageIsLoading();
        return this;
    }

    public boolean isAllPersonHasProfileButton() {
        boolean isAllPersonHasProfileButton = settingsPage.isAllPersonHasProfileButton();
        log.info("All person has profile button : {}", isAllPersonHasProfileButton);
        return isAllPersonHasProfileButton;
    }

    public AgentHistoryPageHelper openRandomProfile() {
        log.info("Open random profile");
        settingsPage.openRandomProfile();
        agentHistoryPageHelper.waitUntilPageLoading();
        return agentHistoryPageHelper;
    }

    public AgentHistoryPageHelper openProfileByEmail(String email) {
        log.info("Open profile with email: {}", email);
        settingsPage.openProfileByEmail(email);
        return agentHistoryPageHelper;
    }

    @Step("Delete user by email : {email}")
    public SettingsPageHelper deleteUserByEmail(String email) {
        log.info("Open Agent with email : {}", email);
        settingsPage.openProfileByEmail(email);
        agentHistoryPageHelper.waitUntilPageLoading();
        log.info("Delete Agent with email : {}", email);
        agentHistoryPageHelper.clickOnButtonDeleteAgentConfirmAndWaitConfirmation();
        return this;
    }
}
