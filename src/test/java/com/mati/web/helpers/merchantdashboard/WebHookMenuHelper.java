package com.mati.web.helpers.merchantdashboard;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.WebHooksMenu;

public class WebHookMenuHelper extends AbstractHelper {

    private WebHooksMenu webHooksMenu = new WebHooksMenu();

    public WebHookMenuHelper waitUntilPageIsOpening() {
        log.info("Wait until page is opening");
        webHooksMenu.waitUntilPageIsOpening();
        return this;
    }

    public WebHookMenuHelper fillSecretInput(String secret) {
        log.info("Fill secret input: {}", secret);
        webHooksMenu.fillSecretInput(secret);
        return this;
    }

    public WebHookMenuHelper fillUrlInput(String url) {
        log.info("Fill url input: {}", url);
        webHooksMenu.fillUrlInput(url);
        return this;
    }

    public WebHookMenuHelper removeLastCharactersInSecretInput(int amount) {
        log.info("Remove last {} characters", amount);
        webHooksMenu.removeLastCharactersInSecretInput(amount);
        return this;
    }

    public String getSecret() {
        String secret = webHooksMenu.getSecret();
        log.info("Get secret: {}", secret);
        return secret;
    }

    public WebHookMenuHelper clickToggleSecretVisibility() {
        log.info("Switch secret visibility toggle");
        webHooksMenu.clickToggleSecretVisibility();
        return this;
    }

    public WebHookMenuHelper clickButtonSave() {
        log.info("Click 'Save' button");
        webHooksMenu.clickSaveButton();
        return this;
    }

    public WebHookMenuHelper waitUntilChangesWillBeSaved() {
        webHooksMenu.waitUntilChangesWillBeSaved();
        return this;
    }

    public boolean isAlertMessageDisplayed() {
        return webHooksMenu.isAlertMessageDisplayed();
    }

    public void close() {
        webHooksMenu.close();
    }
}
