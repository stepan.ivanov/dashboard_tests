package com.mati.web.helpers.merchantdashboard;

import com.codeborne.selenide.Selenide;
import com.mati.enums.verificationchecks.Checks;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.WaitUtils;
import com.mati.web.DriverUtils;
import com.mati.web.IConstants;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.VerificationObjectPage;

import java.util.List;

import static com.codeborne.selenide.Selenide.open;

public class VerificationObjectHelper extends AbstractHelper {

    private VerificationObjectPage verificationObjectPage = new VerificationObjectPage();
    private VerificationListHelper verificationListHelper = new VerificationListHelper();

    public VerificationObjectHelper selectVerificationStatus(VerificationStatus status) {
        verificationObjectPage.selectVerificationStatus(status);
        log.info("Select verification status - " + status);
        WaitUtils.sleepSeconds(5);
        return this;
    }

    public boolean isVerificationStatusButtonHasCorrectColor(VerificationStatus status) {
        boolean result = verificationObjectPage.isVerificationStatusButtonHasCorrectColor(status);
        log.info("Check if verification status button has '" + status.getColor() + "' color - " + result);
        return result;
    }

    public boolean isVerificationStatusCorrect(VerificationStatus status) {
        boolean result = verificationObjectPage.isVerificationStatusCorrect(status);
        log.info("Check if active status is '" + status.getStatus() + "' - " + result);
        return result;
    }

    public VerificationObjectHelper downloadVerificationPDF() {
        verificationObjectPage.downloadVerificationPDF();
        log.info("Download verification PDF file to '" + IConstants.currentFolder + "' folder");
        return this;
    }

    public String getVerificationNumber() {
        String result = verificationObjectPage.getVerificationNumber();
        log.info("Get verification number - " + result);
        return result;
    }

    public VerificationObjectHelper waitSuccessfulAlertToBeVisibility() {
        verificationObjectPage.waitSuccessfulAlertToBeVisibility();
        log.info("Wait for 'Webhook has been sent' message will be displayed");
        return this;
    }

    public void deleteVerification() {
        verificationObjectPage.deleteVerification();
        log.info("Delete verification");
        verificationListHelper.waitForPageLoad();
    }

    public boolean isVerificationObjectPage() {
        boolean result = verificationObjectPage.gerVerificationPageURL().equals(verificationListHelper.getURL() + "/" + verificationObjectPage.getVerificationNumber());
        log.info("Check if verification is open - " + result);
        return result;
    }

    public VerificationObjectHelper clickEditDataButton() {
        verificationObjectPage.clickEditDataButton();
        log.info("Click edit data button");
        return this;
    }

    public VerificationObjectHelper fillVerificationFields() {
        verificationObjectPage.fillVerificationFields();
        log.info("Change data in verification fields");
        return this;
    }

    public List getDataOfVerificationFields() {
        List list = verificationObjectPage.getDataOfVerificationFields();
        log.info("Get data from verification fields - " + list.toString());
        return list;
    }

    public VerificationObjectHelper clickSaveAndSendWebhookButton() {
        verificationObjectPage.clickSaveAndSendWebhookButton();
        log.info("Click save and send webhook button");
        return this;
    }

    public VerificationObjectHelper scrollToEditDataButton() {
        verificationObjectPage.scrollToEditDataButton();
        log.info("Scroll page to edit data button");
        return this;
    }

    public VerificationObjectHelper waitForPageLoad() {
        log.info("Wait until page 'Verification Object' is loading...");
        verificationObjectPage.waitUntilPageIsLoading();
        return this;
    }

    public boolean isBackToVerificationListButtonDisplayed() {
        boolean isBackToVerificationListButtonDisplayed = verificationObjectPage.isBackToVerificationListButtonDisplayed();
        log.info("'Back to verification list button is displayed: {}", isBackToVerificationListButtonDisplayed);
        return isBackToVerificationListButtonDisplayed;
    }

    public boolean isVerificationStatusButtonDisplayed() {
        boolean isVerificationStatusButtonDisplayed = verificationObjectPage.isVerificationStatusButtonDisplayed();
        log.info("'Verification Status button is displayed: {}", isVerificationStatusButtonDisplayed);
        return isVerificationStatusButtonDisplayed;
    }

    public boolean isDownloadPdfButtonDisplayed() {
        boolean isDownloadPdfButtonDisplayed = verificationObjectPage.isDownloadPdfButtonDisplayed();
        log.info("Download PDF button is displayed: {}", isDownloadPdfButtonDisplayed);
        return isDownloadPdfButtonDisplayed;
    }

    public boolean isVerificationDataButtonDisplayed() {
        boolean isVerificationDataButtonDisplayed = verificationObjectPage.isVerificationDataButtonDisplayed();
        log.info("Verification data button is displayed: {}", isVerificationDataButtonDisplayed);
        return isVerificationDataButtonDisplayed;
    }

    public boolean isPageHistoryButtonDisplayed() {
        boolean isPageHistoryButtonDisplayed = verificationObjectPage.isPageHistoryButtonDisplayed();
        log.info("Page History button is displayed: {}", isPageHistoryButtonDisplayed);
        return isPageHistoryButtonDisplayed;
    }

    public boolean isVerificationDateDisplayed() {
        boolean isVerificationDateDisplayed = verificationObjectPage.isVerificationDateDisplayed();
        log.info("Verification page is displayed: {}", isVerificationDateDisplayed);
        return isVerificationDateDisplayed;
    }

    public boolean isVerificationNumberDisplayed() {
        boolean isVerificationNumberDisplayed = verificationObjectPage.isVerificationNumberDisplayed();
        log.info("Verification Number is displayed: {}", isVerificationNumberDisplayed);
        return isVerificationNumberDisplayed;
    }

    public boolean isVerificationFlowDisplayed() {
        boolean isVerificationFlowDisplayed = verificationObjectPage.isVerificationFlowDisplayed();
        log.info("Verification Flow is displayed: {}", isVerificationFlowDisplayed);
        return isVerificationFlowDisplayed;
    }

    public boolean isVerificationSourceDisplayed() {
        boolean isVerificationSourceDisplayed = verificationObjectPage.isVerificationSourceDisplayed();
        log.info("Verification Source is displayed: {}", isVerificationSourceDisplayed);
        return isVerificationSourceDisplayed;
    }

    public VerificationDataObjectModalHelper clickOnVerificationDataButton() {
        verificationObjectPage.clickOnVerificationDataButton();
        log.info("Click on Verification Data Button");
        return new VerificationDataObjectModalHelper();
    }

    public void clickOnHistoryPageButton() {
        verificationObjectPage.clickOnPageHistoryButton();
        log.info("Click on History Page Button");
    }

    public VerificationObjectHelper closeAlerts() {
        log.info("Close alerts");
        verificationObjectPage.closeAlerts();
        return this;
    }

    public VerificationListHelper clickOnBackToVerificationListButton() {
        log.info("Click on 'Back to verification List' button");
        verificationObjectPage.clickOnBackToVerificationListButton();
        return verificationListHelper;
    }

    public VerificationObjectHelper switchToNewView() {
        String currentUrl = DriverUtils.getCurrentUrl();
        String newUrl = currentUrl.replace("identities", "identity");
        open(newUrl);
//        Wait until page will be loaded. Workaround.
        WaitUtils.waitElementToBeClickable(Selenide.$x("//div[text()='Passed flows']/parent::div//button")).click();
        WaitUtils.waitElementToBeClickable(Selenide.$x("//div[text()='Passed flows']/following-sibling::div/div[last()]")).click();
        waitForPageLoad();
        return this;
    }

    public boolean isVerificationNotFoundBannerDisplayed() {
        boolean isVerificationNotFoundBannerDisplayed = verificationObjectPage.isVerificationNotFoundBannerDisplayed();
        log.info("Verification not found banner is displayed: {}", isVerificationNotFoundBannerDisplayed);
        return isVerificationNotFoundBannerDisplayed;
    }

    public VerificationObjectHelper fillFirstName(String firstName) {
        log.info("Fill first name with {}", firstName);
        verificationObjectPage.fillFirstName(firstName);
        return this;
    }

    public VerificationObjectHelper fillLastName(String lastName) {
        log.info("Fill last name with {}", lastName);
        verificationObjectPage.fillLastName(lastName);
        return this;
    }

    public VerificationObjectHelper fillFullName(String fullName) {
        log.info("Fill full name with {}", fullName);
        verificationObjectPage.fillFullName(fullName);
        return this;
    }

    public VerificationObjectHelper saveEdits() {
        log.info("Save edits...");
        verificationObjectPage.saveEdits();
        waitSuccessfulAlertToBeVisibility();
        closeAlerts();
        return this;
    }

    public String getFirstName() {
        String firstName = verificationObjectPage.getFirstName();
        log.info("First name : {}", firstName);
        return firstName;
    }

    public String getSurName() {
        String surName = verificationObjectPage.getSurName();
        log.info("Surname : {}", surName);
        return surName;
    }

    public String getFullName() {
        String fullName = verificationObjectPage.getFullName();
        log.info("Full name: {}", fullName);
        return fullName;
    }

    public String getDateOfBirth() {
        String dateOfBirth = verificationObjectPage.getDateOfBirth();
        log.info("Date of Birth: {}", dateOfBirth);
        return dateOfBirth;
    }

    public VerificationObjectHelper fillDayOfBirth(String day) {
        log.info("Fill day of birth: {}", day);
        verificationObjectPage.fillDayOfBirth(day);
        return this;
    }

    public VerificationObjectHelper fillMonthOfBirth(String month) {
        log.info("Fill month of birth: {}", month);
        verificationObjectPage.fillMonthOfBirth(month);
        return this;
    }

    public VerificationObjectHelper fillYearOfMonth(String year) {
        log.info("Fill year of month: {}", year);
        verificationObjectPage.fillYearOfBirth(year);
        return this;
    }

    public String getName() {
        String name = verificationObjectPage.getName();
        log.info("Name : {}", name);
        return name;
    }

    public VerificationObjectHelper fillDocumentType(String documentType) {
        log.info("Document type: {}", documentType);
        verificationObjectPage.fillInputDocumentType(documentType);
        return this;
    }

    public VerificationObjectHelper fillInputDocumentNumber(String documentNumber) {
        log.info("Document number: {}", documentNumber);
        verificationObjectPage.fillInputDocumentNumber(documentNumber);
        return this;
    }

    public String getDocumentType() {
        String documentType = verificationObjectPage.getDocumentType();
        log.info("Document type: {}", documentType);
        return documentType;
    }

    public String getDocumentNumber() {
        String documentNumber = verificationObjectPage.getDocumentNumber();
        log.info("Document number: {}", documentNumber);
        return documentNumber;
    }

    public VerificationObjectHelper fillDayOfExpirationDate(String day) {
        log.info("Day of Expiration : {}", day);
        verificationObjectPage.fillDayOfExpirationDate(day);
        return this;
    }

    public VerificationObjectHelper fillMonthOfExpirationDate(String month) {
        log.info("Month of Expiration : {}", month);
        verificationObjectPage.fillMonthOfExpirationDate(month);
        return this;
    }

    public VerificationObjectHelper fillYearOfExpirationDate(String year) {
        log.info("Year of Expiration : {}", year);
        verificationObjectPage.fillYearOfExpirationDate(year);
        return this;
    }

    public String getDateOfExpiration() {
        String dateOfExpiration = verificationObjectPage.getDateOfExpiration();
        log.info("Date of Expiration : {}", dateOfExpiration);
        return dateOfExpiration;
    }

    public VerificationObjectHelper clickCopyIdentityIdButton() {
        log.info("Click 'Copy Identity Id' button");
        verificationObjectPage.clickCopyIdentityIdButton();
        return this;
    }

    public String getIdentityId() {
        String identityId = verificationObjectPage.getIdentityId();
        log.info("Identity Id : {}", identityId);
        return identityId;
    }

    public VerificationObjectHelper clickDocumentVerificationButton() {
        log.info("Click on 'DocumentVerificationButton'");
        verificationObjectPage.clickDocumentVerificationButton();
        return this;
    }

    public VerificationObjectHelper clickIpCheckButton() {
        log.info("Click on 'IpCheckButton'");
        verificationObjectPage.clickIpCheckButton();
        verificationObjectPage.waitForIpCheckPageLoaded();
        return this;
    }

    public VerificationObjectHelper clickGovernmentCheckButton() {
        log.info("Click on 'GovernmentCheck Button'");
        verificationObjectPage.clickGovernmentCheckButton();
        return this;
    }

    public boolean isDocumentImageDisplayed() {
        boolean isNationalIdImageDisplayed = verificationObjectPage.isDocumentImageDisplayed();
        log.info("National id is displayed : {}", isNationalIdImageDisplayed);
        return isNationalIdImageDisplayed;
    }

    public VerificationObjectHelper clickDeviceCheckButton() {
        log.info("Click on 'Device check' button");
        verificationObjectPage.clickOnDeviceCheckButton();
        return this;
    }

    public boolean isOsDisplayed() {
        boolean isOsDisplayed = verificationObjectPage.isOsDisplayed();
        log.info("Os is displayed: {}", isOsDisplayed);
        return isOsDisplayed;
    }

    public boolean isBrowserDisplayed() {
        boolean isBrowserDisplayed = verificationObjectPage.isBrowserDisplayed();
        log.info("Browser is displayed: {}", isBrowserDisplayed);
        return isBrowserDisplayed;
    }

    public boolean isDeviceTypeDisplayed() {
        boolean isDeviceTypeDisplayed = verificationObjectPage.isDeviceTypeDisplayed();
        log.info("Device type is displayed: {}", isDeviceTypeDisplayed);
        return isDeviceTypeDisplayed;
    }

    public boolean isNameDisplayed() {
        boolean isNameDisplayed = verificationObjectPage.isNameDisplayed();
        log.info("Name is displayed: {}", isNameDisplayed);
        return isNameDisplayed;
    }

    public boolean isDateOfBirthAgeDisplayed() {
        boolean isDateOfBirthAgeDisplayed = verificationObjectPage.isDateOfBirthAgeDisplayed();
        log.info("Date of birth age is displayed: {}", isDateOfBirthAgeDisplayed);
        return isDateOfBirthAgeDisplayed;
    }

    public boolean isIdentityIdDisplayed() {
        boolean isIdentityIdDisplayed = verificationObjectPage.isIdentityIdDisplayed();
        log.info("Identity Id is displayed: {}", isIdentityIdDisplayed);
        return isIdentityIdDisplayed;
    }

    public VerificationObjectHelper waitUntilVerificationFinished() {
        log.info("Wait until verification finished");
        verificationObjectPage.waitUntilVerificationFinished();
        return this;
    }

    public boolean isVerificationCheckSuccess(Checks checks) {
        boolean isVerificationCheckSuccess = verificationObjectPage.isVerificationCheckSuccess(checks);
        log.info("{} is success: {}", checks.getName(), isVerificationCheckSuccess);
        return isVerificationCheckSuccess;
    }

    public boolean isIpGeolocationDisplayed() {
        boolean isIpGeolocationDisplayed = verificationObjectPage.isIpGeolocationDisplayed();
        log.info("Ip Geolocation is displayed: {}", isIpGeolocationDisplayed);
        return isIpGeolocationDisplayed;
    }

    public VerificationObjectHelper clickCertifiedTimestampButton() {
        log.info("Click CertifiedTimestamp Button");
        verificationObjectPage.clickCertifiedTimestampButton();
        return this;
    }

    public String getUniqueId() {
        String uniqueId = verificationObjectPage.getUniqueId();
        log.info("Unique id: {}", uniqueId);
        return uniqueId;
    }

    public VerificationObjectHelper clickOnCopyUniqueIdButton() {
        log.info("Click On CopyUniqueId Button");
        verificationObjectPage.clickOnCopyUniqueIdButton();
        return this;
    }

    public boolean isCertifiedTimestampDisplayed() {
        boolean isCertifiedTimestampDisplayed = verificationObjectPage.isCertifiedTimestampDisplayed();
        log.info("CertifiedTimestamp is displayed : {}", isCertifiedTimestampDisplayed);
        return isCertifiedTimestampDisplayed;
    }

    public boolean isUniqueIdDisplayed() {
        boolean isUniqueIdDisplayed = verificationObjectPage.isUniqueIdDisplayed();
        log.info("UniqueId is displayed {}", isUniqueIdDisplayed);
        return isUniqueIdDisplayed;
    }

    public boolean isImgMapDisplayed() {
        boolean isImgMapDisplayed = verificationObjectPage.isImgMapDisplayed();
        log.info("Is Image Map displayed: {}", isImgMapDisplayed);
        return isImgMapDisplayed;
    }

    public boolean isCountryDisplayed() {
        boolean isCountryDisplayed = verificationObjectPage.isCountryDisplayed();
        log.info("Is country name displayed: {}", isCountryDisplayed);
        return isCountryDisplayed;
    }

    public boolean isProvinceDisplayed() {
        boolean isProvinceDisplayed = verificationObjectPage.isProvinceDisplayed();
        log.info("Is province displayed: {}", isProvinceDisplayed);
        return isProvinceDisplayed;
    }

    public boolean isCityDisplayed() {
        boolean isCityDisplayed = verificationObjectPage.isCityDisplayed();
        log.info("Is city displayed: {}", isCityDisplayed);
        return isCityDisplayed;
    }

    public boolean isZipCodeDisplayed() {
        boolean isZipCodeDisplayed = verificationObjectPage.isZipCodeDisplayed();
        log.info("Is ZipCode displayed: {}", isZipCodeDisplayed);
        return isZipCodeDisplayed;
    }

    public boolean isVpnDisplayed() {
        boolean isVpnDisplayed = verificationObjectPage.isVpnDisplayed();
        log.info("Is Vpn displayed: {}", isVpnDisplayed);
        return isVpnDisplayed;
    }

    public boolean isGeoRestrictionsDisplayed() {
        boolean isGeoRestrictionsDisplayed = verificationObjectPage.isGeoRestrictionsDisplayed();
        log.info("Is GeoRestrictions displayed: {}", isGeoRestrictionsDisplayed);
        return isGeoRestrictionsDisplayed;
    }

    public VerificationObjectHelper clickOnFrontDocumentImage() {
        log.info("Click on front document image");
        verificationObjectPage.clickOnFrontDocumentImage();
        return this;
    }

    public VerificationObjectHelper clickOnBackDocumentImage() {
        log.info("Click on back document image");
        verificationObjectPage.clickOnBackDocumentImage();
        return this;
    }

    public void openZoomedImageInNewTab() {
        log.info("Open Zoomed Image in new Tab");
        verificationObjectPage.openZoomedImageInNewTab();
    }

    public boolean isImageInNewTabDisplayed() {
        boolean isImageInNewTabDisplayed = verificationObjectPage.isImageInNewTabDisplayed();
        log.info("Image in new tab is displayed: {}", isImageInNewTabDisplayed);
        return isImageInNewTabDisplayed;
    }

    public VerificationObjectHelper clickPhoneCheckButton() {
        log.info("Click PhoneCheck button");
        verificationObjectPage.clickPhoneCheckButton();
        return this;
    }

    public boolean isCountryCodeInValidationsWindowDisplayed() {
        boolean isCountryCodeInValidationsWindowDisplayed = verificationObjectPage.isCountryCodeInValidationsWindowDisplayed();
        log.info("CountryCode In Validations Window is Displayed: {}", isCountryCodeInValidationsWindowDisplayed);
        return isCountryCodeInValidationsWindowDisplayed;
    }

    public boolean isStatusDisplayed() {
        boolean isStatusDisplayed = verificationObjectPage.isStatusDisplayed();
        log.info("Status is displayed: {}", isStatusDisplayed);
        return isStatusDisplayed;
    }

    public boolean isCellphoneNumberInValidationsWindowDisplayed() {
        boolean isCellphoneNumberInValidationsWindowDisplayed = verificationObjectPage.isCellphoneNumberInValidationsWindowDisplayed();
        log.info("CellphoneNumber In Validations Window Displayed: {}", isCellphoneNumberInValidationsWindowDisplayed);
        return isCellphoneNumberInValidationsWindowDisplayed;
    }

    public boolean isCountryCodeInRiskWindowDisplayed() {
        boolean isCountryCodeInRiskWindowDisplayed = verificationObjectPage.isCountryCodeInRiskWindowDisplayed();
        log.info("CountryCode In Risk Window Displayed: {}", isCountryCodeInRiskWindowDisplayed);
        return isCountryCodeInRiskWindowDisplayed;
    }

    public boolean isCellphoneNumberInRiskWindowDisplayed() {
        boolean isCellphoneNumberInRiskWindowDisplayed = verificationObjectPage.isCellphoneNumberInRiskWindowDisplayed();
        log.info("Cellphone Number In Risk Window Displayed: {}", isCellphoneNumberInRiskWindowDisplayed);
        return isCellphoneNumberInRiskWindowDisplayed;
    }

    public boolean isRiskLevelDisplayed() {
        boolean isRiskLevelDisplayed = verificationObjectPage.isRiskLevelDisplayed();
        log.info("Risk Level Displayed: {}", isRiskLevelDisplayed);
        return isRiskLevelDisplayed;
    }

    public boolean isRiskScoreDisplayed() {
        boolean isRiskScoreDisplayed = verificationObjectPage.isRiskScoreDisplayed();
        log.info("Risk Score Displayed: {}", isRiskScoreDisplayed);
        return isRiskScoreDisplayed;
    }

    public boolean isTimeZoneDisplayed() {
        boolean isTimeZoneDisplayed = verificationObjectPage.isTimeZoneDisplayed();
        log.info("TimeZone Displayed: {}", isTimeZoneDisplayed);
        return isTimeZoneDisplayed;
    }

    public boolean isLineTypeDisplayed() {
        boolean isLineTypeDisplayed = verificationObjectPage.isLineTypeDisplayed();
        log.info("LineType Displayed: {}", isLineTypeDisplayed);
        return isLineTypeDisplayed;
    }

    public boolean isCarrierDisplayed() {
        boolean isCarrierDisplayed = verificationObjectPage.isCarrierDisplayed();
        log.info("Carrier Displayed: {}", isCarrierDisplayed);
        return isCarrierDisplayed;
    }
}
