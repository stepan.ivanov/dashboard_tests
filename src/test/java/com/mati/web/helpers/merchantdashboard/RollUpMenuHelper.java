package com.mati.web.helpers.merchantdashboard;

import com.mati.enums.rollup.Language;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.RollUpMenuPage;

import java.util.List;

public class RollUpMenuHelper extends AbstractHelper {

    private RollUpMenuPage rollUpMenuPage = new RollUpMenuPage();
    private AnalyticsHelper analyticsHelper = new AnalyticsHelper();
    private VerificationListHelper verificationListHelper = new VerificationListHelper();
    private VerificationFlowsHelper verificationFlowsHelper = new VerificationFlowsHelper();
    private InvitationPageHelper invitationPageHelper = new InvitationPageHelper();

    public boolean isLanguageButtonDisplayed() {
        boolean result = rollUpMenuPage.isLanguageButtonDisplayed();
        log.info("Check if 'Language' button is displayed - " + result);
        return result;
    }

    public RollUpMenuHelper clickOnLanguageButton() {
        log.info("Click on 'Language' button");
        rollUpMenuPage.clickOnLanguageButton();
        return this;
    }

    public RollUpMenuHelper selectLanguage(Language language) {
        log.info("Select 'Language' "+ language);
        rollUpMenuPage.selectLanguage(language);
        return this;
    }

    public RollUpMenuHelper clickLogoutButton() {
        log.info("Click 'Logout' button");
        rollUpMenuPage.clickLogoutButton();
        return this;
    }

    public void clickConfirmPopUpButton() {
        log.info("Click 'Quite' button on confirm popup");
        rollUpMenuPage.clickConfirmPopUpButton();
    }

    public List<String> getLanguagesList() {
        List<String> result = rollUpMenuPage.getLanguagesList();
        log.info("Get list of 'Languages' - " + result);
        return result;
    }

    public String getRollUpMenuElementText() {
        String result = rollUpMenuPage.getRollUpMenuElementText();
        log.info("Get 'RollUp Menu' element text - " + result);
        return result;
    }

    public String getAnalyticsElementText() {
        String result = rollUpMenuPage.getAnalyticsElementText();
        log.info("Get 'Analytics' element text - " + result);
        return result;
    }

    public String getVerificationListElementText() {
        String result = rollUpMenuPage.getVerificationListElementText();
        log.info("Get 'Verification List' element text - " + result);
        return result;
    }

    public String getVerificationFlowsElementText() {
        String result = rollUpMenuPage.getVerificationFlowsElementText();
        log.info("Get 'Verification Flow' element text - " + result);
        return result;
    }

    public String getForDevelopersElementText() {
        String result = rollUpMenuPage.getForDevelopersElementText();
        log.info("Get 'For Developers' element text - " + result);
        return result;
    }

    public String getFAQElementText() {
        String result = rollUpMenuPage.getFAQElementText();
        log.info("Get 'FAQ' element text - " + result);
        return result;
    }

    public String getWhatsNewElementText() {
        String result = rollUpMenuPage.getWhatsNewElementText();
        log.info("Get 'What's new' element text - " + result);
        return result;
    }

    public String getInviteTeammateElementText() {
        String result = rollUpMenuPage.getInviteTeammateElementText();
        log.info("Get 'Invite Teammate' element text - " + result);
        return result;
    }

    public String getSettingsElementText() {
        String result = rollUpMenuPage.getSettingsElementText();
        log.info("Get 'Settings' element text - " + result);
        return result;
    }

    public String getLogoutElementText() {
        String result = rollUpMenuPage.getLogoutElementText();
        log.info("Get 'Logout' element text - " + result);
        return result;
    }
    public AnalyticsHelper selectAnalyticsMenu() {
        rollUpMenuPage.selectAnalyticsMenu();
        log.info("Select 'analytics menu'");
        return analyticsHelper;
    }

    public VerificationListHelper selectVerificationListMenu() {
        rollUpMenuPage.selectVerificationListMenu();
        log.info("Select 'verification list' menu");
        return verificationListHelper;
    }

    public VerificationFlowsHelper selectVerificationFlowsMenu() {
        rollUpMenuPage.selectVerificationFlowsMenu();
        log.info("Select 'verification flows' menu");
        return verificationFlowsHelper;
    }

    public InvitationPageHelper clickOnInviteTeamButton(){
        rollUpMenuPage.clickOnInviteTeamButton();
        log.info("Click on 'Invite a teammate' button");
        return invitationPageHelper;
    }
}
