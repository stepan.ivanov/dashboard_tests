package com.mati.web.helpers.merchantdashboard;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.DevPage;

public class DevHelper extends AbstractHelper {

    private WebHookMenuHelper webHookMenuHelper = new WebHookMenuHelper();
    private DevPage devPage = new DevPage();


    public DevHelper navigateToPage() {
        log.info("Open Dev page");
        devPage.navigateToPage();
        return this;
    }

    public WebHookMenuHelper openWebHooksMenu() {
        log.info("Click on WebHook button");
        devPage.clickOnWebHookButton();
        webHookMenuHelper.waitUntilPageIsOpening();
        return webHookMenuHelper;
    }
}
