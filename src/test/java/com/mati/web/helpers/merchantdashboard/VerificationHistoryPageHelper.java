package com.mati.web.helpers.merchantdashboard;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.VerificationHistoryPage;

import java.util.List;

public class VerificationHistoryPageHelper extends AbstractHelper {

    private VerificationHistoryPage verificationHistoryPage = new VerificationHistoryPage();

    public VerificationHistoryPageHelper waitPageLoaded() {
        log.info("Wait until page is loading...");
        verificationHistoryPage.waitPageIsLoaded();
        return this;
    }

    public List<String> getListHistoryActions() {
        log.info("Get list history actions");
        return verificationHistoryPage.getListHistoryActions();
    }
}
