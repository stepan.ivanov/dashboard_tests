package com.mati.web.helpers.merchantdashboard;

import com.mati.enums.Role;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.InvitationPage;

public class InvitationPageHelper extends AbstractHelper {

    private InvitationPage invitationPage = new InvitationPage();

    public InvitationPageHelper fillEmailField(String email) {
        invitationPage.fillEmailInput(email);
        log.info("Fill 'email' field with - " + email);
        return this;
    }

    public InvitationPageHelper fillFirstNameInput(String firstName) {
        invitationPage.fillFirstNameInput(firstName);
        log.info("Fill 'First Name' field with - " + firstName);
        return this;
    }

    public InvitationPageHelper fillLastNameInput(String lastName) {
        invitationPage.fillLastNameInput(lastName);
        log.info("Fill 'Last Name' field with - " + lastName);
        return this;
    }

    public void clickOnSendButton() {
        log.info("Click on Send button");
        invitationPage.clickOnButtonSend();
    }

    public boolean isErrorMessageDisplayed(String message) {
        boolean isErrorMessageDisplayed = invitationPage.isErrorMessageDisplayed(message);
        log.info("Error message is displayed: " + isErrorMessageDisplayed);
        return isErrorMessageDisplayed;
    }

    public boolean isFirstNameFieldDisplayed() {
        boolean isDisplayed = invitationPage.isFirstNameFieldDisplayed();
        log.info("FirstName field is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public boolean isLastNameFieldDisplayed() {
        boolean isDisplayed = invitationPage.isLastNameFieldDisplayed();
        log.info("LastName field is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public boolean isEmailDisplayed() {
        boolean isDisplayed = invitationPage.isEmailFieldDisplayed();
        log.info("Email field is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public boolean isSendButtonDisplayed() {
        boolean isDisplayed = invitationPage.isSendButtonDisplayed();
        log.info("Send button is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public boolean isCancelButtonDisplayed() {
        boolean isDisplayed = invitationPage.isCancelButtonDisplayed();
        log.info("Cancel button is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public boolean isAdminRadioButtonDisplayed() {
        boolean isDisplayed = invitationPage.isAdminRoleRadioButtonDisplayed();
        log.info("Admin radio button is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public boolean isAgentRadioButtonDisplayed() {
        boolean isDisplayed = invitationPage.isAgentRoleRadioButtonDisplayed();
        log.info("Agent radio button is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public boolean isInvitationScreenDisplayed() {
        boolean isDisplayed = invitationPage.isInvitationScreenDisplayed();
        log.info("Invitation screen is displayed: " + isDisplayed);
        return isDisplayed;
    }

    public InvitationPageHelper setRole(Role role) {
        log.info(String.format("Set %s role for new member", role.name()));
        invitationPage.setRole(role);
        return this;
    }

    public SettingsPageHelper inviteNewTeamMember(String firstName, String lastName, String email, Role role) {
        fillFirstNameInput(firstName);
        fillLastNameInput(lastName);
        fillEmailField(email);
        setRole(role);
        clickOnSendButton();
        return new SettingsPageHelper();
    }

    public boolean isRoleChecked(Role role) {
        boolean isChecked = invitationPage.isRoleChecked(role);
        log.info("Role {} is checked: {}", role.name(), isChecked);
        return isChecked;
    }
}
