package com.mati.web.helpers.merchantdashboard;

import com.mati.enums.Period;
import com.mati.enums.verificationlist.VerificationStatus;
import com.mati.utils.WaitUtils;
import com.mati.web.IConstants;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.VerificationsListPage;

import java.time.LocalDate;
import java.util.List;

public class VerificationListHelper extends AbstractHelper {

    private VerificationsListPage verificationsListPage = new VerificationsListPage();

    public boolean isReviewModeButtonEnabled() {
        boolean result = verificationsListPage.isReviewModeButtonEnabled();
        log.info("Check if 'Review mode' button is enabled - " + result);
        return result;
    }

    public boolean isReviewModeButtonBlack() {
        boolean result = verificationsListPage.isReviewModeButtonBlack();
        log.info("Check if 'Review mode' button is black - " + result);
        return result;
    }

    public void clickReviewModeButton() {
        verificationsListPage.clickReviewModeButton();
        log.info("Click 'Review mode' button");
    }

    public VerificationObjectHelper selectVerificationByName(String name) {
        verificationsListPage.selectVerificationByName(name);
        log.info("Select verification by name - " + name);
        return new VerificationObjectHelper();
    }

    public VerificationListHelper waitForPageLoad() {
        verificationsListPage.waitForPageLoad();
        log.info("Wait for 'Verification List' page will be loaded");
        return this;
    }

    public void waitToBeInvisibilityDashboardLoader() {
        verificationsListPage.waitToBeInvisibilityDashboardLoader();
        log.info("Wait to start loading of verification PDF file");
    }

    public void deleteVerificationByName(String name) {
        log.info("Delete verification by name - " + name);
        verificationsListPage.deleteVerificationByName(name);
//      Need some time to send request
        WaitUtils.sleepSeconds(5);
    }

    public boolean isVerificationExist(String name) {
        boolean result = verificationsListPage.isVerificationExist(name);
        log.info("Check if verification by name '" + name + "' is exist - " + result);
        return result;
    }

    public void downloadVerificationCSV() {
        log.info("Download verification CSV file to '" + IConstants.currentFolder + "' folder");
        verificationsListPage.downloadVerificationCSV();
    }

    public void selectOldVerification() {
        log.info("Select old verification");
        verificationsListPage.selectOldVerification();
    }

    public void selectNewVerification() {
        verificationsListPage.selectNewVerification();
        log.info("Select new verification");
    }

    public String getURL() {
        String result = verificationsListPage.getURL();
        log.info("Get verification list page url - " + result);
        return result;
    }

    public VerificationListHelper navigateToPage() {
        verificationsListPage.navigateToPage();
        log.info("Navigate to verification list page");
        verificationsListPage.waitForPageLoad();
        return this;
    }

    public VerificationListHelper selectSortDescendingDate() {
        log.info("Select verification sorting by descending date");
        verificationsListPage.selectSortDescendingDate();
        WaitUtils.sleepSeconds(5);
        return this;
    }

    public VerificationObjectHelper selectVerificationByStatus(VerificationStatus status) {
        log.info("Select verification by status - " + status.getStatus());
        verificationsListPage.selectVerificationByStatus(status);
        waitToBeInvisibilityDashboardLoader();
        return new VerificationObjectHelper();
    }

    public VerificationListHelper openFilter() {
        log.info("Open filter wizard");
        verificationsListPage.openFilter();
        return this;
    }

    public boolean isVerificationListHaveCorrectStatus(VerificationStatus status) {
        boolean result = verificationsListPage.getVerificationStatuses().stream().allMatch(s -> s.equals(status.getStatus()));
        log.info("Check if all verification have '" + status.getStatus() + "' status - " + result);
        return result;
    }

    public List<String> getVerificationStatuses() {
        log.info("Get verification statuses");
        return verificationsListPage.getVerificationStatuses();
    }

    public VerificationListHelper clearFilter() {
        verificationsListPage.clearFilter();
        log.info("Click clear filter button");
        return this;
    }

    public VerificationListHelper scrollToStatus(VerificationStatus status) {
        verificationsListPage.scrollToStatus(status);
        log.info("Scroll to '" + status.getStatus() + "' status");
        return this;
    }

    public VerificationListHelper selectFilterByStatus(VerificationStatus status) {
        verificationsListPage.selectFilterByStatus(status);
        log.info("Select filter by status - " + status);
        return this;
    }

    public void clickApplyFilterButton() {
        log.info("Click apply filter button");
        verificationsListPage.clickApplyFilterButton();
        waitForPageLoad();
    }

    public VerificationListHelper scrollToFlow(String flowName) {
        verificationsListPage.scrollToFlow(flowName);
        log.info("Scroll to '" + flowName + "' flow");
        return this;
    }

    public VerificationListHelper selectFilterByFlow(String flowName) {
        verificationsListPage.selectFilterByFlow(flowName);
        log.info("Select filter by flow - " + flowName);
        return this;
    }

    public boolean isVerificationListHaveCorrectFlow(String flowName) {
        boolean result = getVerificationFlows().stream().allMatch(s -> s.equals(flowName));
        log.info("Check if all verification have '" + flowName + "' flow - " + result);
        return result;
    }

    public List<String> getVerificationFlows() {
        log.info("Get verification flows from verification list");
        return verificationsListPage.getVerificationFlows();
    }

    public VerificationListHelper selectFilterByPeriod(Period period) {
        verificationsListPage.selectFilterByPeriod(period);
        log.info("Select filter by period - " + period);
        return this;
    }

    public List<LocalDate> getFilterDates() {
        List<LocalDate> list = verificationsListPage.getFilterDates();
        log.info("Get period from " + list.get(0) + " to " + list.get(1));
        return list;
    }

    public boolean isVerificationListHaveCorrectPeriod(List<LocalDate> value) {
        LocalDate from = value.get(0);
        LocalDate to = value.get(1);
        boolean result = verificationsListPage.getVerificationDates().stream().allMatch(d ->
                !(d.isAfter(to) & d.isBefore(from)));
        log.info("Check if all verifications are filtered by period from " + from + " to " + to + " - " + result);
        return result;
    }

    public void closeAlert() {
        verificationsListPage.closeAlert();
        log.info("Close alert");
    }

    public VerificationObjectHelper selectFirstVerification() {
        verificationsListPage.selectFirstVerification();
        log.info("Select first verification");
        return new VerificationObjectHelper();
    }

    public boolean isFilterByPeriodDisplayed(String period) {
        boolean isFilterByPeriodDisplayed = verificationsListPage.isFilterByPeriodDisplayed(period);
        log.info("Filter by period displayed: {}", isFilterByPeriodDisplayed);
        return isFilterByPeriodDisplayed;
    }

    public String getFilterByFlowLabel() {
        String filterByFlowLabel = verificationsListPage.getFilterByFlowLabel();
        log.info("Filter by flow label: {}", filterByFlowLabel);
        return filterByFlowLabel;
    }

    public String getFilterByStatusLabel() {
        String filterByStatusLabel = verificationsListPage.getFilterByStatusLabel();
        log.info("Filter by status label: {}", filterByStatusLabel);
        return filterByStatusLabel;
    }

    public boolean isCsvLoadingSpinnerDisplayed() {
        boolean isCsvLoadingSpinnerDisplayed = verificationsListPage.isCsvLoadingSpinnerDisplayed();
        log.info("CSV loading spinner is displayed: {}", isCsvLoadingSpinnerDisplayed);
        return isCsvLoadingSpinnerDisplayed;
    }

    public void waitUntilCsvLoadingSpinnerDisappear() {
        log.info("Wait until CSV loading spinner will be disappeared");
        verificationsListPage.waitUntilCsvLoadingSpinnerDisappear();
    }

    public boolean isDownloadCsvButtonEnabled() {
        boolean isDownloadCsvButtonEnabled = verificationsListPage.isDownloadCsvButtonEnabled();
        log.info("Download CSV button enabled: {}", isDownloadCsvButtonEnabled);
        return isDownloadCsvButtonEnabled;
    }

    public boolean isAlertPresent() {
        boolean isAlertPresent = verificationsListPage.isAlertPresent();
        log.info("Is alert present: {}", isAlertPresent);
        return isAlertPresent;
    }
}
