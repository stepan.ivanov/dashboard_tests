package com.mati.web.helpers.merchantdashboard;

import com.mati.enums.verificationflows.Products;
import com.mati.enums.verificationflows.VerificationInfoByCountry;
import com.mati.enums.verificationflows.settings.FlowSettings;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.merchantdashboard.FlowPage;

import java.util.List;

public class FlowHelper extends AbstractHelper {

    private FlowPage flowPage = new FlowPage();

    public void createVerification(VerificationInfoByCountry verificationInfoByCountry, String frontFileName, String backFileName) {
        log.info("Create verification");
        flowPage.createVerification(verificationInfoByCountry, frontFileName, backFileName);
    }

    public void createVerification(VerificationInfoByCountry verificationInfoByCountry) {
        log.info("Create verification");
        flowPage.createVerification(verificationInfoByCountry);
    }

    public void createPhoneCheckVerification(VerificationInfoByCountry verificationInfoByCountry, String phoneNumber, boolean skip) {
        log.info("Create verification");
        flowPage.createPhoneCheckVerification(verificationInfoByCountry,phoneNumber, skip);
    }

    public void createVerificationWithCountryRestriction(VerificationInfoByCountry verificationInfoByCountry, String frontFileName, String backFileName) {
        log.info("Create verification");
        flowPage.createVerificationWithCountryRestriction(verificationInfoByCountry, frontFileName, backFileName);
    }

    public FlowHelper clickVerifyMeButton() {
        log.info("Click 'Verify Me' button");
        flowPage.clickVerifyMeButton();
        return this;
    }

    public boolean isVerificationWizardDisplayed() {
        boolean result = flowPage.isVerificationWizardDisplayed();
        log.info("Check if verification wizard displayed - " + result);
        return result;
    }

    public boolean isNotProductsResizable() {
        boolean result = flowPage.isNotProductsResizable();
        log.info("Check if the product list is not resizable - " + result);
        return result;
    }

    public boolean isProductListScrollable() {
        boolean result = flowPage.isProductListScrollable();
        log.info("Check if the product list is scrollable - " + result);
        return result;
    }

    public FlowHelper moveProductToFlowBuilder(Products productName) {
        log.info("Move '" + productName + "' product to flow builder");
        flowPage.moveProductToFlowBuilder(productName);
        return this;
    }

    public boolean isProductMovedToFlowBuilder(Products productName) {
        boolean result = flowPage.isProductInFlowBuilder(productName);
        log.info("Check if '" + productName + "' product moved to flow builder - " + result);
        return result;
    }

    public FlowHelper clickSaveAndPublishButton() {
        log.info("Click 'Save and publish' button");
        flowPage.clickSaveAndPublishButton();
        return this;
    }

    public FlowHelper waitFlowWizardToBeLoaded() {
        log.info("Wait for flow wizard to be loaded");
        flowPage.waitSpinnerToBeInvisibility();
        return this;
    }

    public boolean isFlowBuilderScrollable() {
        boolean result = flowPage.isFlowBuilderScrollable();
        log.info("Check if flow builder is scrollable - " + result);
        return result;
    }

    public FlowHelper selectApiIntegration() {
        log.info("Select 'API' integration");
        flowPage.selectApiIntegration();
        return this;
    }

    public FlowHelper selectSdkIntegration() {
        log.info("Select 'SDK' integration");
        flowPage.selectSdkIntegration();
        return this;
    }

    public boolean isDraggableOnlyProducts() {
        boolean result = flowPage.isDraggableOnlyProducts();
        log.info("Check if only products can be drag to flow builder - " + result);
        return result;
    }

    public boolean isProductsAlignedVertically() {
        boolean result = flowPage.isProductsAlignedVertically();
        log.info("Check if all products are aligned vertically - " + result);
        return result;
    }

    public boolean isProductExpandable() {
        boolean result = flowPage.isProductsExpandable();
        log.info("Check if product is expandable - " + result);
        return result;
    }

    public boolean isProductInRightPosition(Products product, int position) {
        boolean result = flowPage.isProductInRightPosition(product, position);
        log.info("Check if '" + product + "' product is in right position in flow builder - " + result);
        return result;
    }

    public FlowHelper moveAllProductsToFlowBuilder() {
        log.info("Move all products to flow builder");
        flowPage.moveAllProductsToFlowBuilder();
        return this;
    }

    public FlowHelper deleteProductsFromFlowBuilder() {
        log.info("Delete all products from flow builder");
        flowPage.deleteProductsFromFlowBuilder();
        return this;
    }

    public FlowHelper deleteProductFromFlowBuilder(Products product) {
        log.info("Delete '" + product.getProduct() + "' product from flow builder");
        flowPage.deleteProductFromFlowBuilder(product);
        return this;
    }

    public boolean isProductInProductsList(Products productName) {
        boolean result = flowPage.isProductInProductsList(productName);
        log.info("Check if '" + productName + "' product deleted from flow builder - " + result);
        return result;
    }

    public boolean isProductTitleDisplayed() {
        boolean result = flowPage.isProductTitleDisplayed();
        log.info("Check if the product title is displayed - " + result);
        return result;
    }

    public boolean isUserInputsDisplayed() {
        boolean result = flowPage.isUserInputsDisplayed();
        log.info("Check if user inputs are displayed - " + result);
        return result;
    }

    public boolean isProductIconDisplayed() {
        boolean result = flowPage.isProductIconDisplayed();
        log.info("Check if the product icon is displayed - " + result);
        return result;
    }

    public boolean isSDKOnlyBadgeDisplayed() {
        boolean result = flowPage.isSDKOnlyBadgeDisplayed();
        log.info("Check if the 'SDK only' badge is displayed - " + result);
        return result;
    }

    public boolean isModalWindowDisplayed() {
        boolean result = flowPage.isModalWindowDisplayed();
        log.info("Check if modal window is displayed - " + result);
        return result;
    }

    public boolean isProductSettingsDisplayed() {
        boolean result = flowPage.isProductSettingsDisplayed();
        log.info("Check if product settings is displayed - " + result);
        return result;
    }

    public FlowHelper closeProductSettings() {
        log.info("Close 'Product settings' panel");
        flowPage.closeProductSettings();
        return this;
    }

    public boolean isDropZoneAlwaysRemainsAtTheBottom() {
        boolean result = false;
        for (Products p : Products.values()) {
            moveProductToFlowBuilder(p);
            result = flowPage.isDropZoneAtBottom();
            if (result == false) return false;
        }
        log.info("Check if 'Drop Zone' always remains at the bottom in flow builder - " + result);
        return result;
    }

    public FlowHelper clickFlowSettingsButton() {
        log.info("Open flow settings pop up");
        flowPage.clickFlowSettingsButton();
        return this;
    }

    public boolean isFlowNamePopUpDisplayed() {
        boolean result = flowPage.isFlowNamePopUpDisplayed();
        log.info("Check if flow name is displayed in flow settings pop up - " + result);
        return result;
    }

    public boolean isFlowIdPopUpDisplayed() {
        boolean result = flowPage.isFlowIdPopUpDisplayed();
        log.info("Check if flow id is displayed in flow settings pop up - " + result);
        return result;
    }

    public boolean isDateCreationFlowPopUpDisplayed() {
        boolean result = flowPage.isDateCreationFlowPopUpDisplayed();
        log.info("Check if date creation of flow is displayed in flow settings pop up - " + result);
        return result;
    }

    public boolean isListsFlowChecksDisplayed() {
        boolean result = flowPage.isFlowChecksPopUpDisplayed();
        log.info("Check if lists flow's checks is displayed in flow settings pop up - " + result);
        return result;
    }

    public boolean isCDPRCheckBoxSwitchable() {
        boolean result = flowPage.isCDPRCheckBoxSwitchable();
        log.info("Check if CDPR setting is switchable - " + result);
        return result;
    }

    public FlowHelper selectGDPRCheckBox(boolean flag) {
        log.info("Select GDPR settings - " + flag);
        flowPage.selectGDPRCheckBox(flag);
        return this;
    }

    public boolean isPoliceDaysEnabled() {
        boolean result = flowPage.isPoliceDaysEnabled();
        log.info("Check if policy days is enabled - " + result);
        return result;
    }

    public boolean isTimestampCheckBoxSwitchable() {
        boolean result = flowPage.isTimestampCheckBoxSwitchable();
        log.info("Check if Timestamp setting is switchable - " + result);
        return result;
    }

    public FlowHelper fillPolicyDays(String value) {
        log.info("Input to Policy days - " + value);
        flowPage.fillPolicyDays(value);
        return this;
    }

    public boolean isUnsavedChangesAlertPopupDisplayed() {
        boolean result = flowPage.isUnsavedChangesAlertPopupDisplayed();
        log.info("Check if alert message is displayed for unsaved changes in flow settings popup - " + result);
        return result;
    }

    public boolean isUnsavedChangesAlertDisplayed() {
        boolean result = flowPage.isUnsavedChangesAlertDisplayed();
        log.info("Check if alert message is displayed for unsaved changes - " + result);
        return result;
    }

    public FlowHelper closeModalWindow() {
        log.info("Close modal window");
        flowPage.closeModalWindow();
        return this;
    }

    public FlowHelper selectTimestampCheckBox(boolean flag) {
        log.info("Select Timestamp settings - " + flag);
        flowPage.selectTimestampCheckBox(flag);
        return this;
    }

    public boolean isFlowSettingsChangesSaved() {
        boolean result = isCDPRCheckBoxSelected()
                && isTimestampCheckBoxSelected()
                && isPolicyDaysFilled();
        return result;
    }

    public boolean isCDPRCheckBoxSelected() {
        boolean result = flowPage.isCDPRCheckBoxSelected();
        log.info("Check if CDPR setting is selected - " + result);
        return result;
    }

    public boolean isTimestampCheckBoxSelected() {
        boolean result = flowPage.isTimestampCheckBoxSelected();
        log.info("Check if Timestamp setting is selected - " + result);
        return result;
    }

    public boolean isPolicyDaysFilled() {
        boolean result = flowPage.isPolicyDaysFilled();
        log.info("Check if policy days are filled - " + result);
        return result;
    }

    public boolean isConfirmationPopupDisplayed() {
        boolean result = flowPage.isConfirmationPopupDisplayed();
        log.info("Check if confirmation popup is displayed - " + result);
        return result;
    }

    public FlowHelper clickDeleteFlowButton() {
        log.info("Click 'Delete' flow button");
        flowPage.clickDeleteFlowButton();
        return this;
    }

    public FlowHelper clickSaveChangesButton() {
        log.info("Click 'Save changes' button");
        flowPage.clickSaveChangesButton();
        return this;
    }

    public FlowHelper clickCancelDeletingButton() {
        log.info("Click 'Cancel' button");
        flowPage.clickCancelDeletingButton();
        return this;
    }

    public boolean isFlowSettingsPopupDisplayed() {
        boolean result = flowPage.isFlowSettingsPopupDisplayed();
        log.info("Check if flow settings popup is displayed - " + result);
        return result;
    }

    public void clickConfirmDeleteFlowButton() {
        log.info("Click 'confirm delete' flow button");
        flowPage.clickConfirmDeleteButton();
    }

    public boolean isSaveAndPublishButtonEnabled() {
        boolean result = flowPage.isSaveAndPublishButtonEnabled();
        log.info("Check if 'Save and publish' button is enabled - " + result);
        return result;
    }

    public void refreshFlowPage() {
        log.info("Refresh flow page");
        flowPage.refreshPage();
    }

    public boolean isAlertPopupDisplayed() {
        boolean result = flowPage.getAlertPopup() != null;
        log.info("Check if alert popup is displayed - " + result);
        return result;
    }

    public boolean isIssuesMessageDisplayed() {
        boolean result = flowPage.isIssuesMessageDisplayed();
        log.info("Check if 'You have issues' message is displayed - " + result);
        return result;
    }

    public void closeAlertPopup() {
        log.info("close alert popup");
        flowPage.clickCancelButtonInAlertPopup();
    }

    public boolean isProductCheckDisplayed(List<String> checks) {
        boolean result = checks.stream().allMatch(c -> {
            boolean isDisplayed = flowPage.isProductCheckDisplayed(c);
            log.info("Check if '" + c + "' check is diplayed - " + isDisplayed);
            return isDisplayed;
        });
        log.info("Check if checks are displayed - " + result);
        return result;
    }

    public boolean isProductSettingDisplayed(FlowSettings setting) {
        boolean result = flowPage.isProductSettingDisplayed(setting);
        log.info("Check if '" + setting + "' setting is displayed - " + result);
        return result;
    }

    public boolean isProductSettingOpened(Products setting) {
        boolean result = flowPage.isProductSettingOpened(setting);
        log.info("Check if '" + setting + "' setting is opened - " + result);
        return result;
    }

    public boolean isProductSettingCheckBoxSwitchable(FlowSettings setting) {
        boolean result = flowPage.isSettingCheckBoxSwitchable(setting);
        log.info("Check if '" + setting + "' setting checkbox is switchable - " + result);
        return result;
    }

    public FlowHelper clickProductInFlowBuilder(Products product) {
        log.info("Click " + product + " product and open its settings");
        flowPage.clickProductInFlowBuilder(product);
        return this;
    }

    public FlowHelper selectSettingCheckbox(FlowSettings setting, boolean flag) {
        log.info("Select '" + setting + "' setting - " + flag);
        flowPage.selectSettingCheckbox(setting, flag);
        return this;
    }

    public FlowHelper selectSettingCheckboxes(FlowSettings setting, boolean flag) {
        log.info("Select '" + setting + "' setting - " + flag);
        flowPage.selectSettingCheckboxes(setting, flag);
        return this;
    }

    public boolean isCheckBoxSelected(FlowSettings setting) {
        boolean result = flowPage.isCheckBoxSelected(setting);
        log.info("Check if '" + setting + "' checkbox is selected - " + result);
        return result;
    }

    public boolean isCheckBoxesSetDefault(FlowSettings setting) {
        boolean result = flowPage.isCheckBoxesSetDefault(setting);
        log.info("Check if '" + setting + "' check boxes set default - " + result);
        return result;
    }

    public FlowHelper confirmAddingProductToFlowBuilder() {
        flowPage.confirmAddingProductToFlowBuilder();
        log.info("Confirm adding product to Flow builder");
        return this;
    }

    public boolean isFlowNameDisplayed() {
        boolean result = flowPage.isFlowNameDisplayed();
        log.info("Check if flow name is displayed - " + result);
        return result;
    }

    public boolean isFlowCreationDateDisplayed() {
        boolean result = flowPage.isFlowCreationDateDisplayed();
        log.info("Check if flow creation date is displayed - " + result);
        return result;
    }

    public boolean isFlowNameNotEditable() {
        boolean result = flowPage.isFlowNameNotEditable();
        log.info("Check if flow name is not editable - " + result);
        return result;
    }

    public void clickBackArrowButton() {
        log.info("CLick back arrow button");
        flowPage.clickBackArrowButton();
    }

    public String getFlowPageUrl(String flowId) {
        String url = flowPage.getPageUrl(flowId);
        log.info("Get flow page url - " + url);
        return url;
    }

    public void switchToNewDesign() {
        log.info("Switch to new design pages");
        flowPage.switchToNewDesign();
    }

    public void closeDependencyWizard() {
        log.info("Close product dependency wizard");
        flowPage.closeDependencyWizard();
    }

    public FlowHelper navigateToFlow(String id) {
        log.info("Navigate to flow by id - " + id);
        flowPage.navigate(id);
        return this;
    }

    public boolean isProductSettingSelective(FlowSettings setting) {
        boolean result = flowPage.isProductSettingSelective(setting);
        log.info("Check if '" + setting + "' setting is selective - " + result);
        return result;
    }

    public boolean isWarningWithContentDisplayed(String text) {
        boolean result = flowPage.isWarningWithContentDisplayed(text);
        log.info("Check if warning is displayed with content '" + text + "' - " + result);
        return result;
    }

    public FlowHelper clickEditFlowNameButton() {
        log.info("Click edit flow name button");
        flowPage.clickEditFlowNameButton();
        return this;
    }

    public FlowHelper fillFlowName(String value) {
        log.info("Fill flow name - " + value);
        flowPage.fillFlowName(value);
        return this;
    }

    public FlowHelper clickMarkIconButton() {
        log.info("Confirm editing flow name");
        flowPage.clickMarkIconButton();
        return this;
    }

    public boolean isAlertMessageDisplayed() {
        boolean result = flowPage.isAlertMessageDisplayed();
        log.info("Check if alert message is displayed - " + result);
        return result;
    }

    public boolean isSavedChangesAlertDisplayed() {
        boolean result = flowPage.isSavedChangesAlertDisplayed();
        log.info("Check if alert with 'Changes have been saved' is displayed - " + result);
        return result;
    }

    public boolean isDBRequestInputDisplayed(String unit) {
        boolean result = flowPage.isDBRequestInputDisplayed(unit);
        log.info("Check if input for '" + unit + "' to request DB is displayed - " + result);
        return result;
    }

    public boolean isGovernmentChecksDisplayed(String country, List<String> checks) {
        boolean result = checks.stream().allMatch(c -> {
            boolean actual = flowPage.isGovernmentCheckDisplayed(country, c);
            log.info("'" + c + "' check for '" + country + "' country is displayed - " + actual);
            return actual;
        });
        return result;
    }

    public boolean isGovernmentCheckboxSwitchable(String country, List<String> checks) {
        boolean result = checks.stream().allMatch(c -> {
            boolean actual = flowPage.isGovernmentCheckboxSwitchable(country, c);
            log.info("Checkbox of '" + c + "' check for '" + country + "' country is switchable - " + actual);
            return actual;
        });
        return result;
    }

    public FlowHelper waitUntilChangesWillBeSaved() {
        flowPage.waitUntilChangesWillBeSaved();
        return this;
    }

    public FlowHelper clickProductSettingSelective(FlowSettings setting) {
        flowPage.clickProductSettingSelective(setting);
        return this;
    }

    public FlowHelper clickProductSettingRadioButton(FlowSettings setting) {
        log.info("Click '" + setting + "' product setting");
        flowPage.clickProductSettingRadioButton(setting);
        return this;
    }

    public WebHookMenuHelper clickWebHookButton() {
        flowPage.clickWebHooksButton();
        return new WebHookMenuHelper();
    }

    public FlowHelper clickAddStepButton() {
        log.info("CLick on 'Add Step' button");
        flowPage.clickAddStepButton();
        return this;
    }

    public FlowHelper selectDocumentsCheckbox(String document, boolean flag) {
        log.info("Select '" + document + "' checkbox on 'Add document step' pop-up in product settings - " + flag);
        flowPage.selectDocumentsCheckbox(document, flag);
        return this;
    }

    public FlowHelper clickAddDocumentButton() {
        log.info("CLick on 'Add' document button");
        flowPage.clickAddDocumentButton();
        return this;
    }

    public boolean isDocumentDisplayed(String document) {
        boolean result = flowPage.isDocumentDisplayed(document);
        log.info("Check if '" + document + "' document is added in verification list in product settings - " + result);
        return result;
    }

    public boolean isProductIntoWarningPopup(Products product) {
        boolean result = flowPage.isProductIntoWarningPopup(product);
        log.info("Check if '" + product + "' product is into warning popup - " + result);
        return result;
    }

    public FlowHelper clickBasketButtonIntoProduct(Products product) {
        log.info("Click 'Basket' button into '" + product + "' product");
        flowPage.clickBasketButtonIntoProduct(product);
        return this;
    }

    public FlowHelper deleteDocumentStep(int stepNumber) {
        log.info("Delete document 'Step' with number - " + stepNumber);
        flowPage.deleteDocumentStep(stepNumber);
        return this;
    }

    public FlowHelper addDocumentToDVSettings(String document, boolean flag) {
        clickAddStepButton().selectDocumentsCheckbox(document, flag).clickAddDocumentButton();
        return this;
    }

    public FlowHelper fillAgeThresholdInput(String age) {
        log.info("Fill 'Age Threshold' with value - " + age);
        flowPage.fillAgeThresholdInput(age);
        return this;
    }

    public boolean isErrorAgeThresholdDisplayed() {
        boolean result = flowPage.isErrorAgeThresholdDisplayed();
        log.info("Check if 'Age threshold' value is not valid and error message is displayed - " + result);
        return result;
    }

    public boolean isIncorrectTypeDocumentWarningDisplayed() {
        boolean result = flowPage.isIncorrectTypeDocumentWarningDisplayed();
        log.info("Check if 'Products Gov Check and AML won't work with this type of document' warning is displayed - " + result);
        return result;
    }

    public String getVerificationWizardTitle() {
        String title = flowPage.getVerificationWizardTitle();
        log.info("Get 'Verification wizard' title - " + title);
        return title;
    }

    public FlowHelper switchToVerificationWizard() {
        log.info("Switch to 'Verification wizard'");
        flowPage.switchToVerificationWizard();
        return this;
    }

    public boolean isAddStepButtonNotDisplayed() {
        boolean result = flowPage.isAddStepButtonNotDisplayed();
        log.info("Check if 'Add Step' button is displayed - " + result);
        return result;
    }

    public FlowHelper clickStartVerificationButton() {
        log.info("Click on 'Start verification' button");
        flowPage.navigateToVerificationWizard(flowPage.getWizardUrl());
        flowPage.clickStartVerificationButton();
        return this;
    }

    public FlowHelper selectCountry(VerificationInfoByCountry verificationInfoByCountry) {
        log.info("Select country for document verification - " + verificationInfoByCountry.getName());
        flowPage.selectCountry(verificationInfoByCountry);
        return this;
    }

    public FlowHelper putFrontPhoto(String frontFileName) {
        log.info("Upload front photo with name - " + frontFileName);
        flowPage.putFrontPhoto(frontFileName);
        return this;
    }

    public boolean isErrorVerificationPopupAppeared() {
        boolean result = flowPage.isErrorVerificationPopupAppeared();
        log.info("Check if 'Error verification' popup is appeared - " + result);
        return result;
    }

    public FlowHelper clickEditStepButton(int stepNumber) {
        log.info("Edit document 'Step' with number - " + stepNumber);
        flowPage.clickEditStepButton(stepNumber);
        return this;
    }

    public FlowHelper putBackPhoto(String backFileName) {
        log.info("Upload back photo with name - " + backFileName);
        flowPage.putBackPhoto(backFileName);
        return this;
    }

    public FlowHelper fillCountryRestriction(String value) {
        log.info("Select country restriction - " + value);
        flowPage.fillCountryRestriction(value);
        return this;
    }

    public FlowHelper clickCountryRestriction(String country) {
        log.info("Delete country restriction - " + country);
        flowPage.clickCountryRestriction(country);
        return this;
    }

    public boolean isCountryRestrictionDisplayed(String country) {
        boolean result = flowPage.isCountryRestrictionDisplayed(country);
        log.info("Check if '" + country + "' country restriction is displayed - " + result);
        return result;
    }

    public FlowHelper clickCrossCountryRestrictionButton() {
        log.info("Delete all country restriction");
        flowPage.clickCrossCountryRestrictionButton();
        return this;
    }

    public boolean isSettingsRadioButtonSelected(FlowSettings setting) {
        boolean result = flowPage.isSettingsRadioButtonSelected(setting);
        log.info("Check if '" + setting + "' radio button is selected - " + result);
        return result;
    }
}
