package com.mati.web.helpers;

import com.mati.web.IConstants;
import com.mati.web.helpers.login.ResetPasswordHelper;
import com.mati.web.pages.GmailPage;
import org.openqa.selenium.NoSuchElementException;

public class GmailHelper extends AbstractHelper {

    private GmailPage gmailPage = new GmailPage();

    public GmailHelper navigateToPage() {
        gmailPage.navigateToPage();
        log.info("Navigate to gmail.com");
        return this;
    }

    public GmailHelper fillEmailField(String email) {
        gmailPage.fillEmailField(email);
        log.info("Fill 'email' field with - " + email);
        return this;
    }

    public GmailHelper clickNextButton() {
        gmailPage.clickNextButton();
        log.info("Click 'next' button");
        return this;
    }

    public GmailHelper fillPasswordField(String password) {
        gmailPage.fillPasswordField(password);
        log.info("Fill 'password' field with - " + password);
        return this;
    }

    public void waitProfileIdentifierToLoaded() {
        gmailPage.waitProfileIdentifierToLoaded();
        log.info("Wait until 'profile identifier' will be load");
    }

    public void waitGmailLogoToLoaded() {
        gmailPage.waitGmailLogoToLoaded();
        log.info("Wait until 'gmail logo' will be load");
    }

    public GmailHelper selectMessage() {
        gmailPage.selectGetMatiMessage();
        log.info("Select the message from getmati.com");
        return this;
    }

    public boolean isResetPasswordButtonDisplayed() {
        boolean result = gmailPage.isResetPasswordButtonDisplayed();
        log.info("Check if 'Reset password' button is displayed - " + result);
        return result;
    }

    public void openInputGmailMessages(String email, String password) {
        navigateToPage()
                .fillEmailField(email)
                .clickNextButton()
                .waitProfileIdentifierToLoaded();
        fillPasswordField(password)
                .clickNextButton()
                .waitGmailLogoToLoaded();
    }

    public void deleteMessage() {
        gmailPage.deleteMessage();
        log.info("Delete message");
    }

    public boolean isEmailForPresent(String email) {
        boolean isPresent = gmailPage.isEmailForPresent(email);
        log.info(String.format("Email for %s is present: %s", email, isPresent));
        return isPresent;
    }

    public ResetPasswordHelper clickSetPasswordButton() {
        log.info("Click on 'Set password' button");
        gmailPage.clickOnSetPasswordButton();
        return new ResetPasswordHelper();
    }

    public GmailHelper openMessageBody() {
        log.info("Click on 'Hide Message' button");
        try {
            gmailPage.clickOnHideButton();
        } catch (NoSuchElementException e) {
            log.info("Email body is opened");
        }
        return this;
    }
}
