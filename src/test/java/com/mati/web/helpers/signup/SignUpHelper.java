package com.mati.web.helpers.signup;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.signup.SignUpPage;

public class SignUpHelper extends AbstractHelper {

    SignUpPage signUpPage = new SignUpPage();

    public SignUpHelper fillEmailField(String email) {
        signUpPage.fillEmailInput(email);
        log.info("Fill 'email' field with - " + email);
        return this;
    }

    public SignUpHelper fillPasswordField(String password) {
        signUpPage.fillPasswordInput(password);
        log.info("Fill 'password' field with - " + password);
        return this;
    }

    public void clickSignInButton() {
        log.info("Click on 'Sign in' button");
        signUpPage.clickButtonSignUp();
    }

    public SignUpHelper navigateToPage() {
        log.info("Navigate to 'SignUp' page");
        signUpPage.navigateToPage();
        return this;
    }

    public boolean isErrorMessageDisplayed(String message) {
        boolean result = signUpPage.isErrorMessageDisplayed(message);
        log.info(String.format("Check if %s is displayed for empty 'email' field - %s", message, result));
        return result;
    }

    public boolean isErrorMessageSpanDisplayed(String message) {
        boolean result = signUpPage.isErrorMessageSpanDisplayed(message);
        log.info(String.format("Check if %s is displayed for empty 'email' field - %s", message, result));
        return result;
    }
}
