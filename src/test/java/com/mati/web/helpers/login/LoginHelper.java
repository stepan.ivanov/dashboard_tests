package com.mati.web.helpers.login;

import com.codeborne.selenide.WebDriverRunner;
import com.mati.api.ApiService;
import com.mati.enums.rollup.Language;
import com.mati.utils.EnvironmentConfig;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.helpers.merchantdashboard.AnalyticsHelper;
import com.mati.web.helpers.merchantdashboard.RollUpMenuHelper;
import com.mati.web.pages.login.LoginPage;

public class LoginHelper extends AbstractHelper {

    private LoginPage loginPage = new LoginPage();
    private RecoveryPasswordHelper recoveryPasswordHelper = new RecoveryPasswordHelper();
    private AnalyticsHelper analyticsHelper = new AnalyticsHelper();
    private RollUpMenuHelper rollUpMenuHelper = new RollUpMenuHelper();
    private ContactUsHelper contactUsHelper= new ContactUsHelper();

    public AnalyticsHelper loginMatiSuccessful() {
        navigateToPage()
                .fillEmailField(EnvironmentConfig.login)
                .fillPasswordField(EnvironmentConfig.password)
                .clickSignInButton();
        analyticsHelper.waitForPageLoad();
        analyticsHelper.closeInformationWizard().closePostPopUp();
        ApiService.updateLanguage(Language.ENGLISH);
        WebDriverRunner.getWebDriver().navigate().refresh();
        return analyticsHelper;
    }

    public AnalyticsHelper loginMatiAsAgent() {
        navigateToPage()
                .fillEmailField(EnvironmentConfig.loginAgent)
                .fillPasswordField(EnvironmentConfig.passwordAgent)
                .clickSignInButton();
        analyticsHelper.waitForPageLoad();
        analyticsHelper.closeInformationWizard().closePostPopUp().switchToNewDesign();
        rollUpMenuHelper.clickOnLanguageButton().selectLanguage(Language.ENGLISH);
        return analyticsHelper;
    }

    public LoginHelper navigateToPage() {
        log.info("Navigate to 'Login' page");
        loginPage.navigateToPage();
        return this;
    }

    public void clickSignInButton() {
        log.info("Click on 'Sign in' button");
        loginPage.clickSignInButton();
    }

    public boolean isEmailErrorMessageDisplayed() {
        boolean result = loginPage.isEmailErrorMessageDisplayed();
        log.info("Check if error message is displayed for empty 'email' field - " + result);
        return result;
    }

    public boolean isPasswordErrorMessageDisplayed() {
        boolean result = loginPage.iPasswordErrorMessageDisplayed();
        log.info("Check if error message is displayed for empty 'password' field - " + result);
        return result;
    }

    public boolean isForgotPasswordLinkDisplayed() {
        boolean result = loginPage.isForgotPasswordLinkDisplayed();
        log.info("Check if 'Forgot password' link is displayed - " + result);
        return result;
    }

    public String getEmailErrorMessage() {
        String result = loginPage.getEmailErrorMessage();
        log.info("Email error message has value - " + result);
        return result;
    }

    public String getPasswordErrorMessage() {
        String result = loginPage.getPasswordErrorMessage();
        log.info("Password error message has value - " + result);
        return result;
    }

    public LoginHelper fillEmailField(String email) {
        loginPage.fillEmailField(email);
        log.info("Fill 'email' field with - " + email);
        return this;
    }

    public LoginHelper fillPasswordField(String password) {
        loginPage.fillPasswordField(password);
        log.info("Fill 'password' field with - " + password);
        return this;
    }

    public boolean isSignInTitleDisplayed() {
        boolean result = loginPage.isSignInTitleDisplayed();
        log.info("Check if 'Sign in' title is displayed - " + result);
        return result;
    }

    public boolean isLanguageMenuDisplayed() {
        boolean result = loginPage.isLanguageMenuDisplayed();
        log.info("Check if language menu is displayed - " + result);
        return result;
    }

    public boolean isContactUsLinkDisplayed() {
        boolean result = loginPage.isContactUsLinkDisplayed();
        log.info("Check if 'Contact Us' link is displayed - " + result);
        return result;
    }

    public boolean isMatiLogoDisplayed() {
        boolean result = loginPage.isMatiLogoDisplayed();
        log.info("Check if 'MATI' logo is displayed - " + result);
        return result;
    }

    public boolean isSignInButtonDisplayed() {
        boolean result = loginPage.isSignInButtonDisplayed();
        log.info("Check if 'Sign in' button is displayed - " + result);
        return result;
    }

    public boolean isEmailFieldDisplayed() {
        boolean result = loginPage.isEmailFieldDisplayed();
        log.info("Check if all 'login' page elements are displayed - " + result);
        return result;
    }

    public boolean isPasswordFieldDisplayed() {
        boolean result = loginPage.isPasswordFieldDisplayed();
        log.info("Check if all 'login' page elements are displayed - " + result);
        return result;
    }

    public RecoveryPasswordHelper clickForgotPasswordLink() {
        loginPage.clickForgotPasswordLink();
        log.info("Click 'forgot password' link and move to 'Recovery password' page");
        return recoveryPasswordHelper;
    }

    public boolean isFlexStartDisplayed() {
        boolean result = loginPage.isFlexStartDisplayed();
        log.info("Check if 'flex start' part of page is displayed - " + result);
        return result;
    }

    public boolean isLoginPage() {
        boolean result = loginPage.isLoginPage();
        log.info("Check if this is 'Login' page - " + result);
        return result;
    }

    public LoginHelper waitUntilPageLoading() {
        log.info("Wait until page is loading....");
        loginPage.waitUntilPageLoading();
        return this;
    }

    public ContactUsHelper clickContactUsLink() {
        log.info("Click ContactUsLink");
        loginPage.clickContactUsLink();
        contactUsHelper.waitForPageLoaded();
        return contactUsHelper;
    }
}
