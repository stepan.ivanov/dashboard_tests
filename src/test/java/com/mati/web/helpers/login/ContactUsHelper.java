package com.mati.web.helpers.login;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.login.ContactUsPage;

public class ContactUsHelper extends AbstractHelper {

    private ContactUsPage contactUsPage = new ContactUsPage();

    public ContactUsHelper waitForPageLoaded() {
        log.info("Wait for page loaded");
        contactUsPage.waitForPageLoaded();
        return this;
    }

    public ContactUsHelper fillContactUsForm(String email, String firstName, String lastName, String number,
                                             String goals) {
        log.info("Fill email: {}", email);
        contactUsPage.fillEmailInput(email);
        log.info("Fill first name: {}", firstName);
        contactUsPage.fillFirstNameInput(firstName);
        log.info("Fill last name: {}", lastName);
        contactUsPage.fillLastNameInput(lastName);
        log.info("Fill phone number: {}", number);
        contactUsPage.fillYourPhoneNumber(number);
        log.info("Fill goals: {}", goals);
        contactUsPage.fillGoalsForUsing(goals);
        return this;
    }

    public ContactUsHelper clickSubmitButton() {
        log.info("Click submit button");
        contactUsPage.clickSubmitButton();
        return this;
    }

    public boolean isScheduleFrameDisplayed() {
        boolean isScheduleFrameDisplayed = contactUsPage.isScheduleFrameDisplayed();
        log.info("Thank message is displayed: {}", isScheduleFrameDisplayed);
        return isScheduleFrameDisplayed;
    }
}
