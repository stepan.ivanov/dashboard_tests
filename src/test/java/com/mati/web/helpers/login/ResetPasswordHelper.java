package com.mati.web.helpers.login;

import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.login.ResetPasswordPage;

public class ResetPasswordHelper extends AbstractHelper {

    private ResetPasswordPage resetPasswordPage = new ResetPasswordPage();

    public ResetPasswordHelper fillInputPassword(String password) {
        log.info("Fill password {}", password);
        resetPasswordPage.fillInputPassword(password);
        return this;
    }

    public void clickOnButtonReset(){
        log.info("Click on button 'Reset Password'");
        resetPasswordPage.clickButtonResetPassword();
    }

}
