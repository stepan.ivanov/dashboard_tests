package com.mati.web.helpers.login;

import com.mati.web.IConstants;
import com.mati.web.helpers.AbstractHelper;
import com.mati.web.pages.login.RecoveryPasswordPage;

public class RecoveryPasswordHelper extends AbstractHelper {

    private RecoveryPasswordPage recoveryPasswordPage = new RecoveryPasswordPage();

    public RecoveryPasswordHelper fillEmailField(String email) {
        recoveryPasswordPage.fillEmailField(email);
        log.info("Fill 'email' field with - " + email);
        return this;
    }

    public void clickSendResetEmailButton() {
        recoveryPasswordPage.clickSendResetEmailButton();
        log.info("Click 'Send reset email' button");
    }

    public boolean isInvalidEmailAddressErrorMessageDisplayed() {
        boolean result = recoveryPasswordPage.isInvalidEmailAddressErrorMessageDisplayed();
        log.info("Check if error message is displayed for invalid email address - " + result);
        return result;
    }

    public String getInvalidEmailAddressErrorMessage() {
        String actual = recoveryPasswordPage.getInvalidEmailAddressErrorMessage();
        log.info("'Invalid email address' error message - " + actual);
        return actual;
    }

    public boolean isRecoveryPasswordPage() {
        boolean result = recoveryPasswordPage.isRecoveryPasswordPage();
        log.info("Check if this is 'Recovery Password' page - " + result);
        return result;
    }
}
