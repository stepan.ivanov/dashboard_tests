package com.mati.web.listeners;


import com.codeborne.selenide.WebDriverRunner;
import com.google.common.io.BaseEncoding;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {

    private static final Logger LOGGER = LogManager.getLogger(TestListener.class);

    @Override
    public void onTestFailure(ITestResult result) {
        result.getThrowable().printStackTrace();
        saveLog(result.getThrowable().getMessage());
        LOGGER.info("TEST '{}' FAILED", result.getName());
//        LOGGER.info("RP_MESSAGE#BASE64#{}#{}", BaseEncoding.base64().encode(takeScreenshot()), "Screenshot");
        makeAllureScreenshot();
    }


    @Override
    public void onTestStart(ITestResult result) {
        LOGGER.info("TEST '{}' STARTED", result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        makeAllureScreenshot();
        LOGGER.info("TEST '{}' SUCCESS", result.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        LOGGER.info("TEST '{}' SKIPPED", result.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    @Override
    public void onFinish(ITestContext context) {
        LOGGER.info("CLASS '{}' FINISHED", context.getName());
    }

    @Override
    public void onStart(ITestContext context) {
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] makeAllureScreenshot() {
        return takeScreenshot();
    }

    @Attachment(value = "{0}", type = "text/plain")
    public String saveLog(String message) {
        return message;
    }

    private byte[] takeScreenshot() {
        try {
            return ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
        } catch (WebDriverException e) {
            LOGGER.error("Screenshot making error", e);
            return null;
        }
    }
}
