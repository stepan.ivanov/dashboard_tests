package com.mati.web.listeners;

import io.qameta.allure.Attachment;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import java.io.Serializable;

@Plugin(name = "ReportAppender", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE, printObject = true)
public class ReportAppender extends AbstractAppender {

    protected ReportAppender(final String name, final Filter filter, final Layout<? extends Serializable> layout,
                             final boolean ignoreExceptions, final Property[] properties) {
        super(name, filter, layout, ignoreExceptions, properties);
    }

    public void append(LogEvent event) {
        String log = new String(getLayout().toByteArray(event));
        saveLog(log);
    }

    @Attachment(value = "{0}", type = "text/plain")
    private String saveLog(String message) {
        return message;
    }

    @PluginFactory
    public static ReportAppender createAppender(@PluginAttribute("name") String name,
                                                @PluginElement("filter") Filter filter,
                                                @PluginElement("layout") Layout<LogEvent> layout,
                                                @PluginElement("Properties") Property[] properties
    ) {
        if (name == null) {
            LOGGER.error("No name provided for ReportAppender");
            return null;
        }

        if (layout == null) {
            LOGGER.error("No layout provided for ReportAppender");
            return null;
        }
        return new ReportAppender(name, filter, layout, true, properties);
    }
}
