package com.mati.web;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.mati.utils.Properties;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DriverUtils {

    private static final Logger log = LogManager.getLogger(DriverUtils.class);

    private DriverUtils() {
    }

    public static void runBrowser() {
        Configuration.browser = System.getProperty("browser", "chrome");
        Configuration.browserVersion = Properties.getPropertyString("version");
        Configuration.pageLoadTimeout = Properties.getPropertyLong("page.load.timeout") * 1000;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.<String, Object>of(
                "enableVNC", true,
                "enableVideo", true
        ));
        Configuration.browserCapabilities = capabilities;
        Configuration.startMaximized= true;
        if ("Mac OS X".equals(System.getProperty("os.name").replaceAll("\\d", "").trim())) {
            WebDriverManager.chromedriver().setup();
        }
        else {
            Configuration.remote = Properties.getPropertyString("hub");
        }
    }


    public static void tearDownBrowser() {
        WebDriverRunner.closeWebDriver();
    }

    public static Map<String, Object> downloadOption() {
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", System.getProperty("user.dir") + File.separator + IConstants.currentFolder);
        return prefs;
    }

    public static String getCurrentUrl() {
        String currentUrl = WebDriverRunner.getWebDriver().getCurrentUrl();
        log.info("Current url: {}", currentUrl);
        return currentUrl;
    }

    public static void openUrl(String url) {
        log.info("Open url: {}", url);
        WebDriverRunner.getWebDriver().navigate().to(url);
    }

    public static void moveToNewWindowByTitle(String title) {
        log.info("Move to Window with name: {}", title);
        Set<String> windowHandles = WebDriverRunner.getWebDriver().getWindowHandles();
        windowHandles.stream()
                .anyMatch(it -> WebDriverRunner.getWebDriver().switchTo().window(it).getTitle().equals(title));
    }
}
