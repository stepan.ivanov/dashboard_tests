package com.mati.web;

public interface IConstants {

    String prefix = "[AQA]_";
    String emptyString = "";

    String reviewModeButtonColor = "rgba(35, 41, 57, 1)";

    String testEmail = "shared_auto@mati.io";
    String testMatiPassword = "testtest123";
    String testGmailPassword = "Stepan1999!";
    String emailWithoutDog = "testgmail.com";
    String emailWithDotAfterDog = "test@.gmail.com";
    String testEmailPersonal = "TestIvanSmith2";
    String passwordForTestEmailPersonal = "TestIvanSmith2qq!!1";

    String verificationNameForMexico = "Enrique Arturo Blanco Miranda";
    String verificationNameForCanada = "Kristie Lindell";
    String currentFolder = "target";
    String verificationPDFFileNameFormat = "mati-identity-%s.pdf";
    String verificationCSVFileName = "mati-verifications.zip";
    String frontMexico = "MX_NI_FRONT";
    String backMexico = "MX_NI_BACK";
    String frontMexicoBW = "MX_NI_FRONT_BW";
    String backMexicoBW = "MX_NI_BACK_BW";
    String frontCanada = "ca_dl_front";
    String backCanada = "ca_dl_back";
    String filePath = System.getProperty("user.dir") + "/src/test/resources/verificationfoto/%s.jpeg";

    String AQA_FLOW_NAME = "[AQA] flow";
    String TIMESTAMP_FLOW = "Timestamp";
    String AQA_FLOW_ID = "6127676cd09e25001b772900";

    String phoneNumberUS = "2163547758";
}
