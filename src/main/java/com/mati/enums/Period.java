package com.mati.enums;

public enum Period {

    TODAY("Today", "Hoy", "Hoje"),
    YESTERDAY("Yesterday", "Ayer", "Ontem"),
    LAST_SEVEN_DAYS("Last 7 Days", "Los últimos 7 días", "Últimos 7 dias"),
    LAST_THIRTY_DAYS("Last 30 Days", "Últimos 30 días", "Últimos 30 dias"),
    LAST_WEEK("Last Week", "La semana pasada", "Semana Anterior"),
    LAST_MONTH("Last Month", "El mes pasado", "Mês passado"),
    THIS_MONTH("This Month", "Este mes", "Este mês"),
    THIS_WEEK("This Week", "Esta semana", "Esta semana");

    private String period;
    private String spanishPeriod;
    private String portuguesePeriod;

    Period(String period, String spanishPeriod, String portuguesePeriod) {
        this.period = period;
        this.spanishPeriod = spanishPeriod;
        this.portuguesePeriod = portuguesePeriod;
    }

    public String getSpanishPeriod() {
        return spanishPeriod;
    }

    public String getPortuguesePeriod() {
        return portuguesePeriod;
    }

    public String getPeriod() {
        return period;
    }
}
