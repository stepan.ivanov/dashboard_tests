package com.mati.enums;

public enum CountriesFilter {

    MEXICO("MX");

    private final String value;

    CountriesFilter(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
