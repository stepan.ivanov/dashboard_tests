package com.mati.enums.rollup;

public enum Language {

    ENGLISH("English", "en"),
    SPANISH("Español","es"),
    PORTUGUESE("Português","pt-BR");

    private String language;
    private String apiLanguage;

    Language(String language, String apiLanguage) {
        this.language = language;
        this.apiLanguage = apiLanguage;
    }

    public String getLanguage() {
        return language;
    }
    public String getApiLanguage() {
        return apiLanguage;
    }
}
