package com.mati.enums;

public enum Role {
    ADMIN("1"),
    AGENT("2");

    private String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
