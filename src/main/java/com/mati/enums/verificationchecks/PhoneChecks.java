package com.mati.enums.verificationchecks;

public enum PhoneChecks implements Checks {

    PHONE_VALIDATION("panel-phone-ownership-validation-header"),
    PHONE_RISK("panel-phone-risk-analysis-validation-header");

    private final String value;

    PhoneChecks(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String getName() {
        return this.name();
    }
}
