package com.mati.enums.verificationchecks;

public interface Checks {

    String getValue();

    String getName();
}
