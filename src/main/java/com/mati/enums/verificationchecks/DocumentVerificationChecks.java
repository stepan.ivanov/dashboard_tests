package com.mati.enums.verificationchecks;

public enum DocumentVerificationChecks implements Checks {

    DOCUMENT_ALTERATION_DETECTION("panel-alteration-detection-header"),
    DOCUMENT_FIELDS_EXTRACTION("panel-empty-fields-header"),
    DOCUMENT_EXPIRE_DETECTION("panel-expired-date-header"),
    DOCUMENT_TEMPLATE_MATCHING("panel-template-matching-header"),
    WATCHLIST_DETECTION("panel-watchlists-header"),
    THIS_USER_IS_OLD_ENOUGH("panel-age-check-header");

    private String value;

    DocumentVerificationChecks(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return this.name();
    }
}
