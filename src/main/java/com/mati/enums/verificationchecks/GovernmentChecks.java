package com.mati.enums.verificationchecks;

public enum GovernmentChecks implements Checks {

    ARGENTINA_DNI_DATABASE_MATCH("panel-argentinian-renaper-validation-header"),
    ARGENTINA_DNI_VALIDATION("panel-argentinian-dni-validation-header"),
    BOLIVIAN_OEP_VALIDATION("bolivian-oep-validation"),
    BRAZIL_CPF_VALIDATION("brazilian-cpf-validation"),
    MEXICO_PEP_VALIDATION("mexican-pep-validation"),
    MEXICO_CURP_VALIDATION("mexican-curp-validation"),
    MEXICO_RFC_VALIDATION("mexican-rfc-validation"),
    CHILE_CIVIL_VALIDATION("panel-chilean-registro-civil-validation-header"),
    COSTA_RICO_ATV_VALIDATION("costa-rican-atv-validation"),
    COSTA_RICO_TSE_VALIDATION("costa-rican-tse-validation"),
    COSTA_RICO_SOCIAL_SECURITY_VALIDATION("costa-rican-social-security-validation"),
    ECUADOR_REGISTRO_VALIDATION("ecuadorian-registro-civil-validation"),
    GUATEMALA_TSE_VALIDATION("guatemalan-tse-validation"),
    HONDURAS_RNP_VALIDATION("honduran-rnp-validation"),
    PERU_RENIEC_VALIDATION("panel-peruvian-reniec-validation-header"),
    PANAMA_TRIBUNAL_ELECTORAL_VALIDATION("panamenian-tribunal-electoral-validation"),
    SALVADOR_TSE_VALIDATION("salvadorian-tse-validation"),
    DOMINICAN_REPUBLIC_JCE_VALIDATION("panel-dominican-jce-validation-header");

    private String value;

    GovernmentChecks(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String getName() {
        return this.name();
    }
}
