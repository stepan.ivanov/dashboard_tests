package com.mati.enums;

public enum Errors {
    INVALID_EMAIL_ADDRESS("Invalid email address"),
    FIELD_IS_REQUIRED("Field is required"),
    CAN_NOT_BE_PERSONAL_EMAIL("It cannot be personal email (Gmail, Hotmail, etc)."),
    PASSWORD_NEED_TO_BE_AT_LEAST_8_CHAR("Password needs to be 8 characters long and at least one number"),
    EMAIL_ALREADY_IN_USE("The specified email is already in use."),
    NO_SPECIAL_CHARACTERS("No special characters allowed"),
    INCORRECT_CREDENTIALS("Password or email is incorrect");


    Errors(String message) {
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }
}
