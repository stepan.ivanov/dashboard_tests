package com.mati.enums.verificationlist;

public enum VerificationStatus {

    VERIFIED("Verified", "rgba(90, 199, 148, 1)", "GREEN"),
    REVIEW_NEEDED("Review Needed", "rgba(254, 198, 73, 1)", "ORANGE"),
    REJECTED("Rejected", "rgba(254, 117, 129, 1)", "RED"),
    POSTPONED("Postponed", "", ""),
    RUNNING("Running", "", ""),
    IN_REVIEW_MODE("In Review Mode", "", "");

    private String status;
    private String rgba;
    private String color;

    VerificationStatus(String status, String rgba, String color) {
        this.status = status;
        this.rgba = rgba;
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public String getColor() {
        return color;
    }

    public String getRgba() {
        return rgba;
    }
}
