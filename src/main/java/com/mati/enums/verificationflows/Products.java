package com.mati.enums.verificationflows;

public enum Products {

    IP_CHECK("IP check", "IpCheck"),
    DOCUMENT_VERIFICATION("Document Verification", "DocumentVerification"),
    BIOMETRIC_VERIFICATION("Biometric Verification", "BiometricVerification"),
    GOVERNMENT_CHECK("Government check", "GovernmentCheck"),
    ANTI_MONEY_LAUNDERING("Anti-Money Laundering", "AmlCheck"),
    USER_ACCESS_MANAGEMENT("User access management", "ReVerification"),
    EMAIL_CHECK("Email check", "EmailCheck"),
    PHONE_CHECK("Phone check", "PhoneCheck"),
    CREDIT_CHECK("Credit check", "CreditCheck"),
    CUSTOM_DOCUMENT("Custom Document", "CustomDocuments");

    private String product;
    private String dataID;

    Products(String product, String dataID) {
        this.product = product;
        this.dataID = dataID;
    }

    public String getProduct() {
        return product;
    }

    public String getDataID() {
        return dataID;
    }
}
