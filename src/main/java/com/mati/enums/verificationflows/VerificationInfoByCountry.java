package com.mati.enums.verificationflows;

public enum VerificationInfoByCountry {

    MEXICO("Mexico", "MX_NI_FRONT", "MX_NI_BACK", "Enrique Arturo Blanco Miranda"),
    CANADA("Canada", "ca_dl_front", "ca_dl_back", "Kristie Lindell"),
    ARGENTINA("Argentina",  "argentina_front","argentina_back", "Gaston Andres Guilleron"),
    BOLIVIA("Bolivia","bolivia_front","bolivia_back","BRAYAN LUIS QUISPE CORINE"),
    BRAZIL("Brazil","brazil_front","brazil_back", "Vanessa Dolores Mavila Del Rio"),
    CHILE("Chile","chile_front","chile_back","Nelson Hernan Garrido Alvial"),
    COSTA_RICA("Costa Rica","costa_rica_front","costa_rica_back","KENDALL ABRAHAN VARGAS SANDOVAL"),
    ECUADOR("Ecuador","ecuador_front","ecuador_back","CEVALLOS FIGUEROA EVELYN MARILYN"),
    GUATEMALA("Guatemala","guatemala_front","guatemala_back","MARVELIO LEVI, LIMA ,"),
    HONDURAS("Honduras","honduras_front","honduras_back","GLEDIA YOLANY PASTOR AGUILAR"),
    PERU("Peru", "peru_front","peru_back","Carlos Antonio De La Torre Hernandez"),
    PANAMA("Panama","panama_front","panama_back","Miguel Arturo Pereyra Garrido"),
    SALVADOR("El Salvador","salvador_front","salvador_back","ANNER NOE RIVAS GRANADOS"),
    UNITED_STATES("United States","us_front","us_back","NONE"),
    DOMINICAN_REPUBLIC("Dominican Republic","dominican_republic_front","dominican_republic_back","Angel Enmanuel King Castillo");

    private final String name;
    private final String frontSide;
    private final String backSide;
    private final String personName;

    VerificationInfoByCountry(String name, String frontSide, String backSide, String personName) {
        this.name = name;
        this.frontSide = frontSide;
        this.backSide = backSide;
        this.personName = personName;
    }

    public String getName() {
        return name;
    }

    public String getFrontSide() {
        return frontSide;
    }

    public String getBackSide() {
        return backSide;
    }

    public String getPersonName() {
        return personName;
    }
}
