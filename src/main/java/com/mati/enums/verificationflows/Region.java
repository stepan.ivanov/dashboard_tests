package com.mati.enums.verificationflows;

public enum Region {

    ALBERTA("Alberta"),
    MANITOBA("Manitoba");

    private String name;

    Region(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
