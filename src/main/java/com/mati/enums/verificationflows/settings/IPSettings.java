package com.mati.enums.verificationflows.settings;

public enum IPSettings implements FlowSettings {

    IP_GEO_RESTRICTION("IP Geo-restriction"),
    VPN_RESTRICTION("VPN restriction");

    private final String value;

    IPSettings(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
