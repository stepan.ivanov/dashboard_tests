package com.mati.enums.verificationflows.settings;

public enum PhoneCheckSettings implements FlowSettings {

    MAKE_STEP_OPTIONAL("Make step optional"),
    RISK_ANALYSIS("Risk analysis");

    private final String value;

    PhoneCheckSettings(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
