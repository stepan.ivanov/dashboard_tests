package com.mati.enums.verificationflows.settings;

public enum GovernmentCheckSettings implements FlowSettings {

    COUNTIES_AND_CHECKS("Countries and checks"),
    DATABASES_REQUEST_TIMEOUT("Databases request timeout"),
    ARGENTINA("Argentina"),
    BOLIVIA("Bolivia"),
    BRAZIL("Brazil"),
    CHILE("Chile"),
    MEXICO("Mexico"),
    COSTA_RICA("Costa Rica"),
    ECUADOR("Ecuador"),
    GUATEMALA("Guatemala"),
    HONDURAS("Honduras"),
    PERU("Peru"),
    PANAMA("Panama"),
    SALVADOR("Salvador"),
    DOMINICAN_REPUBLIC("Dominican Republic");

    private final String value;

    GovernmentCheckSettings(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
