package com.mati.enums.verificationflows.settings;

public enum DocumentationSettings implements FlowSettings {

    DUPLICATE_USER_DETECTION("Duplicate user detection"),
    RESTRICT_IMAGE_UPLOAD_FROM_GALLERY("Restrict image upload from gallery"),
    AGE_THRESHOLD("Age threshold"),
    CUSTOM_LIMIT("Custom limit"),
    BW_VALIDATION("Black & white validation"),
    SAME_SIDE_UPLOAD_VALIDATION("Same side upload validation"),
    COUNTRY_RESTRICTION("Country restriction"),
    RECOMMENDED_THRESHOLD("Recommended threshold (70%)"),
    PROOF_OF_OWNERSHIP("Proof of ownership");

    private String value;

    DocumentationSettings(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
