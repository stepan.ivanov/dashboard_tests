package com.mati.enums.verificationflows.settings;

public enum BiometricsVerificationSettings implements FlowSettings {

    SELFIE_VIDEO("Selfie Video"),
    SELFIE_VIDEO_VOICE("Selfie Video + Voice"),
    SELFIE_PHOTO("Selfie Photo");

    private String value;

    BiometricsVerificationSettings(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }

}
