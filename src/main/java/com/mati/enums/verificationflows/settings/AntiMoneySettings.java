package com.mati.enums.verificationflows.settings;

public enum AntiMoneySettings implements FlowSettings {

    SEARCH("Search"),
    MONITORING("Monitoring");

    private String value;

    AntiMoneySettings(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
