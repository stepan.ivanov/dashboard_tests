package com.mati.enums.verificationflows.settings;

public interface FlowSettings {

    String getValue();
}
