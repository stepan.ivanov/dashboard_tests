
package com.mati.model.requests;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Headers {

    private String host;
    @JsonProperty("user-agent")
    private String userAgent;
    @JsonProperty("content-length")
    private String contentLength;
    private String accept;
    @JsonProperty("content-type")
    private String contentType;
    @JsonProperty("x-amzn-trace-id")
    private String xAmznTraceId;
    @JsonProperty("x-forwarded-host")
    private String xForwardedHost;
    @JsonProperty("x-forwarded-server")
    private String xForwardedServer;
    @JsonProperty("x-real-ip")
    private String xRealIp;
    @JsonProperty("x-signature")
    private String xSignature;
    @JsonProperty("accept-encoding")
    private String acceptEncoding;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
