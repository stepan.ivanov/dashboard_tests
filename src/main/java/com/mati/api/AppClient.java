package com.mati.api;

import com.mati.api.models.TokenContainer;
import com.mati.utils.EnvironmentConfig;
import com.mati.web.IConstants;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class AppClient {

    public Response response(Method method, RequestSpecification requestSpec) {
        requestSpec.baseUri(EnvironmentConfig.apiUrl)
                .contentType(ContentType.JSON)
                .header("x-csrf-token", TokenContainer.getInstance().getToken())
                .cookie("_csrf_token", TokenContainer.getInstance().getToken())
                .filter(new LoggingFilter());
        RequestSpecification total = given().spec(requestSpec);
        return given(total).request(method);
    }

}
