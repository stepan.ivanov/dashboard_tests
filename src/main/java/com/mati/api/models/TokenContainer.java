package com.mati.api.models;

import lombok.Data;

import java.util.List;

@Data
public class TokenContainer {

    private static class TokenContainerHolder {
        private static final TokenContainer INSTANCE = new TokenContainer();
    }

    public static TokenContainer getInstance() {
        return TokenContainerHolder.INSTANCE;
    }

    private String token = "";
    private String bearer = "";
    private String merchant = "";
    private ThreadLocal<List<String>> flowId = new ThreadLocal<>();

    public void setBearer(String bearer) {
        this.bearer = "Bearer "+ bearer;
    }
}