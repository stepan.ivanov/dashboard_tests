package com.mati.api.models;

import lombok.Data;

@Data
public class TokenModel{
	private String token;
}