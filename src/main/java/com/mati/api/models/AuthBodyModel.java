package com.mati.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public  class AuthBodyModel{
	private String email;
	private String password;
	private String passwords;
}