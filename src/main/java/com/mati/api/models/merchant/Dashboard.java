package com.mati.api.models.merchant;

import lombok.Data;

public @Data class Dashboard{
	private boolean onboardingModal;
	private String language;
	private Object showOldDesignUntil;
}