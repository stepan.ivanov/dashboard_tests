package com.mati.api.models.merchant;

import java.util.List;
import lombok.Data;

public @Data class Merchant{
	private String owner;
	private Settings settings;
	private String createdAt;
	private Configurations configurations;
	private String displayName;
	private List<Object> collaborators;
	private String id;
	private Billing billing;
	private IndexFields indexFields;
	private List<String> tags;
	private String updatedAt;
}