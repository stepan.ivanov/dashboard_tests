package com.mati.api.models.merchant;

import java.util.List;
import lombok.Data;

public @Data class MerchantModel{
	private Merchant merchant;
	private List<MerchantsItem> merchants;
	private User user;
	private String token;
}