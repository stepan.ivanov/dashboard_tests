package com.mati.api.models.merchant;

import lombok.Data;

public @Data class MerchantsItem{
	private Merchant merchant;
	private User user;
	private String token;
}