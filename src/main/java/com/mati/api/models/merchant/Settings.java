package com.mati.api.models.merchant;

import java.util.List;
import lombok.Data;

public @Data class Settings{
	private List<Object> customDocumentConfig;
}