package com.mati.api.models.merchant;

import java.util.List;
import lombok.Data;

public @Data class IndexFields{
	private List<String> emails;
	private String ownerEmail;
}