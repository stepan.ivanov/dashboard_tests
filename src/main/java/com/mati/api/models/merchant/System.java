package com.mati.api.models.merchant;

import lombok.Data;

public @Data class System{
	private boolean forceMX;
}