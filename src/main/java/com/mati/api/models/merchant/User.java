package com.mati.api.models.merchant;

import lombok.Data;

public @Data class User{
	private String dateCreated;
	private String id;
	private String userType;
	private String locale;
	private String email;
	private String status;
	private String intercomHash;
}