package com.mati.api.models.merchant;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mati.api.models.common.InputTypesItem;
import com.mati.api.models.common.IpValidation;
import com.mati.api.models.common.Style;
import com.mati.api.models.common.VerificationPatterns;
import lombok.Data;

public @Data class Configurations{
	private List<InputTypesItem> inputTypes;
	private IpValidation ipValidation;
	@JsonIgnore
	private List<String> computations;
	private String digitalSignature;
	@JsonIgnore
	private List<String> verificationSteps;
	@JsonIgnore
	private List<String> pinnedCountries;
	private int version;
	private int emailRiskThreshold;
	private System system;
	private String integrationType;
	private List<Object> inputValidationChecks;
	private int amlWatchlistsFuzzinessThreshold;
	@JsonIgnore
	private VerificationPatterns verificationPatterns;
	private Style style;
	@JsonIgnore
	private Dashboard dashboard;
	private List<Object> customDocumentConfig;
	private List<Object> supportedCountries;
}