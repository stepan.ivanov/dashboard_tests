package com.mati.api.models.merchant;

import java.util.List;
import lombok.Data;

public @Data class Billing{
	private List<Object> providers;
}