package com.mati.api.models.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

public @Data class VerificationPatterns {
	private boolean guatemalanTseValidation;
	private boolean creditArgentinianFidelitasValidation;
	private boolean bolivianOepValidation;
	private boolean proofOfOwnership;
	private boolean mexicanPepValidation;
	private boolean costaRicanSocialSecurityValidation;
	private boolean reFacematch;
	private String ipValidation;
	private String phoneOwnershipValidation;
	private boolean honduranRnpValidation;
	private boolean emailRiskValidation;
	private boolean costaRicanTseValidation;
	private boolean chileanRegistroCivilValidation;
	private boolean peruvianSunatValidation;
	private String premiumAmlWatchlistsSearchValidation;
	private boolean venezuelanCneValidation;
	private boolean colombianNitValidation;
	private boolean peruvianReniecValidation;
	private boolean phoneRiskAnalysisValidation;
	private boolean paraguayanRcpValidation;
	private String brazilianCpfValidation;
	private boolean colombianProcuraduriaValidation;
	private boolean ecuadorianSriValidation;
	private boolean vpnDetection;
	private boolean colombianNationalPoliceValidation;
	private String emailOwnershipValidation;
	private boolean costaRicanAtvValidation;
	private boolean mexicanRfcValidation;
	private boolean colombianRegistraduriaValidation;
	private boolean duplicateUserDetection;
	private boolean ecuadorianRegistroCivilValidation;
	private boolean mexicanIneValidation;
	private String biometrics;
	private boolean venezuelanSeniatValidation;
	private boolean salvadorianTseValidation;
	private boolean mexicanCurpValidation;
	private boolean argentinianRenaperValidation;
	private boolean dominicanJceValidation;
	private boolean panamenianTribunalElectoralValidation;
	@JsonProperty("argentinian-dni-validation")
	private boolean argentinianDniValidation;
	private boolean creditBrazilianSerasaValidation;
	private boolean ghanaianGraValidation;
	private boolean colombianContraloriaValidation;
}