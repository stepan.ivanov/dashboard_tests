package com.mati.api.models.common;

import lombok.Data;

public @Data class Style{
	private String color;
	private String language;
}