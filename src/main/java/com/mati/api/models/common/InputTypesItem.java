package com.mati.api.models.common;

import lombok.Data;

public @Data class InputTypesItem {
	private String id;
}