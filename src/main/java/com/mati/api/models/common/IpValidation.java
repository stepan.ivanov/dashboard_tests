package com.mati.api.models.common;

import java.util.List;
import lombok.Data;

public @Data class IpValidation{
	private List<Object> allowedRegions;
}