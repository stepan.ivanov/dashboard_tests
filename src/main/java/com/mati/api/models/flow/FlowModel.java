package com.mati.api.models.flow;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mati.api.models.common.InputTypesItem;
import com.mati.api.models.common.IpValidation;
import com.mati.api.models.common.Style;
import com.mati.api.models.common.VerificationPatterns;
import lombok.Data;

public @Data class FlowModel{
	private List<InputTypesItem> inputTypes;
	private IpValidation ipValidation;
	@JsonIgnore
	private List<String> computations;
	private String digitalSignature;
	@JsonIgnore
	private List<String> verificationSteps;
	@JsonIgnore
	private List<String> pinnedCountries;
	private int emailRiskThreshold;
	private String createdAt;
	private String integrationType;
	private List<Object> inputValidationChecks;
	private int amlWatchlistsFuzzinessThreshold;
	private String name;
	@JsonIgnore
	private VerificationPatterns verificationPatterns;
	private Style style;
	private String id;
	private List<Object> customDocumentConfig;
	private List<Object> supportedCountries;
	private String updatedAt;
}