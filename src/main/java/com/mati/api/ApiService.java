package com.mati.api;

import com.mati.api.models.AuthBodyModel;
import com.mati.api.models.TokenContainer;
import com.mati.api.models.flow.FlowModel;
import com.mati.enums.rollup.Language;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Collections;
import java.util.List;

import static io.restassured.RestAssured.given;

public class ApiService {

    protected static AppClient client = new AppClient();

    public static Response getToken() {
        RequestSpecification requestSpecification = given()
                .basePath("/api/v1/security/csrf");
        return client.response(Method.GET, requestSpecification);
    }

    public static Response auth(AuthBodyModel bodyModel) {
        RequestSpecification requestSpecification = given()
                .basePath("/api/v1/auth")
                .body(bodyModel);
        return client.response(Method.POST, requestSpecification);
    }

    public static Response createFlow(String id, String flowName) {
        RequestSpecification requestSpecification = given()
                .basePath(String.format("/api/v1/merchants/%s/flows",id))
                .header("authorization", TokenContainer.getInstance().getBearer())
                .body(String.format("{\"name\":\"%s\"}",flowName));
        return client.response(Method.POST, requestSpecification);
    }


    public static Response deleteFlows(String merchantId, String flowId) {
        RequestSpecification requestSpecification = given()
                .basePath(String.format("/api/v1/merchants/%s/flows/%s",merchantId,flowId))
                .header("authorization", TokenContainer.getInstance().getBearer());
        return client.response(Method.DELETE, requestSpecification);
    }


    public static Response updateLanguage(Language language) {
        RequestSpecification requestSpecification = given()
                .basePath("/api/v1/merchants/me")
                .header("authorization", TokenContainer.getInstance().getBearer())
                .body(String.format("{\"configurations\":{\"dashboard\":{\"language\":\"%s\"}}}",language.getApiLanguage()));
        return client.response(Method.PATCH, requestSpecification);
    }

    public static Response createFlow(String flowName) {
        RequestSpecification requestSpecification = given()
                .basePath(String.format("/api/v1/merchants/%s/flows", TokenContainer.getInstance().getMerchant()))
                .header("authorization", TokenContainer.getInstance().getBearer())
                .body(String.format("{\"name\":\"%s\"}", flowName));
        Response response = client.response(Method.POST, requestSpecification);
        TokenContainer.getInstance().getFlowId().set(Collections.singletonList(response.as(FlowModel.class).getId()));
        return response;
    }

    public static void deleteFlows() {
        List<String> list = TokenContainer.getInstance().getFlowId().get();
        if (list != null) list.forEach(ApiService::deleteDefaultFlow);
    }

    public static Response deleteDefaultFlow(String flowId) {
        RequestSpecification requestSpecification = given()
                .basePath(String.format("/api/v1/merchants/%s/flows/%s",TokenContainer.getInstance().getMerchant(), flowId))
                        .header("authorization", TokenContainer.getInstance().getBearer());
        return client.response(Method.DELETE, requestSpecification);
    }

}