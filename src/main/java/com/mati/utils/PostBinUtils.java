package com.mati.utils;

import com.mati.model.requests.Body;
import com.mati.model.requests.Requests;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static io.restassured.RestAssured.given;

public class PostBinUtils {

    private PostBinUtils() {
    }

    private static final Logger log = LogManager.getLogger(PostBinUtils.class);

    public static final String POST_BIN_URL = "https://wh.mati.io/";
    public static final String SECRET_KEY = "secret";

    private static RequestSpecification reqSp = new RequestSpecBuilder()
            .setBaseUri(POST_BIN_URL + "api/bin")
            .setContentType(ContentType.JSON)
            .build();

    public static String createBin() {
        return given()
                .spec(reqSp)
                .when()
                .post()
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .path("binId");
    }

    public static List<Requests> getAllRequestsToBin(String binId) {
        ExtractableResponse<Response> extract = given()
                .spec(reqSp)
                .when()
                .get("/" + binId + "/reqs")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract();
        log.info(extract.body().asString());
        return Arrays.asList(extract.as(Requests[].class));
    }

    public static Body getRequestsByValidationId(String binId, String idValidation) {
        return PostBinUtils.getAllRequestsToBin(binId).stream()
                .map(Requests::getBody)
                .filter(it -> !it.getAdditionalProperties().isEmpty() && it.getAdditionalProperties().containsKey("step"))
                .filter(it -> (((HashMap<String, String>) it.getAdditionalProperties().get("step")).get("id")).equals(idValidation))
                .findFirst().get();
    }
}
