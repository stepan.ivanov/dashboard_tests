package com.mati.utils;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitUtils {

    private WaitUtils() {
    }

    private static final WebDriverWait wait = new WebDriverWait(WebDriverRunner.getWebDriver(),
            Properties.getPropertyInt("fluent.wait.timeout"));

    public static WebElement waitElementToBeVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public static void waitElementsToBeVisibility(ElementsCollection elements) {
        elements.forEach(e -> wait.until(ExpectedConditions.visibilityOf(e)));
    }

    public static void waitElementToBeInvisibility(WebElement element) {
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public static WebElement waitElementToBeClickable(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return element;
    }

    public static void sleepSeconds(int value) {
        try {
            Thread.sleep(value * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
}
