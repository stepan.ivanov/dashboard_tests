package com.mati.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonUtils {

    private JsonUtils() {
    }

    private static final ObjectMapper mapper = new ObjectMapper();

    public static JsonNode objectToJson(Object object) {
        String str;
        JsonNode jsonNode = null;
        try {
            str = mapper.writeValueAsString(object);
            jsonNode = mapper.readTree(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonNode;
    }

    public static <T> T readValue(String str, Class<T> valueType) {
        T t = null;
        try {
            t = mapper.readValue(str, valueType);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return t;
    }
}
