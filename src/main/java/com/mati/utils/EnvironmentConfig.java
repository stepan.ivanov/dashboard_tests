package com.mati.utils;

import com.mati.web.IConstants;

public class EnvironmentConfig {

    public static String url;
    public static String apiUrl;
    public static String login;
    public static String password;
    public static String loginAgent;
    public static String passwordAgent;

    static {
        url = Properties.getPropertyString("env.url");
        apiUrl = Properties.getPropertyString("env.api.url");
        login = IConstants.testEmail;
        password = IConstants.testMatiPassword;
        loginAgent ="valeriia.sysoeva+++++++++@mati.io";
        passwordAgent = "123456789";
    }
}
