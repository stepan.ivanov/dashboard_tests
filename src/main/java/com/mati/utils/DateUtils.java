package com.mati.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DateUtils {

    private static DateTimeFormatter formatDateVerification = DateTimeFormatter.ofPattern("dd MMM,yyyy", Locale.US);
    private static DateTimeFormatter formatDateOfRegistration = DateTimeFormatter.ofPattern("dd MMM, yyyy HH:mm", Locale.US);

    public static LocalDate parseVerificationCreationDate(String date) {
        return LocalDate.parse(date, formatDateVerification);
    }

    public static LocalDateTime parseDateOfRegistration(String date) {
        return LocalDateTime.parse(date, formatDateVerification);
    }

    public static LocalDateTime currentDateOfRegistration() {
        String currentDateTime = LocalDateTime.now().format(formatDateOfRegistration);
        return LocalDateTime.parse(currentDateTime, formatDateOfRegistration);
    }

    public static List<LocalDate> parseFilterDate(String date) {
        String[] dates = date.split("\n");
        List<LocalDate> list = new ArrayList<>();
        list.add(DateUtils.parseVerificationCreationDate(dates[0] + "," + dates[1]));
        list.add(DateUtils.parseVerificationCreationDate(dates[2] + "," + dates[3]));
        return list;
    }
}
