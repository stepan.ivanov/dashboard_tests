package com.mati.utils;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.open;

public class SmsUtils {


    public static String getSmsCode(String msgForCode) {
        WebDriver driver = WebDriverRunner.getWebDriver();
        WaitUtils.sleepSeconds(40);  //Wait sms
        Selenide.executeJavaScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        open("https://www.receivesms.co/us-phone-number/3465/");
        String text = Selenide.$x(String.format("//div[text()='%s']/span", msgForCode)).getText();
        driver.close();
        driver.switchTo().window(tabs.get(0));
        return text;
    }
}
