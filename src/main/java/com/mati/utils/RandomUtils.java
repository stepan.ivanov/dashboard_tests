package com.mati.utils;

import com.mati.web.IConstants;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;

public class RandomUtils {

    public static String randomEmailWithPrefix() {
        return "AQA" + RandomStringUtils.randomAlphanumeric(5) + "@mati.io";
    }

    public static String randomPersonalEmail() {
        return RandomStringUtils.randomAlphanumeric(5) + "@gmail.com";
    }

    public static String randomTestPersonalEmailWithPrefix() {
        return String.format("%s%s%s%s", IConstants.testEmailPersonal, "+", randomString(), "@gmail.com");
    }

    public static String randomString() {
        return RandomStringUtils.randomAlphanumeric(8);
    }

    public static String randomNumber() {
        return RandomStringUtils.randomNumeric(2);
    }
    public static Integer randomInt(int number) {
        return org.apache.commons.lang3.RandomUtils.nextInt(0, number);
    }

    public static <T> T getRandomElementFromList(List<T> list) {
        return list.get(org.apache.commons.lang3.RandomUtils.nextInt(0, list.size() - 1));
    }

    public static String randomStringWithPrefix() {
        return IConstants.prefix + randomString();
    }

    public static String randomURL() {
        return "https://" + RandomStringUtils.randomAlphabetic(8) + ".com";
    }
}
