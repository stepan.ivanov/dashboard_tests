package com.mati.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.io.File;
import java.io.IOException;

public class SchemaValidatorUtils {

    private SchemaValidatorUtils(){}

    private static final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    private static final ObjectMapper mapper = new ObjectMapper();

    public static boolean isValidSchema(String pathToSchema, JsonNode json) throws IOException, ProcessingException {
        JsonNode fstabSchema = mapper.readTree(new File(pathToSchema));
        JsonSchema schema = factory.getJsonSchema(fstabSchema);
        return schema.validate(json).isSuccess();
    }
}
